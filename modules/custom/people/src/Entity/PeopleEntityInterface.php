<?php

namespace Drupal\people\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining People entity entities.
 *
 * @ingroup people
 */
interface PeopleEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the People entity name.
   *
   * @return string
   *   Name of the People entity.
   */
  public function getName();

  /**
   * Sets the People entity name.
   *
   * @param string $name
   *   The People entity name.
   *
   * @return \Drupal\people\Entity\PeopleEntityInterface
   *   The called People entity entity.
   */
  public function setName($name);

  /**
   * Gets the People entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the People entity.
   */
  public function getCreatedTime();

  /**
   * Sets the People entity creation timestamp.
   *
   * @param int $timestamp
   *   The People entity creation timestamp.
   *
   * @return \Drupal\people\Entity\PeopleEntityInterface
   *   The called People entity entity.
   */
  public function setCreatedTime($timestamp);

}
