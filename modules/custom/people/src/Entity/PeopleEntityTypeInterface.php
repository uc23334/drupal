<?php

namespace Drupal\people\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining People entity type entities.
 */
interface PeopleEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
