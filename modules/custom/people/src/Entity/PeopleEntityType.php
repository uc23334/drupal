<?php

namespace Drupal\people\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the People entity type entity.
 *
 * @ConfigEntityType(
 *   id = "people_entity_type",
 *   label = @Translation("People entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\people\PeopleEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\people\Form\PeopleEntityTypeForm",
 *       "edit" = "Drupal\people\Form\PeopleEntityTypeForm",
 *       "delete" = "Drupal\people\Form\PeopleEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\people\PeopleEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "people_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "people_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/people_entity_type/{people_entity_type}",
 *     "add-form" = "/admin/structure/people_entity_type/add",
 *     "edit-form" = "/admin/structure/people_entity_type/{people_entity_type}/edit",
 *     "delete-form" = "/admin/structure/people_entity_type/{people_entity_type}/delete",
 *     "collection" = "/admin/structure/people_entity_type"
 *   }
 * )
 */
class PeopleEntityType extends ConfigEntityBundleBase implements PeopleEntityTypeInterface {

  /**
   * The People entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The People entity type label.
   *
   * @var string
   */
  protected $label;

}
