<?php

namespace Drupal\cars\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CarsEntityTypeForm.
 */
class CarsEntityTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $cars_entity_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $cars_entity_type->label(),
      '#description' => $this->t("Label for the Cars entity type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $cars_entity_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\cars\Entity\CarsEntityType::load',
      ],
      '#disabled' => !$cars_entity_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $cars_entity_type = $this->entity;
    $status = $cars_entity_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Cars entity type.', [
          '%label' => $cars_entity_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Cars entity type.', [
          '%label' => $cars_entity_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($cars_entity_type->toUrl('collection'));
  }

}
