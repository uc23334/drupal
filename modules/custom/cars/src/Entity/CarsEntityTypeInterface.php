<?php

namespace Drupal\cars\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Cars entity type entities.
 */
interface CarsEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
