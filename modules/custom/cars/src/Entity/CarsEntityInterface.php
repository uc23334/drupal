<?php

namespace Drupal\cars\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Cars entity entities.
 *
 * @ingroup cars
 */
interface CarsEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Cars entity name.
   *
   * @return string
   *   Name of the Cars entity.
   */
  public function getName();

  /**
   * Sets the Cars entity name.
   *
   * @param string $name
   *   The Cars entity name.
   *
   * @return \Drupal\cars\Entity\CarsEntityInterface
   *   The called Cars entity entity.
   */
  public function setName($name);

  /**
   * Gets the Cars entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Cars entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Cars entity creation timestamp.
   *
   * @param int $timestamp
   *   The Cars entity creation timestamp.
   *
   * @return \Drupal\cars\Entity\CarsEntityInterface
   *   The called Cars entity entity.
   */
  public function setCreatedTime($timestamp);

}
