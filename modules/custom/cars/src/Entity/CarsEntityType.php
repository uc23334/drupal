<?php

namespace Drupal\cars\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Cars entity type entity.
 *
 * @ConfigEntityType(
 *   id = "cars_entity_type",
 *   label = @Translation("Cars entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\cars\CarsEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\cars\Form\CarsEntityTypeForm",
 *       "edit" = "Drupal\cars\Form\CarsEntityTypeForm",
 *       "delete" = "Drupal\cars\Form\CarsEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\cars\CarsEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "cars_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "cars_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/content/cars_entity_type/{cars_entity_type}",
 *     "add-form" = "/admin/content/cars_entity_type/add",
 *     "edit-form" = "/admin/content/cars_entity_type/{cars_entity_type}/edit",
 *     "delete-form" = "/admin/content/cars_entity_type/{cars_entity_type}/delete",
 *     "collection" = "/admin/content/cars_entity_type"
 *   }
 * )
 */
class CarsEntityType extends ConfigEntityBundleBase implements CarsEntityTypeInterface {

  /**
   * The Cars entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Cars entity type label.
   *
   * @var string
   */
  protected $label;

}
