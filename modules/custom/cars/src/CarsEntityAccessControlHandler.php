<?php

namespace Drupal\cars;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Cars entity entity.
 *
 * @see \Drupal\cars\Entity\CarsEntity.
 */
class CarsEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\cars\Entity\CarsEntityInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished cars entity entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published cars entity entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit cars entity entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete cars entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add cars entity entities');
  }


}
