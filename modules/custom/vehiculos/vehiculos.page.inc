<?php

/**
 * @file
 * Contains vehiculos.page.inc.
 *
 * Page callback for Vehiculos entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Vehiculos templates.
 *
 * Default template: vehiculos.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_vehiculos(array &$variables) {
  // Fetch Vehiculos Entity Object.
  $vehiculos = $variables['elements']['#vehiculos'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
