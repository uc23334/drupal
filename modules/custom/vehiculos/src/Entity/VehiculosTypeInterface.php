<?php

namespace Drupal\vehiculos\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Vehiculos type entities.
 */
interface VehiculosTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
