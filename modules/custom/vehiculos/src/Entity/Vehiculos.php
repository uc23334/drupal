<?php

namespace Drupal\vehiculos\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Vehiculos entity.
 *
 * @ingroup vehiculos
 *
 * @ContentEntityType(
 *   id = "vehiculos",
 *   label = @Translation("Vehiculos"),
 *   bundle_label = @Translation("Vehiculos type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\vehiculos\VehiculosListBuilder",
 *     "views_data" = "Drupal\vehiculos\Entity\VehiculosViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\vehiculos\Form\VehiculosForm",
 *       "add" = "Drupal\vehiculos\Form\VehiculosForm",
 *       "edit" = "Drupal\vehiculos\Form\VehiculosForm",
 *       "delete" = "Drupal\vehiculos\Form\VehiculosDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\vehiculos\VehiculosHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\vehiculos\VehiculosAccessControlHandler",
 *   },
 *   base_table = "vehiculos",
 *   translatable = FALSE,
 *   admin_permission = "administer vehiculos entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/vehiculos/{vehiculos}",
 *     "add-page" = "/admin/structure/vehiculos/add",
 *     "add-form" = "/admin/structure/vehiculos/add/{vehiculos_type}",
 *     "edit-form" = "/admin/structure/vehiculos/{vehiculos}/edit",
 *     "delete-form" = "/admin/structure/vehiculos/{vehiculos}/delete",
 *     "collection" = "/admin/structure/vehiculos",
 *   },
 *   bundle_entity_type = "vehiculos_type",
 *   field_ui_base_route = "entity.vehiculos_type.edit_form"
 * )
 */
class Vehiculos extends ContentEntityBase implements VehiculosInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Vehiculos entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Vehiculos is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
