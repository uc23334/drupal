<?php

namespace Drupal\vehiculos\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Vehiculos type entity.
 *
 * @ConfigEntityType(
 *   id = "vehiculos_type",
 *   label = @Translation("Vehiculos type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\vehiculos\VehiculosTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\vehiculos\Form\VehiculosTypeForm",
 *       "edit" = "Drupal\vehiculos\Form\VehiculosTypeForm",
 *       "delete" = "Drupal\vehiculos\Form\VehiculosTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\vehiculos\VehiculosTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "vehiculos_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "vehiculos",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/vehiculos_type/{vehiculos_type}",
 *     "add-form" = "/admin/structure/vehiculos_type/add",
 *     "edit-form" = "/admin/structure/vehiculos_type/{vehiculos_type}/edit",
 *     "delete-form" = "/admin/structure/vehiculos_type/{vehiculos_type}/delete",
 *     "collection" = "/admin/structure/vehiculos_type"
 *   }
 * )
 */
class VehiculosType extends ConfigEntityBundleBase implements VehiculosTypeInterface {

  /**
   * The Vehiculos type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Vehiculos type label.
   *
   * @var string
   */
  protected $label;

}
