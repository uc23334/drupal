<?php

namespace Drupal\vehiculos\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Vehiculos entities.
 *
 * @ingroup vehiculos
 */
interface VehiculosInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Vehiculos name.
   *
   * @return string
   *   Name of the Vehiculos.
   */
  public function getName();

  /**
   * Sets the Vehiculos name.
   *
   * @param string $name
   *   The Vehiculos name.
   *
   * @return \Drupal\vehiculos\Entity\VehiculosInterface
   *   The called Vehiculos entity.
   */
  public function setName($name);

  /**
   * Gets the Vehiculos creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Vehiculos.
   */
  public function getCreatedTime();

  /**
   * Sets the Vehiculos creation timestamp.
   *
   * @param int $timestamp
   *   The Vehiculos creation timestamp.
   *
   * @return \Drupal\vehiculos\Entity\VehiculosInterface
   *   The called Vehiculos entity.
   */
  public function setCreatedTime($timestamp);

}
