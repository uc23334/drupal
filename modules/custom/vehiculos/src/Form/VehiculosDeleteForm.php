<?php

namespace Drupal\vehiculos\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Vehiculos entities.
 *
 * @ingroup vehiculos
 */
class VehiculosDeleteForm extends ContentEntityDeleteForm {


}
