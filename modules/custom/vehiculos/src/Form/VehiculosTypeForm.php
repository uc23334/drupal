<?php

namespace Drupal\vehiculos\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class VehiculosTypeForm.
 */
class VehiculosTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $vehiculos_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $vehiculos_type->label(),
      '#description' => $this->t("Label for the Vehiculos type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $vehiculos_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\vehiculos\Entity\VehiculosType::load',
      ],
      '#disabled' => !$vehiculos_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $vehiculos_type = $this->entity;
    $status = $vehiculos_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Vehiculos type.', [
          '%label' => $vehiculos_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Vehiculos type.', [
          '%label' => $vehiculos_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($vehiculos_type->toUrl('collection'));
  }

}
