<?php

namespace Drupal\vehiculos;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Vehiculos entity.
 *
 * @see \Drupal\vehiculos\Entity\Vehiculos.
 */
class VehiculosAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\vehiculos\Entity\VehiculosInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished vehiculos entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published vehiculos entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit vehiculos entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete vehiculos entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add vehiculos entities');
  }


}
