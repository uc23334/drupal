<?php

/**
 * @file
 * Contains santander_mortgage_calculator.webform.inc.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * {@inheritdoc}
 */
function _mortgage_calculator_webform_settings_form(array &$form, FormStateInterface $form_state, $webform) {

  $mortgage_comparison_url = $webform->getThirdPartySetting('santander_webform', 'mortgage_comparison_url');

  $form['third_party_settings']['santander_webform'] = [
    '#type' => 'details',
    '#title' => t('Santander Webforms'),
    '#open' => TRUE,
    '#description' => t('Settings relating to/used by the Mortgage Loan Calculator.'),
  ];

  $form['third_party_settings']['santander_webform']['mortgage_comparison_url'] = [
    '#type' => 'textfield',
    '#title' => t('Mortgage comparison URL'),
    '#description' => t("Enter the relative path to the page containing the mortgage loan product comparison calculator."),
    '#default_value' => $mortgage_comparison_url,
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#return_value' => TRUE,
  ];

  $form['#validate'][] = '_mortgage_calculator_webform_settings_validate';
}

/**
 * {@inheritdoc}
 */
function _mortgage_calculator_webform_settings_validate(&$form, FormStateInterface $form_state) {
  $url = $form_state->getValue(['third_party_settings',
    'santander_webform', 'mortgage_comparison_url',
  ]);
  if (!UrlHelper::isValid($url, FALSE)) {
    $form_state->setErrorByName('mortgage_comparison_url', t('The specified url for the mortgage loan product comparison page is invalid.'));
  }
}

/**
 * {@inheritdoc}
 */
function _mortgage_calculator_additional_webform_settings_form(array &$form, FormStateInterface $form_state, $webform) {
  $mortgage_comparison_additional_url = $webform->getThirdPartySetting('santander_webform', 'mortgage_comparison_additional_url');

  $form['third_party_settings']['santander_webform'] = [
    '#type' => 'details',
    '#title' => t('Santander Webforms'),
    '#open' => TRUE,
    '#description' => t('Settings relating to/used by the Mortgage Additional Loan Calculator.'),
  ];

  $form['third_party_settings']['santander_webform']['mortgage_comparison_additional_url'] = [
    '#type' => 'textfield',
    '#title' => t('Mortgage comparison additional URL'),
    '#description' => t("Enter the relative path to the page containing the mortgage additional loan product comparison calculator."),
    '#default_value' => $mortgage_comparison_additional_url,
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#return_value' => TRUE,
  ];

  $form['#validate'][] = '_mortgage_calculator_additional_webform_settings_validate';
}

/**
 * {@inheritdoc}
 */
function _mortgage_calculator_additional_webform_settings_validate(&$form, FormStateInterface $form_state) {
  $url = $form_state->getValue(['third_party_settings', 'santander_webform',
    'mortgage_comparison_additional_url',
  ]);
  if (!UrlHelper::isValid($url, FALSE)) {
    $form_state->setErrorByName('mortgage_comparison_additional_url', t('The specified url for the mortgage additional loan product comparison page is invalid.'));
  }
}

/**
 * {@inheritdoc}
 */
function _mortgage_comparison_webform_settings_form(array &$form, FormStateInterface $form_state, $webform) {
  $mortgage_calculator_url = $webform->getThirdPartySetting('santander_webform', 'mortgage_calculator_url');

  $form['third_party_settings']['santander_webform'] = [
    '#type' => 'details',
    '#title' => t('Santander Webforms'),
    '#open' => TRUE,
    '#description' => t('Settings relating to/used by the Mortgage Loan Product Comparison.'),
  ];

  $form['third_party_settings']['santander_webform']['mortgage_calculator_url'] = [
    '#type' => 'textfield',
    '#title' => t('Mortgage calculator URL'),
    '#description' => t("Enter the relative path to the page containing the mortgage loan calculator ."),
    '#default_value' => $mortgage_calculator_url,
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#return_value' => TRUE,
  ];

  $form['#validate'][] = '_mortgage_comparison_webform_settings_validate';
}

/**
 * {@inheritdoc}
 */
function _mortgage_comparison_webform_settings_validate(&$form, FormStateInterface $form_state) {
  $url = $form_state->getValue(['third_party_settings', 'santander_webform',
    'mortgage_calculator_url',
  ]);
  if (!UrlHelper::isValid($url, FALSE)) {
    $form_state->setErrorByName('mortgage_calculator_url', t('The specified url for the mortgage loan calculator page is invalid.'));
  }
}

/**
 * {@inheritdoc}
 */
function _mortgage_comparison_additional_webform_settings_form(array &$form, FormStateInterface $form_state, $webform) {
  $mortgage_additional_calculator_url = $webform->getThirdPartySetting('santander_webform', 'mortgage_additional_calculator_url');

  $form['third_party_settings']['santander_webform'] = [
    '#type' => 'details',
    '#title' => t('Santander Webforms'),
    '#open' => TRUE,
    '#description' => t('Settings relating to/used by the Mortgage Additional Loan Product Comparison.'),
  ];

  $form['third_party_settings']['santander_webform']['mortgage_additional_calculator_url'] = [
    '#type' => 'textfield',
    '#title' => t('Mortgage additional loan calculator URL'),
    '#description' => t("Enter the relative path to the page containing the mortgage additional loan calculator ."),
    '#default_value' => $mortgage_additional_calculator_url,
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#return_value' => TRUE,
  ];

  $form['#validate'][] = '_mortgage_comparison_additional_webform_settings_validate';
}

/**
 * {@inheritdoc}
 */
function _mortgage_comparison_additional_webform_settings_validate(&$form, FormStateInterface $form_state) {
  $url = $form_state->getValue(['third_party_settings', 'santander_webform',
    'mortgage_additional_calculator_url',
  ]);
  if (!UrlHelper::isValid($url, FALSE)) {
    $form_state->setErrorByName('mortgage_calculator_url', t('The specified url for the mortgage additional loan calculator page is invalid.'));
  }
}

/**
 * Implements hook_webform_third_party_settings_form_alter().
 */
function santander_mortgage_calculator_webform_third_party_settings_form_alter(array &$form, FormStateInterface $form_state) {
  $webform = $form_state->getFormObject()->getEntity();
  if ($webform->ID() === 'mortgage_calculator') {
    _mortgage_calculator_webform_settings_form($form, $form_state, $webform);
  }
  elseif ($webform->ID() === 'mortgage_calculator_additional') {
    _mortgage_calculator_additional_webform_settings_form($form, $form_state, $webform);
  }
  elseif ($webform->ID() === 'mortgage_comparison') {
    _mortgage_comparison_webform_settings_form($form, $form_state, $webform);
  }
  elseif ($webform->ID() === 'mortgage_comparison_additional') {
    _mortgage_comparison_additional_webform_settings_form($form, $form_state, $webform);
  }
}
