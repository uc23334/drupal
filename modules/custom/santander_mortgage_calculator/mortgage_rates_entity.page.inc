<?php

/**
 * @file
 * Contains mortgage_rates_entity.page.inc.
 *
 * Page callback for Mortgage rates entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Mortgage rates templates.
 *
 * Default template: mortgage_rates_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_mortgage_rates_entity(array &$variables) {
  // Fetch MortgageRatesEntity Entity Object.
  $mortgage_rates_entity = $variables['elements']['#mortgage_rates_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
