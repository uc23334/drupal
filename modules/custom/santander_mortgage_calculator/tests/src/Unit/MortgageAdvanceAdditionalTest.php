<?php

namespace Drupal\Tests\santander_mortgage_calculator\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\santander_mortgage_calculator\BorrowAdditionalResult;

/**
 * Test functionality of Santander Additional Advance Mortgage Calculator.
 *
 * @group santander_mortgage_calculator
 */

/**
 * To execute these tests (on Windows 10)
 *
 * ../vendor/phpunit/phpunit/phpunit  -c core/phpunit.xml.dist  modules/custom/santander_mortgage_calculator/tests/src/Unit --testdox.
 *
 * To debug
 * c:/php/php.exe -dxdebug.remote-mode=req -dxdebug.remote_port=8880 ../vendor/phpunit/phpunit/phpunit -c core/phpunit.xml.dist modules/custom/santander_mortgage_calculator/tests/src/Unit.
 */
class MortgageAdvanceAdditionalTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   *
   * @dataProvider getMortgageCalcAdditionalV2Scenarios
   */
  public function testRunV2Scenarios($expected, $inputdata) {
    $this->validateMortgageCalc($expected, $inputdata, 'v2');
  }

  /**
   * {@inheritdoc}
   *
   * @dataProvider getMortgageCalcAdditionalV3Scenarios
   */
  public function testRunV3Scenarios($expected, $inputdata) {
    $this->validateMortgageCalc($expected, $inputdata, 'v3');
  }

  /**
   * Compares the expected test data result value to the result returned by new Web18 calculator using supplied test data.
   */
  private function validateMortgageCalc($expected, $inputdata, $version) {

    $web18data = $this->mapDatatoWeb18Inputs($inputdata);

    // Call the new web18 implementation.
    $web18 = new BorrowAdditionalResult($version);
    $validated = $web18->validate($web18data);
    $web18->setParams($web18data);
    $web18result = $web18->calculate();

    // Compare the results.
    $this->assertEquals($expected, $web18result['loan']['loan']);
  }

  /**
   * {@inheritdoc}
   */
  private function mapDatatoWeb18Inputs($inputdata) {

    return [

      'applicants'                              => $inputdata['applicants'],
      'dependants'                              => $inputdata['dependants'],
      'loan_term_years'                         => $inputdata['loan_term_years'],
      'loan_term_months'                        => $inputdata['loan_term_months'],

      'debt_consolidation'                      => $inputdata['debt_consolidation'],
      'property_value'                          => $inputdata['property_value'],

      'loan_part_amount_1'                      => $inputdata['loan_part_amount_1'],
      'loan_part_term_years_1'                  => $inputdata['loan_part_term_years_1'],
      'loan_part_term_months_1'                 => $inputdata['loan_part_term_months_1'],
      'loan_part_repayment_method_1'            => $inputdata['loan_part_repayment_method_1'],

      'loan_part_amount_2'                      => $inputdata['loan_part_amount_2'],
      'loan_part_term_years_2'                  => $inputdata['loan_part_term_years_2'],
      'loan_part_term_months_2'                 => $inputdata['loan_part_term_months_2'],
      'loan_part_repayment_method_2'            => $inputdata['loan_part_repayment_method_2'],

      'loan_part_amount_3'                      => $inputdata['loan_part_amount_3'],
      'loan_part_term_years_3'                  => $inputdata['loan_part_term_years_3'],
      'loan_part_term_months_3'                 => $inputdata['loan_part_term_months_3'],
      'loan_part_repayment_method_3'            => $inputdata['loan_part_repayment_method_3'],

      'loan_part_amount_4'                      => $inputdata['loan_part_amount_4'],
      'loan_part_term_years_4'                  => $inputdata['loan_part_term_years_4'],
      'loan_part_term_months_4'                 => $inputdata['loan_part_term_months_4'],
      'loan_part_repayment_method_4'            => $inputdata['loan_part_repayment_method_4'],

      'loan_part_amount_5'                      => $inputdata['loan_part_amount_5'],
      'loan_part_term_years_5'                  => $inputdata['loan_part_term_years_5'],
      'loan_part_term_months_5'                 => $inputdata['loan_part_term_months_5'],
      'loan_part_repayment_method_5'            => $inputdata['loan_part_repayment_method_5'],

      'income_applicant_1_annual_amount'        => $inputdata['income_applicant_1_annual_amount'],
      'income_applicant_1_other_annual_amount'  => $inputdata['income_applicant_1_other_annual_amount'],
      'income_applicant_1_extra_annual_amount'  => $inputdata['income_applicant_1_extra_annual_amount'],
      'income_applicant_1_csa_monthly_amount'   => $inputdata['income_applicant_1_csa_monthly_amount'],

      'income_applicant_2_annual_amount'        => $inputdata['income_applicant_2_annual_amount'],
      'income_applicant_2_other_annual_amount'  => $inputdata['income_applicant_2_other_annual_amount'],
      'income_applicant_2_extra_annual_amount'  => $inputdata['income_applicant_2_extra_annual_amount'],
      'income_applicant_2_csa_monthly_amount'   => $inputdata['income_applicant_2_csa_monthly_amount'],

      'commitments_credit_card'                 => $inputdata['commitments_credit_card'],
      'commitments_loans'                       => $inputdata['commitments_loans'],
      'commitments_outgoings'                   => $inputdata['commitments_outgoings'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMortgageCalcAdditionalV2Scenarios() {

    return [

      [
        111749,
        [
          'applicants' => '1',
          'dependants' => '0',
          'loan_term_years' => '10',
          'loan_term_months' => '0',

          'property_value' => '500000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '50000',
          'loan_part_term_years_1' => '34',
          'loan_part_term_months_1' => '0',
          'loan_part_repayment_method_1' => 'interest',

          'loan_part_amount_2' => '100000',
          'loan_part_term_years_2' => '20',
          'loan_part_term_months_2' => '0',
          'loan_part_repayment_method_2' => 'repayment',

          'loan_part_amount_3' => '',
          'loan_part_term_years_3' => '',
          'loan_part_term_months_3' => '',
          'loan_part_repayment_method_3' => '',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '60000',
          'income_applicant_1_other_annual_amount' => '',
          'income_applicant_1_extra_annual_amount' => '',
          'income_applicant_1_csa_monthly_amount' => '',

          'income_applicant_2_annual_amount' => '',
          'income_applicant_2_other_annual_amount' => '',
          'income_applicant_2_extra_annual_amount' => '',
          'income_applicant_2_csa_monthly_amount' => '',

          'commitments_credit_card' => '',
          'commitments_loans' => '',
          'commitments_outgoings' => '',
        ],
      ],

      [
        21762,
        [
          'applicants' => '1',
          'dependants' => '2',
          'loan_term_years' => '7',
          'loan_term_months' => '0',

          'property_value' => '65000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '17000',
          'loan_part_term_years_1' => '25',
          'loan_part_term_months_1' => '9',
          'loan_part_repayment_method_1' => 'repayment',

          'loan_part_amount_2' => '12000',
          'loan_part_term_years_2' => '13',
          'loan_part_term_months_2' => '0',
          'loan_part_repayment_method_2' => 'repayment',

          'loan_part_amount_3' => '',
          'loan_part_term_years_3' => '',
          'loan_part_term_months_3' => '',
          'loan_part_repayment_method_3' => '',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '62000',
          'income_applicant_1_other_annual_amount' => '1000',
          'income_applicant_1_extra_annual_amount' => '500',
          'income_applicant_1_csa_monthly_amount' => '1500',

          'income_applicant_2_annual_amount' => '',
          'income_applicant_2_other_annual_amount' => '',
          'income_applicant_2_extra_annual_amount' => '',
          'income_applicant_2_csa_monthly_amount' => '',

          'commitments_credit_card' => '1000',
          'commitments_loans' => '800',
          'commitments_outgoings' => '1000',
        ],
      ],

      [
        58000,
        [
          'applicants' => '2',
          'dependants' => '1',
          'loan_term_years' => '5',
          'loan_term_months' => '0',

          'property_value' => '180000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '80000',
          'loan_part_term_years_1' => '25',
          'loan_part_term_months_1' => '9',
          'loan_part_repayment_method_1' => 'repayment',

          'loan_part_amount_2' => '10000',
          'loan_part_term_years_2' => '13',
          'loan_part_term_months_2' => '0',
          'loan_part_repayment_method_2' => 'repayment',

          'loan_part_amount_3' => '5000',
          'loan_part_term_years_3' => '10',
          'loan_part_term_months_3' => '6',
          'loan_part_repayment_method_3' => 'interest',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '100000',
          'income_applicant_1_other_annual_amount' => '1000',
          'income_applicant_1_extra_annual_amount' => '2000',
          'income_applicant_1_csa_monthly_amount' => '0',

          'income_applicant_2_annual_amount' => '100000',
          'income_applicant_2_other_annual_amount' => '1000',
          'income_applicant_2_extra_annual_amount' => '2000',
          'income_applicant_2_csa_monthly_amount' => '0',

          'commitments_credit_card' => '5000',
          'commitments_loans' => '4000',
          'commitments_outgoings' => '2000',
        ],
      ],

      [
        49000,
        [
          'applicants' => '2',
          'dependants' => '1',
          'loan_term_years' => '15',
          'loan_term_months' => '0',

          'property_value' => '180000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '80000',
          'loan_part_term_years_1' => '25',
          'loan_part_term_months_1' => '9',
          'loan_part_repayment_method_1' => 'repayment',

          'loan_part_amount_2' => '10000',
          'loan_part_term_years_2' => '13',
          'loan_part_term_months_2' => '0',
          'loan_part_repayment_method_2' => 'repayment',

          'loan_part_amount_3' => '5000',
          'loan_part_term_years_3' => '10',
          'loan_part_term_months_3' => '6',
          'loan_part_repayment_method_3' => 'interest',

          'loan_part_amount_4' => '7000',
          'loan_part_term_years_4' => '9',
          'loan_part_term_months_4' => '5',
          'loan_part_repayment_method_4' => 'interest',

          'loan_part_amount_5' => '2000',
          'loan_part_term_years_5' => '8',
          'loan_part_term_months_5' => '0',
          'loan_part_repayment_method_5' => 'repayment',

          'income_applicant_1_annual_amount' => '107500',
          'income_applicant_1_other_annual_amount' => '1000',
          'income_applicant_1_extra_annual_amount' => '2000',
          'income_applicant_1_csa_monthly_amount' => '500',

          'income_applicant_2_annual_amount' => '107500',
          'income_applicant_2_other_annual_amount' => '1000',
          'income_applicant_2_extra_annual_amount' => '2000',
          'income_applicant_2_csa_monthly_amount' => '500',

          'commitments_credit_card' => '5000',
          'commitments_loans' => '4000',
          'commitments_outgoings' => '2000',
        ],
      ],

      [
        0,
        [
          'applicants' => '1',
          'dependants' => '0',
          'loan_term_years' => '16',
          'loan_term_months' => '3',

          'property_value' => '65000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '45000',
          'loan_part_term_years_1' => '25',
          'loan_part_term_months_1' => '9',
          'loan_part_repayment_method_1' => 'repayment',

          'loan_part_amount_2' => '',
          'loan_part_term_years_2' => '',
          'loan_part_term_months_2' => '',
          'loan_part_repayment_method_2' => '',

          'loan_part_amount_3' => '',
          'loan_part_term_years_3' => '',
          'loan_part_term_months_3' => '',
          'loan_part_repayment_method_3' => '',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '15000',
          'income_applicant_1_other_annual_amount' => '',
          'income_applicant_1_extra_annual_amount' => '',
          'income_applicant_1_csa_monthly_amount' => '',

          'income_applicant_2_annual_amount' => '',
          'income_applicant_2_other_annual_amount' => '',
          'income_applicant_2_extra_annual_amount' => '',
          'income_applicant_2_csa_monthly_amount' => '',

          'commitments_credit_card' => '1000',
          'commitments_loans' => '800',
          'commitments_outgoings' => '1000',
        ],
      ],

      [
        100000,
        [
          'applicants' => '1',
          'dependants' => '0',
          'loan_term_years' => '10',
          'loan_term_months' => '0',

          'property_value' => '700000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '300000',
          'loan_part_term_years_1' => '34',
          'loan_part_term_months_1' => '0',
          'loan_part_repayment_method_1' => 'repayment',

          'loan_part_amount_2' => '',
          'loan_part_term_years_2' => '',
          'loan_part_term_months_2' => '',
          'loan_part_repayment_method_2' => '',

          'loan_part_amount_3' => '',
          'loan_part_term_years_3' => '',
          'loan_part_term_months_3' => '',
          'loan_part_repayment_method_3' => '',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '80000',
          'income_applicant_1_other_annual_amount' => '',
          'income_applicant_1_extra_annual_amount' => '',
          'income_applicant_1_csa_monthly_amount' => '',

          'income_applicant_2_annual_amount' => '',
          'income_applicant_2_other_annual_amount' => '',
          'income_applicant_2_extra_annual_amount' => '',
          'income_applicant_2_csa_monthly_amount' => '',

          'commitments_credit_card' => '',
          'commitments_loans' => '',
          'commitments_outgoings' => '',
        ],
      ],

      [
        153148,
        [
          'applicants' => '1',
          'dependants' => '0',
          'loan_term_years' => '10',
          'loan_term_months' => '0',

          'property_value' => '500000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '200000',
          'loan_part_term_years_1' => '34',
          'loan_part_term_months_1' => '0',
          'loan_part_repayment_method_1' => 'repayment',

          'loan_part_amount_2' => '',
          'loan_part_term_years_2' => '',
          'loan_part_term_months_2' => '',
          'loan_part_repayment_method_2' => '',

          'loan_part_amount_3' => '',
          'loan_part_term_years_3' => '',
          'loan_part_term_months_3' => '',
          'loan_part_repayment_method_3' => '',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '100000',
          'income_applicant_1_other_annual_amount' => '',
          'income_applicant_1_extra_annual_amount' => '',
          'income_applicant_1_csa_monthly_amount' => '',

          'income_applicant_2_annual_amount' => '',
          'income_applicant_2_other_annual_amount' => '',
          'income_applicant_2_extra_annual_amount' => '',
          'income_applicant_2_csa_monthly_amount' => '',

          'commitments_credit_card' => '4000',
          'commitments_loans' => '750',
          'commitments_outgoings' => '',
        ],
      ],

      [
        556597,
        [
          'applicants' => '1',
          'dependants' => '0',
          'loan_term_years' => '25',
          'loan_term_months' => '0',

          'property_value' => '1000000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '100000',
          'loan_part_term_years_1' => '34',
          'loan_part_term_months_1' => '0',
          'loan_part_repayment_method_1' => 'repayment',

          'loan_part_amount_2' => '',
          'loan_part_term_years_2' => '',
          'loan_part_term_months_2' => '',
          'loan_part_repayment_method_2' => '',

          'loan_part_amount_3' => '',
          'loan_part_term_years_3' => '',
          'loan_part_term_months_3' => '',
          'loan_part_repayment_method_3' => '',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '125000',
          'income_applicant_1_other_annual_amount' => '',
          'income_applicant_1_extra_annual_amount' => '',
          'income_applicant_1_csa_monthly_amount' => '',

          'income_applicant_2_annual_amount' => '',
          'income_applicant_2_other_annual_amount' => '',
          'income_applicant_2_extra_annual_amount' => '',
          'income_applicant_2_csa_monthly_amount' => '',

          'commitments_credit_card' => '',
          'commitments_loans' => '',
          'commitments_outgoings' => '',
        ],
      ],

      [
        135000,
        [
          'applicants' => '1',
          'dependants' => '0',
          'loan_term_years' => '25',
          'loan_term_months' => '0',

          'property_value' => '400000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '205000',
          'loan_part_term_years_1' => '34',
          'loan_part_term_months_1' => '0',
          'loan_part_repayment_method_1' => 'interest',

          'loan_part_amount_2' => '',
          'loan_part_term_years_2' => '',
          'loan_part_term_months_2' => '',
          'loan_part_repayment_method_2' => '',

          'loan_part_amount_3' => '',
          'loan_part_term_years_3' => '',
          'loan_part_term_months_3' => '',
          'loan_part_repayment_method_3' => '',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '125000',
          'income_applicant_1_other_annual_amount' => '',
          'income_applicant_1_extra_annual_amount' => '',
          'income_applicant_1_csa_monthly_amount' => '',

          'income_applicant_2_annual_amount' => '',
          'income_applicant_2_other_annual_amount' => '',
          'income_applicant_2_extra_annual_amount' => '',
          'income_applicant_2_csa_monthly_amount' => '',

          'commitments_credit_card' => '',
          'commitments_loans' => '',
          'commitments_outgoings' => '',
        ],
      ],

      [
        145000,
        [
          'applicants' => '2',
          'dependants' => '2',
          'loan_term_years' => '25',
          'loan_term_months' => '0',

          'property_value' => '500000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '280000',
          'loan_part_term_years_1' => '34',
          'loan_part_term_months_1' => '0',
          'loan_part_repayment_method_1' => 'interest',

          'loan_part_amount_2' => '',
          'loan_part_term_years_2' => '',
          'loan_part_term_months_2' => '',
          'loan_part_repayment_method_2' => '',

          'loan_part_amount_3' => '',
          'loan_part_term_years_3' => '',
          'loan_part_term_months_3' => '',
          'loan_part_repayment_method_3' => '',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '100000',
          'income_applicant_1_other_annual_amount' => '',
          'income_applicant_1_extra_annual_amount' => '',
          'income_applicant_1_csa_monthly_amount' => '',

          'income_applicant_2_annual_amount' => '100000',
          'income_applicant_2_other_annual_amount' => '',
          'income_applicant_2_extra_annual_amount' => '',
          'income_applicant_2_csa_monthly_amount' => '',

          'commitments_credit_card' => '3000',
          'commitments_loans' => '200',
          'commitments_outgoings' => '100',
        ],
      ],

      [
        0,
        [
          'applicants' => '1',
          'dependants' => '0',
          'loan_term_years' => '16',
          'loan_term_months' => '3',

          'property_value' => '65000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '45000',
          'loan_part_term_years_1' => '25',
          'loan_part_term_months_1' => '9',
          'loan_part_repayment_method_1' => 'repayment',

          'loan_part_amount_2' => '',
          'loan_part_term_years_2' => '',
          'loan_part_term_months_2' => '',
          'loan_part_repayment_method_2' => '',

          'loan_part_amount_3' => '',
          'loan_part_term_years_3' => '',
          'loan_part_term_months_3' => '',
          'loan_part_repayment_method_3' => '',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '15000',
          'income_applicant_1_other_annual_amount' => '',
          'income_applicant_1_extra_annual_amount' => '',
          'income_applicant_1_csa_monthly_amount' => '',

          'income_applicant_2_annual_amount' => '',
          'income_applicant_2_other_annual_amount' => '',
          'income_applicant_2_extra_annual_amount' => '',
          'income_applicant_2_csa_monthly_amount' => '',

          'commitments_credit_card' => '1000',
          'commitments_loans' => '800',
          'commitments_outgoings' => '1000',
        ],
      ],

    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMortgageCalcAdditionalV3Scenarios() {

    return [

      [
        106237,
        [
          'applicants' => '1',
          'dependants' => '0',
          'loan_term_years' => '10',
          'loan_term_months' => '0',

          'property_value' => '500000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '50000',
          'loan_part_term_years_1' => '34',
          'loan_part_term_months_1' => '0',
          'loan_part_repayment_method_1' => 'interest',

          'loan_part_amount_2' => '100000',
          'loan_part_term_years_2' => '20',
          'loan_part_term_months_2' => '0',
          'loan_part_repayment_method_2' => 'repayment',

          'loan_part_amount_3' => '',
          'loan_part_term_years_3' => '',
          'loan_part_term_months_3' => '',
          'loan_part_repayment_method_3' => '',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '60000',
          'income_applicant_1_other_annual_amount' => '',
          'income_applicant_1_extra_annual_amount' => '',
          'income_applicant_1_csa_monthly_amount' => '',

          'income_applicant_2_annual_amount' => '',
          'income_applicant_2_other_annual_amount' => '',
          'income_applicant_2_extra_annual_amount' => '',
          'income_applicant_2_csa_monthly_amount' => '',

          'commitments_credit_card' => '',
          'commitments_loans' => '',
          'commitments_outgoings' => '',
        ],
      ],

      [
        15336,
        [
          'applicants' => '1',
          'dependants' => '2',
          'loan_term_years' => '7',
          'loan_term_months' => '0',

          'property_value' => '65000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '17000',
          'loan_part_term_years_1' => '25',
          'loan_part_term_months_1' => '9',
          'loan_part_repayment_method_1' => 'repayment',

          'loan_part_amount_2' => '12000',
          'loan_part_term_years_2' => '13',
          'loan_part_term_months_2' => '0',
          'loan_part_repayment_method_2' => 'repayment',

          'loan_part_amount_3' => '',
          'loan_part_term_years_3' => '',
          'loan_part_term_months_3' => '',
          'loan_part_repayment_method_3' => '',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '62000',
          'income_applicant_1_other_annual_amount' => '1000',
          'income_applicant_1_extra_annual_amount' => '500',
          'income_applicant_1_csa_monthly_amount' => '1500',

          'income_applicant_2_annual_amount' => '',
          'income_applicant_2_other_annual_amount' => '',
          'income_applicant_2_extra_annual_amount' => '',
          'income_applicant_2_csa_monthly_amount' => '',

          'commitments_credit_card' => '1000',
          'commitments_loans' => '800',
          'commitments_outgoings' => '1000',
        ],
      ],

      [
        58000,
        [
          'applicants' => '2',
          'dependants' => '1',
          'loan_term_years' => '5',
          'loan_term_months' => '0',

          'property_value' => '180000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '80000',
          'loan_part_term_years_1' => '25',
          'loan_part_term_months_1' => '9',
          'loan_part_repayment_method_1' => 'repayment',

          'loan_part_amount_2' => '10000',
          'loan_part_term_years_2' => '13',
          'loan_part_term_months_2' => '0',
          'loan_part_repayment_method_2' => 'repayment',

          'loan_part_amount_3' => '5000',
          'loan_part_term_years_3' => '10',
          'loan_part_term_months_3' => '6',
          'loan_part_repayment_method_3' => 'interest',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '100000',
          'income_applicant_1_other_annual_amount' => '1000',
          'income_applicant_1_extra_annual_amount' => '2000',
          'income_applicant_1_csa_monthly_amount' => '0',

          'income_applicant_2_annual_amount' => '100000',
          'income_applicant_2_other_annual_amount' => '1000',
          'income_applicant_2_extra_annual_amount' => '2000',
          'income_applicant_2_csa_monthly_amount' => '0',

          'commitments_credit_card' => '5000',
          'commitments_loans' => '4000',
          'commitments_outgoings' => '2000',
        ],
      ],

      [
        49000,
        [
          'applicants' => '2',
          'dependants' => '1',
          'loan_term_years' => '15',
          'loan_term_months' => '0',

          'property_value' => '180000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '80000',
          'loan_part_term_years_1' => '25',
          'loan_part_term_months_1' => '9',
          'loan_part_repayment_method_1' => 'repayment',

          'loan_part_amount_2' => '10000',
          'loan_part_term_years_2' => '13',
          'loan_part_term_months_2' => '0',
          'loan_part_repayment_method_2' => 'repayment',

          'loan_part_amount_3' => '5000',
          'loan_part_term_years_3' => '10',
          'loan_part_term_months_3' => '6',
          'loan_part_repayment_method_3' => 'interest',

          'loan_part_amount_4' => '7000',
          'loan_part_term_years_4' => '9',
          'loan_part_term_months_4' => '5',
          'loan_part_repayment_method_4' => 'interest',

          'loan_part_amount_5' => '2000',
          'loan_part_term_years_5' => '8',
          'loan_part_term_months_5' => '0',
          'loan_part_repayment_method_5' => 'repayment',

          'income_applicant_1_annual_amount' => '107500',
          'income_applicant_1_other_annual_amount' => '1000',
          'income_applicant_1_extra_annual_amount' => '2000',
          'income_applicant_1_csa_monthly_amount' => '500',

          'income_applicant_2_annual_amount' => '107500',
          'income_applicant_2_other_annual_amount' => '1000',
          'income_applicant_2_extra_annual_amount' => '2000',
          'income_applicant_2_csa_monthly_amount' => '500',

          'commitments_credit_card' => '5000',
          'commitments_loans' => '4000',
          'commitments_outgoings' => '2000',
        ],
      ],

      [
        0,
        [
          'applicants' => '1',
          'dependants' => '0',
          'loan_term_years' => '16',
          'loan_term_months' => '3',

          'property_value' => '65000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '45000',
          'loan_part_term_years_1' => '25',
          'loan_part_term_months_1' => '9',
          'loan_part_repayment_method_1' => 'repayment',

          'loan_part_amount_2' => '',
          'loan_part_term_years_2' => '',
          'loan_part_term_months_2' => '',
          'loan_part_repayment_method_2' => '',

          'loan_part_amount_3' => '',
          'loan_part_term_years_3' => '',
          'loan_part_term_months_3' => '',
          'loan_part_repayment_method_3' => '',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '15000',
          'income_applicant_1_other_annual_amount' => '',
          'income_applicant_1_extra_annual_amount' => '',
          'income_applicant_1_csa_monthly_amount' => '',

          'income_applicant_2_annual_amount' => '',
          'income_applicant_2_other_annual_amount' => '',
          'income_applicant_2_extra_annual_amount' => '',
          'income_applicant_2_csa_monthly_amount' => '',

          'commitments_credit_card' => '1000',
          'commitments_loans' => '800',
          'commitments_outgoings' => '1000',
        ],
      ],

      [
        98550,
        [
          'applicants' => '1',
          'dependants' => '0',
          'loan_term_years' => '10',
          'loan_term_months' => '0',

          'property_value' => '700000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '300000',
          'loan_part_term_years_1' => '34',
          'loan_part_term_months_1' => '0',
          'loan_part_repayment_method_1' => 'repayment',

          'loan_part_amount_2' => '',
          'loan_part_term_years_2' => '',
          'loan_part_term_months_2' => '',
          'loan_part_repayment_method_2' => '',

          'loan_part_amount_3' => '',
          'loan_part_term_years_3' => '',
          'loan_part_term_months_3' => '',
          'loan_part_repayment_method_3' => '',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '80000',
          'income_applicant_1_other_annual_amount' => '',
          'income_applicant_1_extra_annual_amount' => '',
          'income_applicant_1_csa_monthly_amount' => '',

          'income_applicant_2_annual_amount' => '',
          'income_applicant_2_other_annual_amount' => '',
          'income_applicant_2_extra_annual_amount' => '',
          'income_applicant_2_csa_monthly_amount' => '',

          'commitments_credit_card' => '',
          'commitments_loans' => '',
          'commitments_outgoings' => '',
        ],
      ],

      [
        148411,
        [
          'applicants' => '1',
          'dependants' => '0',
          'loan_term_years' => '10',
          'loan_term_months' => '0',

          'property_value' => '500000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '200000',
          'loan_part_term_years_1' => '34',
          'loan_part_term_months_1' => '0',
          'loan_part_repayment_method_1' => 'repayment',

          'loan_part_amount_2' => '',
          'loan_part_term_years_2' => '',
          'loan_part_term_months_2' => '',
          'loan_part_repayment_method_2' => '',

          'loan_part_amount_3' => '',
          'loan_part_term_years_3' => '',
          'loan_part_term_months_3' => '',
          'loan_part_repayment_method_3' => '',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '100000',
          'income_applicant_1_other_annual_amount' => '',
          'income_applicant_1_extra_annual_amount' => '',
          'income_applicant_1_csa_monthly_amount' => '',

          'income_applicant_2_annual_amount' => '',
          'income_applicant_2_other_annual_amount' => '',
          'income_applicant_2_extra_annual_amount' => '',
          'income_applicant_2_csa_monthly_amount' => '',

          'commitments_credit_card' => '4000',
          'commitments_loans' => '750',
          'commitments_outgoings' => '',
        ],
      ],

      [
        551221,
        [
          'applicants' => '1',
          'dependants' => '0',
          'loan_term_years' => '25',
          'loan_term_months' => '0',

          'property_value' => '1000000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '100000',
          'loan_part_term_years_1' => '34',
          'loan_part_term_months_1' => '0',
          'loan_part_repayment_method_1' => 'repayment',

          'loan_part_amount_2' => '',
          'loan_part_term_years_2' => '',
          'loan_part_term_months_2' => '',
          'loan_part_repayment_method_2' => '',

          'loan_part_amount_3' => '',
          'loan_part_term_years_3' => '',
          'loan_part_term_months_3' => '',
          'loan_part_repayment_method_3' => '',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '125000',
          'income_applicant_1_other_annual_amount' => '',
          'income_applicant_1_extra_annual_amount' => '',
          'income_applicant_1_csa_monthly_amount' => '',

          'income_applicant_2_annual_amount' => '',
          'income_applicant_2_other_annual_amount' => '',
          'income_applicant_2_extra_annual_amount' => '',
          'income_applicant_2_csa_monthly_amount' => '',

          'commitments_credit_card' => '',
          'commitments_loans' => '',
          'commitments_outgoings' => '',
        ],
      ],

      [
        135000,
        [
          'applicants' => '1',
          'dependants' => '0',
          'loan_term_years' => '25',
          'loan_term_months' => '0',

          'property_value' => '400000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '205000',
          'loan_part_term_years_1' => '34',
          'loan_part_term_months_1' => '0',
          'loan_part_repayment_method_1' => 'interest',

          'loan_part_amount_2' => '',
          'loan_part_term_years_2' => '',
          'loan_part_term_months_2' => '',
          'loan_part_repayment_method_2' => '',

          'loan_part_amount_3' => '',
          'loan_part_term_years_3' => '',
          'loan_part_term_months_3' => '',
          'loan_part_repayment_method_3' => '',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '125000',
          'income_applicant_1_other_annual_amount' => '',
          'income_applicant_1_extra_annual_amount' => '',
          'income_applicant_1_csa_monthly_amount' => '',

          'income_applicant_2_annual_amount' => '',
          'income_applicant_2_other_annual_amount' => '',
          'income_applicant_2_extra_annual_amount' => '',
          'income_applicant_2_csa_monthly_amount' => '',

          'commitments_credit_card' => '',
          'commitments_loans' => '',
          'commitments_outgoings' => '',
        ],
      ],

      [
        145000,
        [
          'applicants' => '2',
          'dependants' => '2',
          'loan_term_years' => '25',
          'loan_term_months' => '0',

          'property_value' => '500000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '280000',
          'loan_part_term_years_1' => '34',
          'loan_part_term_months_1' => '0',
          'loan_part_repayment_method_1' => 'interest',

          'loan_part_amount_2' => '',
          'loan_part_term_years_2' => '',
          'loan_part_term_months_2' => '',
          'loan_part_repayment_method_2' => '',

          'loan_part_amount_3' => '',
          'loan_part_term_years_3' => '',
          'loan_part_term_months_3' => '',
          'loan_part_repayment_method_3' => '',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '100000',
          'income_applicant_1_other_annual_amount' => '',
          'income_applicant_1_extra_annual_amount' => '',
          'income_applicant_1_csa_monthly_amount' => '',

          'income_applicant_2_annual_amount' => '100000',
          'income_applicant_2_other_annual_amount' => '',
          'income_applicant_2_extra_annual_amount' => '',
          'income_applicant_2_csa_monthly_amount' => '',

          'commitments_credit_card' => '3000',
          'commitments_loans' => '200',
          'commitments_outgoings' => '100',
        ],
      ],

      [
        0,
        [
          'applicants' => '1',
          'dependants' => '0',
          'loan_term_years' => '16',
          'loan_term_months' => '3',

          'property_value' => '65000',
          'debt_consolidation' => 'no',

          'loan_part_amount_1' => '45000',
          'loan_part_term_years_1' => '25',
          'loan_part_term_months_1' => '9',
          'loan_part_repayment_method_1' => 'repayment',

          'loan_part_amount_2' => '',
          'loan_part_term_years_2' => '',
          'loan_part_term_months_2' => '',
          'loan_part_repayment_method_2' => '',

          'loan_part_amount_3' => '',
          'loan_part_term_years_3' => '',
          'loan_part_term_months_3' => '',
          'loan_part_repayment_method_3' => '',

          'loan_part_amount_4' => '',
          'loan_part_term_years_4' => '',
          'loan_part_term_months_4' => '',
          'loan_part_repayment_method_4' => '',

          'loan_part_amount_5' => '',
          'loan_part_term_years_5' => '',
          'loan_part_term_months_5' => '',
          'loan_part_repayment_method_5' => '',

          'income_applicant_1_annual_amount' => '15000',
          'income_applicant_1_other_annual_amount' => '',
          'income_applicant_1_extra_annual_amount' => '',
          'income_applicant_1_csa_monthly_amount' => '',

          'income_applicant_2_annual_amount' => '',
          'income_applicant_2_other_annual_amount' => '',
          'income_applicant_2_extra_annual_amount' => '',
          'income_applicant_2_csa_monthly_amount' => '',

          'commitments_credit_card' => '1000',
          'commitments_loans' => '800',
          'commitments_outgoings' => '1000',
        ],
      ],

    ];
  }

}
