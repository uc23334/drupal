<?php

namespace Drupal\santander_mortgage_calculator;

use Drupal\santander_mortgage_calculator\RateCards\BorrowResultLoanV1;
use Drupal\santander_mortgage_calculator\RateCards\BorrowResultLoanV2;
use Drupal\santander_mortgage_calculator\RateCards\BorrowResultLoanV3;

/**
 * Loan model.
 */
class BorrowResultLoan {

  /**
   * {@inheritdoc}
   *
   * Ratecard Constants/Data/Values.
   */
  private $ltv;

  /**
   * {@inheritdoc}
   */
  private $secondaryWeighting;

  /**
   * {@inheritdoc}
   */
  private $minAmountPolicy;

  /**
   * {@inheritdoc}
   */
  private $concessionaryStressRate;

  /**
   * {@inheritdoc}
   *
   * Mortgage.
   */
  private $mortgage = NULL;

  /**
   * {@inheritdoc}
   *
   * Income.
   */
  private $income = NULL;

  /**
   * {@inheritdoc}
   *
   * Commitments.
   */
  private $commitments = NULL;

  /**
   * {@inheritdoc}
   *
   * Gross.
   */
  private $gross = NULL;

  /**
   * {@inheritdoc}
   *
   * Expenditure.
   */
  private $expenditure = NULL;

  /**
   * {@inheritdoc}
   *
   * Credit.
   */
  private $credit = NULL;

  /**
   * {@inheritdoc}
   *
   * Loan.
   */
  private $loan = [
  // ÂŁ.
    'allowable' => 0,
  // ÂŁ.
    'disposable' => 0,
    'ltv' => [
      'p4p' => [
  // ÂŁ.
        'affordable' => 0,
  // ÂŁ.
        'multiple' => 0,
  // ÂŁ.
        'ltv' => 0,
  // ÂŁ.
        'restricted' => 0,
  // %.
        'percentage' => 0,
      ],
      'low' => [
      // ÂŁ.
        'affordable' => 0,
      // ÂŁ.
        'multiple' => 0,
      // ÂŁ.
        'ltv' => 0,
      // ÂŁ.
        'restricted' => 0,
      // %.
        'percentage' => 0,
      ],
      'medium' => [
      // ÂŁ.
        'affordable' => 0,
      // ÂŁ.
        'multiple' => 0,
      // ÂŁ.
        'ltv' => 0,
      // ÂŁ.
        'restricted' => 0,
      // %.
        'percentage' => 0,
      ],
      'high' => [
      // ÂŁ.
        'affordable' => 0,
      // ÂŁ.
        'multiple' => 0,
      // ÂŁ.
        'ltv' => 0,
      // ÂŁ.
        'restricted' => 0,
      // %.
        'percentage' => 0,
      ],
      'higher' => [
      // ÂŁ.
        'affordable' => 0,
      // ÂŁ.
        'multiple' => 0,
      // ÂŁ.
        'restricted' => 0,
      // %
        'percentage' => 0,
      ],
    ],
    // ÂŁ.
    'loan' => 0,
  ];

  /**
   * Constructor.
   */
  public function __construct($mortgage, $income, $commitments, $i, $e, $c, $version = 'v1') {

    $this->mortgage = $mortgage;
    $this->income = $income;
    $this->commitments = $commitments;
    $this->gross = $i;
    $this->expenditure = $e;
    $this->credit = $c;

    // Load the ratecard data from the appropriate ratecard.
    if ($version == 'v1') {
      $ratecard = new BorrowResultLoanV1();
    }
    elseif ($version == 'v3') {
      $ratecard = new BorrowResultLoanV3();
    }
    else {
      $ratecard = new BorrowResultLoanV2();
    }

    $this->ltv = $ratecard->ltv;
    $this->secondaryWeighting = $ratecard->secondaryWeighting;
    $this->minAmountPolicy = $ratecard->minAmountPolicy;
    $this->concessionaryStressRate = $ratecard->concessionaryStressRate;

  }

  /**
   * Calculate max loan.
   */
  public function getLoan() {
    $this->loan['allowable'] = $this->allowableIncome();
    $this->loan['disposable'] = $this->disposableIncome();

    $this->loan['ltv']['p4p']['affordable'] = $this->p4pAffortable();
    $this->loan['ltv']['p4p']['multiple'] = $this->maxAmountMultiple('p4p');
    $this->loan['ltv']['p4p']['ltv'] = $this->lowMaxAmountLtv();
    $this->loan['ltv']['p4p']['restricted'] = $this->p4pRestrictedAmount();
    $this->loan['ltv']['p4p']['percentage'] = $this->maxAmountLtvPercentage('p4p');

    $this->loan['ltv']['low']['affordable'] = $this->lowAffortable();
    $this->loan['ltv']['low']['multiple'] = $this->maxAmountMultiple('low');
    $this->loan['ltv']['low']['ltv'] = $this->lowMaxAmountLtv();
    $this->loan['ltv']['low']['restricted'] = $this->lowRestrictedAmount();
    $this->loan['ltv']['low']['percentage'] = $this->maxAmountLtvPercentage('low');

    $this->loan['ltv']['medium']['affordable'] = $this->mediumAffortable();
    $this->loan['ltv']['medium']['multiple'] = $this->maxAmountMultiple('medium');
    $this->loan['ltv']['medium']['ltv'] = $this->mediumMaxAmountLtv();
    $this->loan['ltv']['medium']['restricted'] = $this->mediumRestrictedAmount();
    $this->loan['ltv']['medium']['percentage'] = $this->maxAmountLtvPercentage('medium');

    $this->loan['ltv']['high']['affordable'] = $this->highAffortable();
    $this->loan['ltv']['high']['multiple'] = $this->maxAmountMultiple('high');
    $this->loan['ltv']['high']['ltv'] = $this->highMaxAmountLtv();
    $this->loan['ltv']['high']['restricted'] = $this->highRestrictedAmount();
    $this->loan['ltv']['high']['percentage'] = $this->maxAmountLtvPercentage('high');

    $this->loan['ltv']['higher']['affordable'] = $this->higherAffortable();
    $this->loan['ltv']['higher']['multiple'] = $this->maxAmountMultiple('higher');
    $this->loan['ltv']['higher']['restricted'] = $this->higherRestrictedAmount();
    $this->loan['ltv']['higher']['percentage'] = $this->maxAmountLtvPercentage('higher');

    $this->loan['loan'] = $this->maxLoanAmount();

    if ($this->loan['loan'] < $this->minAmountPolicy) {
      $this->loan['loan'] = 0;
    }

    // Limit LTV to 90% when remortgaging.
    if (($this->mortgage['buyer_type'] == 'remortgage') && ($this->loan['loan'] > $this->loan['ltv']['high']['ltv'])) {
      $this->loan['loan'] = $this->loan['ltv']['high']['ltv'];
    }

    return $this->loan;
  }

  /**
   * Calculate allowable income.
   */
  public function allowableIncome() {
    $sum1 = $this->income['basic']['a1']['taxable']
      + $this->income['basic']['a2']['taxable']
      + $this->income['guaranteed']['a1']['taxable']
      + $this->income['guaranteed']['a2']['taxable']
      + $this->income['guaranteed']['a1']['nonTaxable']
      + $this->income['guaranteed']['a2']['nonTaxable'];

    $sum2 = $this->income['regular']['a1']['taxable']
      + $this->income['regular']['a2']['taxable']
      + $this->income['regular']['a1']['nonTaxable']
      + $this->income['regular']['a2']['nonTaxable'];

    $final = $sum1 + $this->secondaryWeighting * $sum2;

    return $final;

  }

  /**
   * Calculate disposable income.
   */
  public function disposableIncome() {
    return $this->gross['totalMonthly'] - $this->expenditure['final'] - $this->credit - $this->commitments['outgoings'];
  }

  // Calculate low LTV max amount affordable
  // public function p4pAffortable()
  // {
  // return max(array(
  // (
  // $this->loan['disposable'] *
  // (1 - pow((1 + ($this->ltv['p4p']['rate'] / 12)), (-1 * (12 * $this->mortgage['term']))))
  // ) / ($this->ltv['p4p']['rate'] / 12)
  // ));
  // }.

  /**
   * Calculate low LTV max amount affordable (new version)
   */
  public function p4pAffortable() {
    if ($this->mortgage['same_borrowers'] == 'no') {
      $rate = $this->ltv['p4p']['rate'];
    }
    elseif ($this->mortgage['same_borrowers'] == 'yes') {
      $rate = $this->concessionaryStressRate;
    }
    return max(
      $this->loan['disposable'] * (1 - pow(1 + ($rate / 12), (-1 * (12 * $this->mortgage['term'])))) / ($rate / 12),
      0
    );

  }

  /**
   * Calculate low LTV max amount affordable.
   */
  public function lowAffortable() {
    return max([
      (
            $this->loan['disposable'] *
            (1 - pow((1 + ($this->ltv['low']['rate'] / 12)), (-1 * (12 * $this->mortgage['term']))))
      ) / ($this->ltv['low']['rate'] / 12),
    ]);
  }

  /**
   * Calculate medium LTV max amount affordable.
   */
  public function mediumAffortable() {
    return max([
      (
            $this->loan['disposable'] *
            (1 - pow((1 + ($this->ltv['medium']['rate'] / 12)), (-1 * (12 * $this->mortgage['term']))))
      ) / ($this->ltv['medium']['rate'] / 12),
    ]);
  }

  /**
   * Calculate high LTV max amount affordable.
   */
  public function highAffortable() {
    $affordable = max([
      (
            $this->loan['disposable'] *
            (1 - pow((1 + ($this->ltv['high']['rate'] / 12)), (-1 * (12 * $this->mortgage['term']))))
      ) / ($this->ltv['high']['rate'] / 12),
    ]);
    return $affordable;
  }

  /**
   * Calculate higher LTV max amount affordable.
   */
  public function higherAffortable() {
    $affordable = max([
      (
            $this->loan['disposable'] *
            (1 - pow((1 + ($this->ltv['higher']['rate'] / 12)), (-1 * (12 * $this->mortgage['term']))))
      ) / ($this->ltv['higher']['rate'] / 12),
    ]);
    return $affordable;
  }

  /**
   * Calculate max amount multiple.
   */
  public function maxAmountMultiple($a) {

    // Buyers is always 'no' according to the original Hogarth js form.
    if ($this->mortgage['buyers'] == 'yes') {
      $maxAmount = $this->ltv[$a]['ftb_rate'] * $this->loan['allowable'];
    }
    else {
      if ($this->mortgage['applicants'] == 1) {
        $maxAmount = $this->ltv[$a]['single'] * $this->loan['allowable'];
      }
      else {
        $maxAmount = $this->ltv[$a]['joint'] * $this->loan['allowable'];
      }
    }

    return $maxAmount;
  }

  /**
   * Calculate low max amount LTV.
   */
  public function lowMaxAmountLtv() {
    return (0.75 / 0.25) * $this->mortgage['deposit'];
  }

  /**
   * Calculate medium max amount LTV.
   */
  public function mediumMaxAmountLtv() {
    return (0.85 / 0.15) * $this->mortgage['deposit'];
  }

  /**
   * Calculate high max amount LTV.
   */
  public function highMaxAmountLtv() {
    return (0.9 / 0.1) * $this->mortgage['deposit'];
  }

  /**
   * Calculate p4p restricted amount.
   */
  public function p4pRestrictedAmount() {
    return min([
      $this->loan['ltv']['p4p']['affordable'],
      $this->loan['ltv']['p4p']['multiple'],
      $this->mortgage['current_mortgage_balance'],
    ]);
  }

  /**
   * Calculate low restricted amount.
   */
  public function lowRestrictedAmount() {
    return min([
      $this->loan['ltv']['low']['affordable'],
      $this->loan['ltv']['low']['multiple'],
      $this->loan['ltv']['low']['ltv'],
    ]);
  }

  /**
   * Calculate medium restricted amount.
   */
  public function mediumRestrictedAmount() {
    return min([
      $this->loan['ltv']['medium']['affordable'],
      $this->loan['ltv']['medium']['multiple'],
      $this->loan['ltv']['medium']['ltv'],
    ]);
  }

  /**
   * Calculate high restricted amount.
   */
  public function highRestrictedAmount() {
    return min([
      $this->loan['ltv']['high']['affordable'],
      $this->loan['ltv']['high']['multiple'],
      $this->loan['ltv']['high']['ltv'],
    ]);
  }

  /**
   * Calculate higher restricted amount.
   */
  public function higherRestrictedAmount() {
    return min([
      $this->loan['ltv']['higher']['affordable'],
      $this->loan['ltv']['higher']['multiple'],
    ]);
  }

  /**
   * Calculate max amount LTV (percentage)
   */
  public function maxAmountLtvPercentage($a) {
    if (($this->mortgage['deposit'] + $this->loan['ltv'][$a]['restricted']) == 0) {
      return '';
    }
    return $this->loan['ltv'][$a]['restricted'] / ($this->loan['ltv'][$a]['restricted'] + $this->mortgage['deposit']);
  }

  // // Calculate maximum loan amount
  // public function maxLoanAmount()
  // {
  // if ($this->mortgage['buyer_type'] == 'remortgage' && $this->mortgage['extra'] == 0) {
  // return floor($this->loan['ltv']['p4p']['restricted']);
  // }
  // if ($this->loan['ltv']['higher']['percentage'] > 0.9) {
  // return floor($this->loan['ltv']['higher']['restricted']);
  // }
  // if ($this->gross['total'] >= 45000 && $this->loan['ltv']['high']['percentage'] > 0.75) {
  // return floor($this->loan['ltv']['high']['restricted']);
  // }
  // if ($this->gross['total'] >= 100000) {
  // return floor($this->loan['ltv']['low']['restricted']);
  // }
  // //    if ($this->gross['total'] >= 45000 && $this->loan['ltv']['high']['percentage'] > $this->loan['ltv']['high']['higher']) {
  // if ($this->gross['total'] >= 45000 && $this->loan['ltv']['high']['percentage'] > $this->loan['ltv']['higher']['percentage']) {
  // return floor($this->loan['ltv']['high']['restricted']);
  // }
  // return floor($this->loan['ltv']['higher']['restricted']);
  // }

  /**
   * {@inheritdoc}
   */
  public function maxLoanAmount() {

    if (($this->mortgage['buyer_type'] == 'remortgage') && ($this->mortgage['extra'] == 0)) {
      $result = $this->loan['ltv']['p4p']['restricted'];
    }
    elseif ($this->loan['ltv']['higher']['percentage'] > 0.9) {
      $result = $this->loan['ltv']['higher']['restricted'];
    }
    elseif (($this->gross['total'] >= 45000) && ($this->loan['ltv']['high']['percentage'] > 0.75)) {
      $result = $this->loan['ltv']['high']['restricted'];
    }
    elseif ($this->gross['total'] >= 100000) {
      $result = $this->loan['ltv']['low']['restricted'];
    }
    elseif (($this->gross['total'] >= 45000) && ($this->loan['ltv']['high']['percentage'] > $this->loan['ltv']['higher']['percentage'])) {
      $result = $this->loan['ltv']['high']['restricted'];
    }
    else {
      $result = $this->loan['ltv']['higher']['restricted'];
    }

    return floor($result);
  }

}
