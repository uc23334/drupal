<?php

namespace Drupal\santander_mortgage_calculator;

use Drupal\santander_mortgage_calculator\RateCards\BorrowAdditionalResultLoanV1;
use Drupal\santander_mortgage_calculator\RateCards\BorrowAdditionalResultLoanV2;
use Drupal\santander_mortgage_calculator\RateCards\BorrowAdditionalResultLoanV3;

/**
 * Loan model.
 */
class BorrowAdditionalResultLoan {

  /**
   * {@inheritdoc}
   */
  private $version;

  /**
   * {@inheritdoc}
   *
   * Ratecard Constants/Data/Values.
   */
  private $ltv;

  /**
   * {@inheritdoc}
   */
  private $minAmountPolicy;

  /**
   * {@inheritdoc}
   */
  private $secondaryWeighting;

  /**
   * {@inheritdoc}
   *
   * Mortgage.
   */
  private $mortgage = NULL;

  /**
   * {@inheritdoc}
   *
   * Income.
   */
  private $income = NULL;

  /**
   * {@inheritdoc}
   *
   * Commitments.
   */
  private $commitments = NULL;

  /**
   * {@inheritdoc}
   *
   * Questions.
   */
  private $questions = NULL;

  /**
   * {@inheritdoc}
   *
   * Gross.
   */
  private $gross = NULL;

  /**
   * {@inheritdoc}
   *
   * Expenditure.
   */
  private $expenditure = NULL;

  /**
   * {@inheritdoc}
   *
   * Credit.
   */
  private $credit = NULL;

  /**
   * {@inheritdoc}
   *
   * Debt consolidation.
   */
  protected $debtConsolidation = NULL;

  /**
   * {@inheritdoc}
   *
   * LTV restriction.
   */
  protected $ltvRestriction = NULL;

  /**
   * {@inheritdoc}
   *
   * Loan result.
   */
  protected $loan = [
    // £
    'allowable' => 0,
    'propertyValue' => 0,
    'additionalLoanTerm' => 0,
    // £
    'disposable' => 0,
    'p1Amount' => 0,
    'p2Amount' => 0,
    'p3Amount' => 0,
    'p4Amount' => 0,
    'p5Amount' => 0,
    'p1Term' => 0,
    'p2Term' => 0,
    'p3Term' => 0,
    'p4Term' => 0,
    'p5Term' => 0,
    'p1Repayment' => 0,
    'p2Repayment' => 0,
    'p3Repayment' => 0,
    'p4Repayment' => 0,
    'p5Repayment' => 0,
    'totalAmount' => 0,
    'initialMaxLoanAmount' => 0,
    'ltv' => [
      'low' => [
        // £
        'affordable' => 0,
        // £
        'multiple' => 0,
        // £
        'ltv' => 0,
        // £
        'restricted' => 0,
        // %
        'percentage' => 0,
      ],
      'high' => [
        // £
        'affordable' => 0,
        // £
        'multiple' => 0,
        // £
        'ltv' => 0,
        // £
        'restricted' => 0,
        // %
        'percentage' => 0,
      ],
    ],
    // £
    'loan' => 0,
  ];

  /**
   * Constructor.
   */
  public function __construct($mortgage, $income, $commitments, $i, $e, $c, $questions, $version = 'v1') {
    $this->version = $version;

    $this->mortgage = $mortgage;
    $this->income = $income;
    $this->commitments = $commitments;
    $this->gross = $i;
    $this->expenditure = $e;
    $this->credit = $c;

    $this->loan['propertyValue'] = $questions['propertyValue'];
    $this->debtConsolidation = $questions['debtConsolidation'];

    $this->loan['additionalLoanTerm'] = $questions['newTerm'];

    $this->loan['p1Amount'] = $questions['loanp1amount'];
    $this->loan['p2Amount'] = $questions['loanp2amount'];
    $this->loan['p3Amount'] = $questions['loanp3amount'];
    $this->loan['p4Amount'] = $questions['loanp4amount'];
    $this->loan['p5Amount'] = $questions['loanp5amount'];

    $this->loan['p1Term'] = $questions['loanp1term'];
    $this->loan['p2Term'] = $questions['loanp2term'];
    $this->loan['p3Term'] = $questions['loanp3term'];
    $this->loan['p4Term'] = $questions['loanp4term'];
    $this->loan['p5Term'] = $questions['loanp5term'];

    $this->loan['p1Repayment'] = $questions['loanp1Repayment'];
    $this->loan['p2Repayment'] = $questions['loanp2Repayment'];
    $this->loan['p3Repayment'] = $questions['loanp3Repayment'];
    $this->loan['p4Repayment'] = $questions['loanp4Repayment'];
    $this->loan['p5Repayment'] = $questions['loanp5Repayment'];

    // Load the ratecard data from the appropriate ratecard.
    if ($version == 'v1') {
      $ratecard = new BorrowAdditionalResultLoanV1();
    }
    elseif ($version == 'v3') {
      $ratecard = new BorrowAdditionalResultLoanV3();
    }
    else {
      $ratecard = new BorrowAdditionalResultLoanV2();
    }

    $this->ltv = $ratecard->ltv;
    $this->secondaryWeighting = $ratecard->secondaryWeighting;
    $this->minAmountPolicy = $ratecard->minAmountPolicy;
  }

  /**
   * Calculate max loan.
   */
  public function getLoan() {
    $this->loan['totalAmount'] = $this->totalpartsLoansLoans();
    $this->loan['allowable'] = $this->allowableIncome();
    $this->loan['disposable'] = $this->disposableIncome();

    $this->ltvRestriction = $this->ltvRestriction();

    $this->loan['ltv']['low']['affordable'] = $this->affordable('low');
    $this->loan['ltv']['low']['multiple'] = $this->maxAmountMultiple('low');
    $this->loan['ltv']['low']['ltv'] = $this->lowMaxAmountLtv();
    $this->loan['ltv']['low']['restricted'] = $this->restrictedAmount('low');
    $this->loan['ltv']['low']['percentage'] = $this->maxAmountLtvPercentage('low');

    $this->loan['ltv']['high']['affordable'] = $this->affordable('high');
    $this->loan['ltv']['high']['multiple'] = $this->maxAmountMultiple('high');
    $this->loan['ltv']['high']['ltv'] = $this->highMaxAmountLtv();
    $this->loan['ltv']['high']['restricted'] = $this->restrictedAmount('high');
    $this->loan['ltv']['high']['percentage'] = $this->maxAmountLtvPercentage('high');

    $this->loan['ltv']['higher']['affordable'] = $this->affordable('higher');
    $this->loan['ltv']['higher']['multiple'] = $this->maxAmountMultiple('higher');
    $this->loan['ltv']['higher']['ltv'] = $this->higherMaxAmountLtv();
    $this->loan['ltv']['higher']['restricted'] = $this->restrictedAmount('higher');
    $this->loan['ltv']['higher']['percentage'] = $this->maxAmountLtvPercentage('higher');

    $this->loan['initialMaxLoanAmount'] = $this->initialMaxLoanAmount();
    $this->loan['loan'] = $this->maxLoanAmount();

    return $this->loan;
  }

  // // Calculate allowable income
  // public function allowableIncome()
  // {
  // return array_sum(array(
  // $this->income['basic']['a1']['taxable'],
  // $this->income['basic']['a2']['taxable'],
  // $this->income['guaranteed']['a1']['taxable'],
  // $this->income['guaranteed']['a2']['taxable'],
  // $this->income['guaranteed']['a1']['nonTaxable'],
  // $this->income['guaranteed']['a2']['nonTaxable']
  // )) + $this->secondaryWeighting * 0.5 * array_sum(array(
  // $this->income['regular']['a1']['taxable'],
  // $this->income['regular']['a1']['nonTaxable'],
  // $this->income['regular']['a2']['taxable'],
  // $this->income['regular']['a2']['nonTaxable']
  // ));
  // }

  /**
   * Calculate allowable income.
   */
  public function allowableIncome() {
    $p1 = array_sum([
      $this->income['basic']['a1']['taxable'],
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['guaranteed']['a1']['nonTaxable'],
      $this->income['guaranteed']['a2']['nonTaxable'],
    ]);

    $p2 = array_sum([
      $this->income['regular']['a1']['taxable'],
      $this->income['regular']['a1']['nonTaxable'],
      $this->income['regular']['a2']['taxable'],
      $this->income['regular']['a2']['nonTaxable'],
    ]);

    if ($this->version === 'v1') {
      return $p1 + ($this->secondaryWeighting * 0.5 * $p2);
    }
    else {
      return $p1 + ($this->secondaryWeighting * $p2);
    }
  }

  /**
   * Calculate total parts amount.
   */
  public function totalpartsLoansLoans() {
    return $this->loan['p1Amount'] + $this->loan['p2Amount'] + $this->loan['p3Amount'] + $this->loan['p4Amount'] + $this->loan['p5Amount'];
  }

  /**
   * Calculate disposable income.
   */
  public function disposableIncome() {
    return $this->gross['totalMonthly'] - $this->expenditure['final'] - $this->credit - $this->commitments['outgoings'];
  }

  /**
   * Calculate Existing loan part 1.
   */
  public function loan1($a) {
    if ($this->loan['p1Amount'] == 0 || $this->loan['p1Term'] == 0 || $this->loan['p1Repayment'] === 0) {
      return 0;
    }
    if ($this->loan['p1Repayment'] == "IO") {
      return ($this->loan['p1Amount'] * $this->ltv[$a]['rate'] / 12) / (1 - pow((1 + ($this->ltv[$a]['rate'] / 12)), (-1 * (max($this->loan['p1Term'], 300)))));
    }
    return $this->loan['p1Amount'] * ($this->ltv[$a]['rate'] / 12) / (1 - (pow(1 + ($this->ltv[$a]['rate'] / 12), (-1 * ($this->loan['p1Term'])))));
  }

  /**
   * Calculate Existing loan part 2.
   */
  public function loan2($a) {
    if ($this->loan['p2Amount'] == 0 || $this->loan['p2Term'] == 0 || $this->loan['p2Repayment'] === 0) {
      return 0;
    }
    if ($this->loan['p2Repayment'] == "IO") {
      return ($this->loan['p2Amount'] * $this->ltv[$a]['rate'] / 12) / (1 - pow((1 + ($this->ltv[$a]['rate'] / 12)), (-1 * (max($this->loan['p2Term'], 300)))));
    }
    return $this->loan['p2Amount'] * ($this->ltv[$a]['rate'] / 12) / (1 - (pow(1 + ($this->ltv[$a]['rate'] / 12), (-1 * ($this->loan['p2Term'])))));
  }

  /**
   * Calculate Existing loan part 3.
   */
  public function loan3($a) {
    if ($this->loan['p3Amount'] == 0 || $this->loan['p3Term'] == 0 || $this->loan['p3Repayment'] === 0) {
      return 0;
    }
    if ($this->loan['p3Repayment'] == "IO") {
      return ($this->loan['p3Amount'] * $this->ltv[$a]['rate'] / 12) / (1 - pow((1 + ($this->ltv[$a]['rate'] / 12)), (-1 * (max($this->loan['p3Term'], 300)))));
    }
    return $this->loan['p3Amount'] * ($this->ltv[$a]['rate'] / 12) / (1 - (pow(1 + ($this->ltv[$a]['rate'] / 12), (-1 * ($this->loan['p3Term'])))));
  }

  /**
   * Calculate low Existing loan part 4.
   */
  public function loan4($a) {
    if ($this->loan['p4Amount'] == 0 || $this->loan['p4Term'] == 0 || $this->loan['p4Repayment'] === 0) {
      return 0;
    }
    if ($this->loan['p4Repayment'] == "IO") {
      return ($this->loan['p4Amount'] * $this->ltv[$a]['rate'] / 12) / (1 - pow((1 + ($this->ltv[$a]['rate'] / 12)), (-1 * (max($this->loan['p4Term'], 300)))));
    }
    return $this->loan['p4Amount'] * ($this->ltv[$a]['rate'] / 12) / (1 - (pow(1 + ($this->ltv[$a]['rate'] / 12), (-1 * ($this->loan['p4Term'])))));
  }

  /**
   * Calculate low Existing loan part 5.
   */
  public function loan5($a) {
    if ($this->loan['p5Amount'] == 0 || $this->loan['p5Term'] == 0 || $this->loan['p5Repayment'] === 0) {
      return 0;
    }
    if ($this->loan['p5Repayment'] == "IO") {
      return ($this->loan['p5Amount'] * $this->ltv[$a]['rate'] / 12) / (1 - pow((1 + ($this->ltv[$a]['rate'] / 12)), (-1 * (max($this->loan['p5Term'], 300)))));
    }
    return $this->loan['p5Amount'] * ($this->ltv[$a]['rate'] / 12) / (1 - (pow(1 + ($this->ltv[$a]['rate'] / 12), (-1 * ($this->loan['p5Term'])))));
  }

  /**
   * Calculate low/high Existing loan total.
   */
  public function totalLoans($a) {
    return $this->loan1($a) + $this->loan2($a) + $this->loan3($a) + $this->loan4($a) + $this->loan5($a);
  }

  /**
   * Calculate high LTV max amount affordable.
   */
  public function affordable($a) {
    $totalStressedPayments = $this->totalLoans($a);
    return max(($this->loan['disposable'] - $totalStressedPayments) * (1 - pow(1 + ($this->ltv[$a]['rate'] / 12), -1 * $this->loan['additionalLoanTerm'])) / ($this->ltv[$a]['rate'] / 12), 0);
  }

  /**
   * Calculate max amount multiple.
   */
  public function maxAmountMultiple($a) {
    if ($this->mortgage['applicants'] == 1) {
      $maxAmount = $this->ltv[$a]['single'] * $this->loan['allowable'] - $this->loan['totalAmount'];
    }
    else {
      $maxAmount = $this->ltv[$a]['joint'] * $this->loan['allowable'] - $this->loan['totalAmount'];
    }
    return max($maxAmount, 0);
  }

  /**
   * Calculate low max amount LTV.
   */
  public function lowMaxAmountLtv() {
    if ($this->version == 'v1') {
      // V1.
      return max((0.75 * $this->loan['propertyValue']) - $this->loan['totalAmount'], 0);
    }
    else {
      // V2.
      return max((0.85 * $this->loan['propertyValue']) - $this->loan['totalAmount'], 0);
    }
  }

  /**
   * Calculate high max amount LTV.
   */
  public function highMaxAmountLtv() {
    return max((0.85 * $this->loan['propertyValue']) - $this->loan['totalAmount'], 0);
  }

  /**
   * Calculate higher max amount LTV.
   */
  public function higherMaxAmountLtv() {
    return max((0.85 * $this->loan['propertyValue']) - $this->loan['totalAmount'], 0);
  }

  /**
   * Calculate  restricted amount.
   */
  public function restrictedAmount($a) {
    return min($this->loan['ltv'][$a]['affordable'], $this->loan['ltv'][$a]['multiple'], $this->loan['ltv'][$a]['ltv']);
  }

  /**
   * Calculate max amount LTV (percentage)
   */
  public function maxAmountLtvPercentage($a) {
    if ($this->loan['propertyValue'] == 0) {
      return '';
    }
    return ($this->loan['ltv'][$a]['restricted'] + $this->loan['totalAmount']) / $this->loan['propertyValue'];
  }

  /**
   * Calculate Initial Maximum Loan Amount.
   */
  public function initialMaxLoanAmount() {
    if ($this->loan['ltv']['high']['percentage'] > 0.75 && $this->gross['total'] >= 45000) {
      return floor($this->loan['ltv']['high']['restricted']);
    }
    if ($this->loan['ltv']['higher']['percentage'] >= 0.75) {
      return floor($this->loan['ltv']['higher']['restricted']);
    }
    if ($this->gross['total'] >= 100000) {
      return floor($this->loan['ltv']['low']['restricted']);
    }
    if ($this->gross['total'] >= 45000 && $this->loan['ltv']['high']['percentage'] > $this->loan['ltv']['higher']['percentage']) {
      return floor($this->loan['ltv']['high']['restricted']);
    }
    return floor($this->loan['ltv']['higher']['restricted']);
  }

  /**
   * LTV Restriction.
   */
  public function ltvRestriction() {
    return floor(max(0.85 * $this->loan['propertyValue'] - $this->loan['totalAmount'], 0));
  }

  /**
   * Debt Consolidation Restriction.
   */
  public function debtConsolidationRestriction() {
    return min(35000, max(0.35 / 0.65 * $this->totalpartsLoansLoans(), 0.5 * $this->loan['propertyValue'] - $this->totalpartsLoansLoans()));
  }

  /**
   * Maximum Loan Amount within Policy.
   */
  public function maxAmountPolicy() {
    if ($this->debtConsolidation == 'yes') {
      return floor(min($this->loan['initialMaxLoanAmount'], $this->ltvRestriction, $this->specialFunctionC39(), $this->specialFunctionC40(), $this->debtConsolidationRestriction()));
    }
    return floor(min($this->loan['initialMaxLoanAmount'], $this->ltvRestriction, $this->specialFunctionC39(), $this->specialFunctionC40()));
  }

  // For all future developers, don't try to understand these two functions.

  /**
   * They represent 2 conditions added to the client's spreadsheet.
   */
  public function specialFunctionC39() {
    $sum = 0;
    // Sum up any interest only loan parts.
    for ($i = 1; $i <= 5; $i++) {
      if ($this->loan['p' . $i . 'Amount'] == 0 || $this->loan['p' . $i . 'Term'] == 0 || $this->loan['p' . $i . 'Repayment'] === 0) {
        continue;
      }
      if ($this->loan['p' . $i . 'Repayment'] == 'IO') {
        $sum += $this->loan['p' . $i . 'Amount'];
      }
    }

    // If there was any interest only part.
    if ($sum) {
      return $this->lowMaxAmountLtv();
    }
    else {
      return $this->highMaxAmountLtv();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function specialFunctionC40() {
    $sum = 0;
    // Sum up any interest only loan parts.
    for ($i = 1; $i <= 5; $i++) {
      if ($this->loan['p' . $i . 'Amount'] == 0 || $this->loan['p' . $i . 'Term'] == 0 || $this->loan['p' . $i . 'Repayment'] === 0) {
        continue;
      }
      if ($this->loan['p' . $i . 'Repayment'] == 'IO') {
        $sum += $this->loan['p' . $i . 'Amount'];
      }
    }

    // If there was any interest only part.
    if (($sum / $this->loan['propertyValue']) > 0.5) {
      return $this->lowMaxAmountLtv();
    }
    else {
      return $this->loan['initialMaxLoanAmount'];
    }
  }

  /**
   * Calculate maximum loan amount.
   */
  public function maxLoanAmount() {
    $maxAmountPolicy = $this->maxAmountPolicy();
    if ($maxAmountPolicy < $this->minAmountPolicy) {
      return 0;
    }
    return $maxAmountPolicy;
  }

}
