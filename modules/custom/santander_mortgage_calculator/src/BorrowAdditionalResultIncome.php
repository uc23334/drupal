<?php

namespace Drupal\santander_mortgage_calculator;

use Drupal\santander_mortgage_calculator\RateCards\BorrowAdditionalResultIncomeV1;
use Drupal\santander_mortgage_calculator\RateCards\BorrowAdditionalResultIncomeV2;
use Drupal\santander_mortgage_calculator\RateCards\BorrowAdditionalResultIncomeV3;

/**
 * {@inheritdoc}
 */
class BorrowAdditionalResultIncome {

  /**
   * {@inheritdoc}
   *
   * Ratecard Constants/Data/Values.
   */
  private $taxFreeAllowance;

  /**
   * {@inheritdoc}
   */
  private $rates;

  /**
   * {@inheritdoc}
   */
  private $primaryNiThreshold;

  /**
   * {@inheritdoc}
   */
  private $upperEarningsNiLimit;

  /**
   * {@inheritdoc}
   */
  private $scalingFactor;

  /**
   * {@inheritdoc}
   */
  private $secondaryWeighting;

  /**
   * {@inheritdoc}
   *
   * Income.
   */
  private $income = NULL;

  /**
   * {@inheritdoc}
   *
   * Gross.
   */
  private $gross = [
    'a1' => [
      'taxable' => 0,
      'taxFreeAllowance' => 0,
      'tax10' => 0,
      'tax20' => 0,
      'tax40' => 0,
      'tax45' => 0,
      'ni2' => 0,
      'ni12' => 0,
      'div10' => 0,
      'div325' => 0,
      'div375' => 0,
      'annual' => [
        'taxable' => 0,
        'nonTaxable' => 0,
        'dividends' => 0,
      ],
      'monthly' => [
        'taxable' => [
          'total' => 0,
      // %
          'basic' => 0,
      // %
          'guaranteed' => 0,
      // %
          'regular' => 0,
          'reduced' => 0,
        ],
        'nonTaxable' => [
          'total' => 0,
        // %
          'basic' => 0,
        // %
          'guaranteed' => 0,
        // %
          'regular' => 0,
          'reduced' => 0,
        ],
        'total' => 0,
      ],
      'total' => 0,
    ],
    'a2' => [
      'taxable' => 0,
      'taxFreeAllowance' => 0,
      'tax10' => 0,
      'tax20' => 0,
      'tax40' => 0,
      'tax45' => 0,
      'ni2' => 0,
      'ni12' => 0,
      'div10' => 0,
      'div325' => 0,
      'div375' => 0,
      'annual' => [
        'taxable' => 0,
        'nonTaxable' => 0,
        'dividends' => 0,
      ],
      'monthly' => [
        'taxable' => [
          'total' => 0,
      // %
          'basic' => 0,
      // %
          'guaranteed' => 0,
      // %
          'regular' => 0,
          'reduced' => 0,
        ],
        'nonTaxable' => [
          'total' => 0,
        // %
          'basic' => 0,
        // %
          'guaranteed' => 0,
        // %
          'regular' => 0,
          'reduced' => 0,
        ],
        'total' => 0,
      ],
      'total' => 0,
    ],
    'total' => 0,
    'totalMonthly' => 0,
  ];

  /**
   * Constructor.
   */
  public function __construct(array $arr, $version = 'v1') {

    $this->income = $arr;

    // Load the ratecard data from the appropriate ratecard.
    if ($version == 'v1') {
      $ratecard = new BorrowAdditionalResultIncomeV1();
    }
    elseif ($version == 'v3') {
      $ratecard = new BorrowAdditionalResultIncomeV3();
    }
    else {
      $ratecard = new BorrowAdditionalResultIncomeV2();
    }

    $this->taxFreeAllowance = $ratecard->taxFreeAllowance;
    $this->rates = $ratecard->rates;
    $this->primaryNiThreshold = $ratecard->primaryNiThreshold;
    $this->upperEarningsNiLimit = $ratecard->upperEarningsNiLimit;
    $this->scalingFactor = $ratecard->scalingFactor;
    $this->secondaryWeighting = $ratecard->secondaryWeighting;
  }

  /**
   * Calculate Total NET monthly income.
   */
  public function getIncome() {

    // Calculate gross income for applicant.
    $this->gross['a1']['total'] = $this->grossApplicant([
      $this->income['basic']['a1']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['guaranteed']['a1']['nonTaxable'],
      $this->income['regular']['a1']['taxable'],
      $this->income['regular']['a1']['nonTaxable'],
    ]);

    $this->gross['a2']['total'] = $this->grossApplicant([
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['guaranteed']['a2']['nonTaxable'],
      $this->income['regular']['a2']['taxable'],
      $this->income['regular']['a2']['nonTaxable'],
    ]);

    // Calculate total gross income.
    $this->gross['total'] = $this->totalGross();

    // Calculate total gross taxable income for applicant.
    $this->gross['a1']['taxable'] = $this->grossTaxableApplicant([
      $this->income['basic']['a1']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['regular']['a1']['taxable'],
    ], 'a1');

    $this->gross['a2']['taxable'] = $this->grossTaxableApplicant([
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['regular']['a2']['taxable'],
    ], 'a2');

    // Calculate final tax free allowance.
    $this->gross['a1']['taxFreeAllowance'] = $this->finalTaxFreeAllowance('a1');
    $this->gross['a2']['taxFreeAllowance'] = $this->finalTaxFreeAllowance('a2');
    // Calculate tax at 10%.
    $this->gross['a1']['tax10'] = $this->tax10('a1');
    $this->gross['a2']['tax10'] = $this->tax10('a2');
    // Calculate tax at 20%.
    $this->gross['a1']['tax20'] = $this->tax20('a1');
    $this->gross['a2']['tax20'] = $this->tax20('a2');
    // Calculate tax at 40%.
    $this->gross['a1']['tax40'] = $this->tax40('a1');
    $this->gross['a2']['tax40'] = $this->tax40('a2');
    // Calculate tax at 45%.
    $this->gross['a1']['tax45'] = $this->tax45('a1');
    $this->gross['a2']['tax45'] = $this->tax45('a2');
    // Calculate NI at 12%.
    $this->gross['a1']['ni12'] = $this->ni12('a1');
    $this->gross['a2']['ni12'] = $this->ni12('a2');
    // Calculate NI at 2%.
    $this->gross['a1']['ni2'] = $this->ni2('a1');
    $this->gross['a2']['ni2'] = $this->ni2('a2');

    // Calculate Net annual income (taxable)
    $this->gross['a1']['annual']['taxable'] = $this->annualTaxable('a1');
    $this->gross['a2']['annual']['taxable'] = $this->annualTaxable('a2');
    // Calculate Net annual income (non taxable)
    $this->gross['a1']['annual']['nonTaxable'] = $this->annualNonTaxable([
      $this->income['guaranteed']['a1']['nonTaxable'],
      $this->income['regular']['a1']['nonTaxable'],
    ]);
    $this->gross['a2']['annual']['nonTaxable'] = $this->annualNonTaxable([
      $this->income['guaranteed']['a2']['nonTaxable'],
      $this->income['regular']['a2']['nonTaxable'],
    ]);
    // Calculate Net monthly income (taxable)
    $this->gross['a1']['monthly']['taxable']['total'] = $this->monthlyTaxable('a1');
    $this->gross['a2']['monthly']['taxable']['total'] = $this->monthlyTaxable('a2');
    // Calculate taxable proportion (%)
    $this->gross['a1']['monthly']['taxable']['basic'] = $this->taxableProportion([
      $this->income['basic']['a1']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['regular']['a1']['taxable'],
    ], $this->income['basic']['a1']['taxable']);
    $this->gross['a1']['monthly']['taxable']['guaranteed'] = $this->taxableProportion([
      $this->income['basic']['a1']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['regular']['a1']['taxable'],
    ], $this->income['guaranteed']['a1']['taxable']);
    $this->gross['a1']['monthly']['taxable']['regular'] = $this->taxableProportion([
      $this->income['basic']['a1']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['regular']['a1']['taxable'],
    ], $this->income['regular']['a1']['taxable']);
    $this->gross['a2']['monthly']['taxable']['basic'] = $this->taxableProportion([
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['regular']['a2']['taxable'],
    ], $this->income['basic']['a2']['taxable']);
    $this->gross['a2']['monthly']['taxable']['guaranteed'] = $this->taxableProportion([
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['regular']['a2']['taxable'],
    ], $this->income['guaranteed']['a2']['taxable']);
    $this->gross['a2']['monthly']['taxable']['regular'] = $this->taxableProportion([
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['regular']['a2']['taxable'],
    ], $this->income['regular']['a2']['taxable']);
    // Calculate Net monthly income (taxable reduced)
    $this->gross['a1']['monthly']['taxable']['reduced'] = $this->monthlyTaxableReduced('a1');
    $this->gross['a2']['monthly']['taxable']['reduced'] = $this->monthlyTaxableReduced('a2');
    // Calculate Net monthly income (non taxable)
    $this->gross['a1']['monthly']['nonTaxable']['total'] = $this->monthlyNonTaxable('a1');
    $this->gross['a2']['monthly']['nonTaxable']['total'] = $this->monthlyNonTaxable('a2');
    // Calculate Gross non taxable proportion (%)
    $this->gross['a1']['monthly']['nonTaxable']['guaranteed'] = $this->nonTaxableProportion('a1', $this->income['guaranteed']['a1']['nonTaxable']);
    $this->gross['a1']['monthly']['nonTaxable']['regular'] = $this->nonTaxableProportion('a1', $this->income['regular']['a1']['nonTaxable']);
    $this->gross['a2']['monthly']['nonTaxable']['guaranteed'] = $this->nonTaxableProportion('a2', $this->income['guaranteed']['a2']['nonTaxable']);
    $this->gross['a2']['monthly']['nonTaxable']['regular'] = $this->nonTaxableProportion('a2', $this->income['regular']['a2']['nonTaxable']);
    // Calculate Net monthly income (non taxable reduced)
    $this->gross['a1']['monthly']['nonTaxable']['reduced'] = $this->monthlyNonTaxableReduced('a1');
    $this->gross['a2']['monthly']['nonTaxable']['reduced'] = $this->monthlyNonTaxableReduced('a2');
    // Calculate total Net monthly income per applicant.
    $this->gross['a1']['monthly']['total'] = $this->totalMonthlyIncomeApplicant('a1');
    $this->gross['a2']['monthly']['total'] = $this->totalMonthlyIncomeApplicant('a2');
    $this->gross['totalMonthly'] = $this->totalMonthlyIncome();

    return $this->gross;
  }

  /**
   * Calculate gross income for applicant.
   */
  public function grossApplicant(array $arr) {
    return array_sum($arr);
  }

  /**
   * Calculate total gross income.
   */
  public function totalGross() {
    return array_sum([
      $this->gross['a1']['total'],
      $this->gross['a2']['total'],
    ]);
  }

  /**
   * Calculate total gross taxable income for applicant.
   */
  public function grossTaxableApplicant(array $arr, $applicant = NULL) {
    $result = BorrowAdditionalResult::sanMax([array_sum($arr)]);
    if (isset($this->income['deduction'])) {
      return $result - (BorrowAdditionalResult::roundUp($this->income['deduction'][$applicant]['taxFree'], 2) * 12);
    }
    else {
      return $result;
    }
  }

  /**
   * Calculate final tax free allowance.
   */
  public function finalTaxFreeAllowance($a) {
    if (isset($this->income['dividends'])) {
      return BorrowAdditionalResult::sanMax([
        $this->taxFreeAllowance - (0.5 * BorrowAdditionalResult::sanMax([
          ($this->gross[$a]['taxable'] + (10 / 9) * $this->income['dividends'][$a]) - 100000,
        ])),
      ]);
    }
    else {
      return BorrowAdditionalResult::sanMax([
        $this->taxFreeAllowance - (0.5 * BorrowAdditionalResult::sanMax([
          $this->gross[$a]['taxable'] - 100000,
        ])),
      ]);
    }
  }

  /**
   * Calculate tax at 10%.
   */
  public function tax10($a) {
    if ($this->gross[$a]['taxable'] > $this->gross[$a]['taxFreeAllowance']) {
      if (($this->gross[$a]['taxable'] - $this->gross[$a]['taxFreeAllowance']) > $this->rates['starting']['high']) {
        return $this->rates['starting']['high'] * $this->rates['starting']['rate'];
      }
      else {
        return ($this->gross[$a]['taxable'] - $this->gross[$a]['taxFreeAllowance']) * $this->rates['starting']['rate'];
      }
    }
    else {
      return 0;
    }
  }

  /**
   * Calculate tax at 20%.
   */
  public function tax20($a) {
    if ($this->gross[$a]['taxable'] > $this->gross[$a]['taxFreeAllowance']) {
      if (($this->gross[$a]['taxable'] - $this->gross[$a]['taxFreeAllowance']) > $this->rates['basic']['high']) {
        return $this->rates['basic']['high'] * $this->rates['basic']['rate'];
      }
      else {
        return ($this->gross[$a]['taxable'] - $this->gross[$a]['taxFreeAllowance']) * $this->rates['basic']['rate'];
      }
    }
    else {
      return 0;
    }
  }

  /**
   * Calculate tax at 40%.
   */
  public function tax40($a) {
    if ($this->gross[$a]['taxable'] > ($this->gross[$a]['taxFreeAllowance'] + $this->rates['basic']['high'])) {
      if (($this->gross[$a]['taxable'] - $this->gross[$a]['taxFreeAllowance'] - $this->rates['basic']['high']) > ($this->rates['higher']['high'] - $this->rates['basic']['high'])) {
        return ($this->rates['higher']['high'] - $this->rates['basic']['high']) * $this->rates['higher']['rate'];
      }
      else {
        return ($this->gross[$a]['taxable'] - $this->gross[$a]['taxFreeAllowance'] - $this->rates['basic']['high']) * $this->rates['higher']['rate'];
      }
    }
    else {
      return 0;
    }
  }

  /**
   * Calculate tax at 45%.
   */
  public function tax45($a) {
    if ($this->gross[$a]['taxable'] > ($this->rates['higher']['high'] + $this->gross[$a]['taxFreeAllowance'])) {
      return ($this->gross[$a]['taxable'] - $this->rates['higher']['high'] - $this->gross[$a]['taxFreeAllowance']) * $this->rates['additional']['rate'];
    }
    else {
      return 0;
    }
  }

  /**
   * Calculate NI at 2%.
   */
  public function ni2($a) {
    if (isset($this->income['pension'])) {
      $ni = $this->gross[$a]['taxable'] - $this->income['pension'][$a];
    }
    else {
      $ni = $this->gross[$a]['taxable'];
    }
    if ($ni > $this->primaryNiThreshold['high']) {
      return ($ni - $this->primaryNiThreshold['high']) * $this->upperEarningsNiLimit['rate'];
    }
    else {
      return 0;
    }
  }

  /**
   * Calculate NI at 12%.
   */
  public function ni12($a) {
    if (isset($this->income['pension'])) {
      $ni = $this->gross[$a]['taxable'] - $this->income['pension'][$a];
    }
    else {
      $ni = $this->gross[$a]['taxable'];
    }
    if ($ni > $this->primaryNiThreshold['low']) {
      if ($ni > $this->primaryNiThreshold['high']) {
        return ($this->primaryNiThreshold['high'] - $this->primaryNiThreshold['low']) * $this->primaryNiThreshold['rate'];
      }
      else {
        return ($ni - $this->primaryNiThreshold['low']) * $this->primaryNiThreshold['rate'];
      }
    }
    else {
      return 0;
    }
  }

  /**
   * Calculate Net annual income (taxable)
   */
  public function annualTaxable($a) {
    return $this->gross[$a]['taxable'] - array_sum([
      $this->gross[$a]['tax10'],
      $this->gross[$a]['tax20'],
      $this->gross[$a]['tax40'],
      $this->gross[$a]['tax45'],
      $this->gross[$a]['ni12'],
      $this->gross[$a]['ni2'],
    ]);
  }

  /**
   * Calculate Net annual income (non taxable)
   */
  public function annualNonTaxable(array $arr) {
    return array_sum($arr);
  }

  /**
   * Calculate Net monthly income (taxable)
   */
  public function monthlyTaxable($a) {
    return BorrowAdditionalResult::sanMax([
      BorrowAdditionalResult::sanMax([
        $this->gross[$a]['annual']['taxable'] / 12,
      ]),
    ]);
  }

  /**
   * Calculate Gross taxable proportion (%)
   */
  public function taxableProportion(array $arr, $a) {
    $v = array_sum($arr);
    if ($v == 0) {
      return 0;
    }
    else {
      return $a / $v;
    }
  }

  /**
   * Calculate Net monthly income (taxable reduced)
   */
  public function monthlyTaxableReduced($a) {
    return array_sum([
      $this->gross[$a]['monthly']['taxable']['basic'],
      $this->gross[$a]['monthly']['taxable']['guaranteed'],
      $this->gross[$a]['monthly']['taxable']['regular'] * $this->secondaryWeighting,
    ]) * $this->gross[$a]['monthly']['taxable']['total'];
  }

  /**
   * Calculate Net monthly income (non taxable)
   */
  public function monthlyNonTaxable($a) {
    if (isset($this->income['dividends'])) {
      return BorrowAdditionalResult::sanMax([
        ($this->gross[$a]['annual']['nonTaxable'] / 12) - ($this->annualDividendTax($a) / 12),
      ]);
    }
    else {
      return BorrowAdditionalResult::sanMax([
        ($this->gross[$a]['annual']['nonTaxable'] / 12),
      ]);
    }
  }

  /**
   * Calculate Gross non taxable proportion (%)
   */
  public function nonTaxableProportion($a, $b) {
    if ($this->gross[$a]['annual']['nonTaxable'] == 0) {
      return 0;
    }
    else {
      return $b / $this->gross[$a]['annual']['nonTaxable'];
    }
  }

  /**
   * Calculate Net monthly income (non taxable reduced)
   */
  public function monthlyNonTaxableReduced($a) {
    return array_sum([
      $this->gross[$a]['monthly']['nonTaxable']['basic'],
      $this->gross[$a]['monthly']['nonTaxable']['guaranteed'],
      $this->gross[$a]['monthly']['nonTaxable']['regular'] * $this->secondaryWeighting,
    ]) * $this->gross[$a]['monthly']['nonTaxable']['total'];
  }

  /**
   * Calculate total Net monthly income per applicant.
   */
  public function totalMonthlyIncomeApplicant($a) {
    return array_sum([
      $this->gross[$a]['monthly']['nonTaxable']['reduced'],
      $this->gross[$a]['monthly']['taxable']['reduced'],
    ]);
  }

  /**
   * Calculate total Net monthly income.
   */
  public function totalMonthlyIncome() {
    // Ensure there is no sclaing factor when it comes to the DIP calculator.
    $total = array_sum([
      $this->gross['a1']['monthly']['total'],
      $this->gross['a2']['monthly']['total'],
    ]);
    if (isset($this->income['deduction'])) {
      return BorrowAdditionalResult::roundUp($total, 2)
        - BorrowAdditionalResult::roundUp($this->income['deduction']['a1']['postTax'], 2)
        - BorrowAdditionalResult::roundUp($this->income['deduction']['a2']['postTax'], 2)
        - BorrowAdditionalResult::roundUp($this->income['deduction']['a1']['taxFree'], 2)
        - BorrowAdditionalResult::roundUp($this->income['deduction']['a2']['taxFree'], 2);
    }
    else {
      return BorrowAdditionalResult::roundUp($total) * $this->scalingFactor;
    }
  }

}
