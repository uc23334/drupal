<?php

namespace Drupal\santander_mortgage_calculator;

/**
 * {@inheritdoc}
 */
class BorrowResult {

  /**
   * {@inheritdoc}
   */
  private $version;

  /**
   * {@inheritdoc}
   */
  private $mortgage = [
    'applicants' => 1,
    'buyers' => '',
    'dependants' => 0,
    'term' => 25,
    'deposit' => 0,
  ];

  /**
   * {@inheritdoc}
   */
  private $income = [
    'basic' => [
      'a1' => ['taxable' => 0],
      'a2' => ['taxable' => 0],
    ],
    'guaranteed' => [
      'a1' => ['taxable' => 0, 'nonTaxable' => 0],
      'a2' => ['taxable' => 0, 'nonTaxable' => 0],
    ],
    'regular' => [
      'a1' => ['taxable' => 0, 'nonTaxable' => 0],
      'a2' => ['taxable' => 0, 'nonTaxable' => 0],
    ],
  ];

  /**
   * {@inheritdoc}
   */
  private $commitments = [
    'creditCard' => 0,
    'loans' => 0,
    'outgoings' => 0,
  ];

  /**
   * Constructor.
   */
  public function __construct($version = 'v1') {
    $this->version = $version;
  }

  /**
   * {@inheritdoc}
   */
  public function getVersion() {
    return $this->version;
  }

  /**
   * Set Params.
   *
   * Receives the data from the input form and sets up the income/outgoings and mortgage arrays.
   *
   * @param array $data
   *   Data.
   */
  public function setParams(array $data) {

    // The JS in the original Hogarth form always sends a '0' value for buyers - We dont have a field for buyers on the form so this was throwing warnings all the time
    // so we just set it to 'no'
    // $this->mortgage['buyers']                       = ($data['buyers'] === 'yes' || $data['buyers'] === '1' || $data['buyers'] === 1) ? 'yes' : 'no';.
    $this->mortgage['buyers'] = 'no';

    if (empty($data['same_borrowers'])) {
      $data['same_borrowers'] = 'yes';
    }
    else {
      $data['same_borrowers'] = trim($data['same_borrowers']);
      if ($data['same_borrowers'] == '-') {
        $data['same_borrowers'] = 'yes';
      }
      else {
        $data['same_borrowers'] = strtolower($data['same_borrowers']);
      }
    }

    $this->mortgage['same_borrowers'] = $data['same_borrowers'];
    $this->mortgage['applicants']     = $data['applicants'];
    $this->mortgage['dependants']     = ($data['dependants'] == '+3') ? 3 : $data['dependants'];
    $this->mortgage['term']           = $data['terms'];
    $this->mortgage['deposit']        = $data['deposit'];
    $this->mortgage['buyer_type']     = $data['buyer_type_select'];
    $this->mortgage['extra']          = isset($data['extra']) ? $data['extra'] : 0;

    $this->mortgage['current_mortgage_balance'] = isset($data['current_mortgage_balance']) ? $data['current_mortgage_balance'] : 0;

    // Applicant 1.
    $this->income['basic']['a1']['taxable'] = $data['income_applicant_1_annual_amount'];
    // Always 0 in the Hogarth version???
    $this->income['guaranteed']['a1']['taxable']    = 0;
    $this->income['guaranteed']['a1']['nonTaxable'] = $data['income_applicant_1_other_annual_amount'];
    $this->income['regular']['a1']['taxable']       = $data['income_applicant_1_extra_annual_amount'];
    $this->income['regular']['a1']['nonTaxable']    = $data['income_applicant_1_csa_monthly_amount'];

    // Applicant 2.
    if ($data['applicants'] == 2) {
      $this->income['basic']['a2']['taxable'] = $data['income_applicant_2_annual_amount'];
      // Always 0 in the Hogarth version???
      $this->income['guaranteed']['a2']['taxable']    = 0;
      $this->income['guaranteed']['a2']['nonTaxable'] = $data['income_applicant_2_other_annual_amount'];
      $this->income['regular']['a2']['taxable']       = $data['income_applicant_2_extra_annual_amount'];
      $this->income['regular']['a2']['nonTaxable']    = $data['income_applicant_2_csa_monthly_amount'];
    }
    else {
      $this->income['basic']['a2']['taxable']         = 0;
      $this->income['guaranteed']['a2']['taxable']    = 0;
      $this->income['guaranteed']['a2']['nonTaxable'] = 0;
      $this->income['regular']['a2']['taxable']       = 0;
      $this->income['regular']['a2']['nonTaxable']    = 0;
    }

    // Outgoings.
    $this->commitments['creditCard'] = $data['commitments_credit_card'];
    $this->commitments['loans']      = $data['commitments_loans'];
    $this->commitments['outgoings']  = $data['commitments_outgoings'];
  }

  // Validate the fields of the form.

  /**
   * This whole functions purpose is a bit suspect for web18.
   */
  public function validate(&$data) {

    $error = FALSE;

    // All the input fields of the form should be listed here so they can be converted/checked as required.
    $fields = [
      'applicants' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 1,
      ],
      // 'buyers'                                  => array('required' => true),
      'dependants' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 0,
        'max' => 4,
      ],
      'terms' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 5,
        'max' => 40,
      ],
      'deposit' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 0,
      ],
      'current_mortgage_balance' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 0,
      ],

      'income_applicant_1_annual_amount' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 0,
      ],
      'income_applicant_1_other_annual_amount' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 0,
      ],
      'income_applicant_1_extra_annual_amount' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 0,
      ],
      'income_applicant_1_csa_monthly_amount' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 0,
      ],

      'income_applicant_2_annual_amount' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 0,
      ],
      'income_applicant_2_other_annual_amount' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 0,
      ],
      'income_applicant_2_extra_annual_amount' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 0,
      ],
      'income_applicant_2_csa_monthly_amount' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 0,
      ],

      'commitments_credit_card' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 0,
      ],
      'commitments_loans' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 0,
      ],
      'commitments_outgoings' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 0,
      ],
    ];

    $invalid = [];

    // For each input field.
    foreach ($fields as $name => $fieldInfo) {

      // If its missing but required.
      if (!isset($data[$name]) && $fieldInfo['required']) {
        $invalid[$name] = 'required';
      }

      // If its supposed to be numeric.
      if (isset($fieldInfo['type']) && $fieldInfo['type'] == 'numeric') {

        // Make sure it is numeric.
        $data[$name] = floatval(preg_replace('/[^\d.]/', '', $data[$name]));

        // If it should have a minimum value.
        if (isset($fieldInfo['min']) && $data[$name] < $fieldInfo['min']) {
          $invalid[$name] = 'min';
        }

        // If it should have a maximum value.
        if (isset($fieldInfo['max']) && $data[$name] > $fieldInfo['max']) {
          $invalid[$name] = 'max';
        }
      }
    }

    if (count($invalid) > 0) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Calculate the result ie how much they can borrow.
   */
  public function calculate() {

    // Get the income of the applicants.
    $resultincome = new BorrowResultIncome($this->income, $this->version);

    $income = $resultincome->getIncome();

    // Get the expenditure - this is worked out by magic based on the income of the applicant/s.
    $resultexpenditure = new BorrowResultExpenditure($this->mortgage, $this->income, $income, $this->version);

    $expenditure = $resultexpenditure->getExpenditure();

    // Get the credit/commitments of the applicants.
    $resultcredit = new BorrowResultCredit($this->commitments);

    $credit = $resultcredit->getCredit();

    // Get the amount we are prepared to loan.
    $resultloan = new BorrowResultLoan($this->mortgage, $this->income, $this->commitments, $income, $expenditure, $credit, $this->version);

    $loan = $resultloan->getLoan();

    return [
      'income' => $income,
      'expenditure' => $expenditure,
      'credit' => $credit,
      'loan' => $loan,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function roundUp($n, $d = 0) {
    return round($n, $d);
  }

  /**
   * {@inheritdoc}
   */
  public static function log($text) {
    $fileHandle = fopen("mc-test-web18.log", "a");
    fwrite($fileHandle, print_r(date("Ymd:hi") . ' - ' . $text, TRUE) . "\n");
    fclose($fileHandle);
  }

}
