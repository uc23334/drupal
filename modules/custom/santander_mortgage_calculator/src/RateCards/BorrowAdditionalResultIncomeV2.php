<?php

namespace Drupal\santander_mortgage_calculator\RateCards;

/**
 * {@inheritdoc}
 */
class BorrowAdditionalResultIncomeV2 {

  /**
   * {@inheritdoc}
   */
  public $taxFreeAllowance = 12500;

  /**
   * {@inheritdoc}
   */
  public $rates = [
    'starting' => [
  // 10%
      'rate' => 0.1,
      'low' => 0,
      'high' => 0,
    ],
    'basic' => [
    // 20%
      'rate' => 0.2,
      'low' => 0,
      'high' => 37500,
    ],
    'higher' => [
    // 40%
      'rate' => 0.4,
      'low' => 37501,
      'high' => 150000,
    ],
    'additional' => [
    // 45%
      'rate' => 0.45,
      'low' => 150001,
    ],
  ];

  /**
   * {@inheritdoc}
   */
  public $primaryNiThreshold = [
  // 12%
    'rate' => 0.12,
    'low' => 8632,
  // DIP upper limit is 49999.
    'high' => 49999,
  ];

  /**
   * {@inheritdoc}
   */
  public $upperEarningsNiLimit = [
  // 2%
    'rate' => 0.02,
  // DIP low limit is 50000.
    'low' => 50000,
  ];

  /**
   * {@inheritdoc}
   */
  public $scalingFactor = 0.95;

  /**
   * {@inheritdoc}
   */
  public $secondaryWeighting = 0.65;

}
