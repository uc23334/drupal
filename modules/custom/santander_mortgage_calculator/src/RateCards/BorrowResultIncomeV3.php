<?php

namespace Drupal\santander_mortgage_calculator\RateCards;

/**
 * {@inheritdoc}
 */
class BorrowResultIncomeV3 {

  /**
   * {@inheritdoc}
   */
  public $taxFreeAllowance = 12500;

  /**
   * {@inheritdoc}
   */
  public $rates = [
    'starting' => [
  // 10%
      'rate' => 0.1,
      'low' => 0,
      'high' => 0,
      'dividends' => 0,
    ],
    'basic' => [
    // 20%
      'rate' => 0.2,
      'low' => 0,
      'high' => 37500,
    // 10% for dividents DIP
      'dividends' => 0.1,
    ],
    'higher' => [
    // 40%
      'rate' => 0.4,
      'low' => 37501,
      'high' => 150000,
    // 32.5% for dividents DIP
      'dividends' => 0.325,
    ],
    'additional' => [
    // 45%
      'rate' => 0.45,
      'low' => 150001,
    // 37.5% for dividents DIP
      'dividends' => 0.375,
    ],
  ];

  /**
   * {@inheritdoc}
   */
  public $directorTaxCredit = 0.1;

  /**
   * {@inheritdoc}
   */
  public $primaryNiThreshold = [
  // 12%
    'rate' => 0.12,
    'low' => 8632,
  // DIP upper limit is 41860.
    'high' => 49999,
  ];

  /**
   * {@inheritdoc}
   */
  public $upperEarningsNiLimit = [
  // 2%
    'rate' => 0.02,
  // DIP low limit is 41861.
    'low' => 50000,
  ];

  /**
   * {@inheritdoc}
   *
   * 95%.
   */
  public $scalingFactor = 0.95;

  /**
   * {@inheritdoc}
   *
   * Secondary Weighting.
   */
  public $secondaryWeighting = 0.65;

}
