<?php

namespace Drupal\santander_mortgage_calculator\RateCards;

/**
 * {@inheritdoc}
 */
class BorrowAdditionalResultLoanV1 {

  /**
   * {@inheritdoc}
   */
  public $ltv = [
    'low' => [
  // %
      'rate' => 0.07,
      'single' => 5.5,
      'joint' => 5.5,
    ],
    'high' => [
    // %
      'rate' => 0.07,
      'single' => 5,
      'joint' => 5,
    ],
    'higher' => [
    // %
      'rate' => 0.07,
      'single' => 4.45,
      'joint' => 4.45,
    ],
  ];

  /**
   * {@inheritdoc}
   *
   * Minimum Loan Amount Policy.
   */
  public $minAmountPolicy = 5000;

  /**
   * {@inheritdoc}
   *
   * Secondary Weighting 65%.
   */
  public $secondaryWeighting = 0.65;

}
