<?php

namespace Drupal\santander_mortgage_calculator\RateCards;

/**
 * {@inheritdoc}
 */
class BorrowResultLoanV1 {

  /**
   * {@inheritdoc}
   */
  public $ltv = [
    'p4p' => [
  // %
      'rate' => 0.07,
      'single' => 5.5,
      'joint' => 5.5,
      'ftb_rate' => 4.45,
    ],
    'low' => [
    // %
      'rate' => 0.07,
      'single' => 5.5,
      'joint' => 5.5,
      'ftb_rate' => 4.45,
    ],
    'medium' => [
    // %
      'rate' => 0.07,
      'single' => 5.5,
      'joint' => 5.5,
      'ftb_rate' => 4.45,
    ],
    'high' => [
    // %
      'rate' => 0.07,
      'single' => 5,
      'joint' => 5,
      'ftb_rate' => 4.45,
    ],
    'higher' => [
    // %
      'rate' => 0.07,
      'single' => 4.45,
      'joint' => 4.45,
      'ftb_rate' => 4.45,
    ],
  ];

  /**
   * {@inheritdoc}
   *
   * Secondary Weighting.
   */
  public $secondaryWeighting = 0.65;

  /**
   * {@inheritdoc}
   *
   * Minimum Loan Amount Policy.
   */
  public $minAmountPolicy = 5000;

}
