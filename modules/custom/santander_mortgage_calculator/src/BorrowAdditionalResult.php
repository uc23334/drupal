<?php

namespace Drupal\santander_mortgage_calculator;

/**
 * {@inheritdoc}
 */
class BorrowAdditionalResult {

  /**
   * {@inheritdoc}
   */
  private $version;

  /**
   * {@inheritdoc}
   */
  private $mortgage = [
    'applicants' => 1,
    'buyers' => '',
    'dependants' => 0,
    'term' => 25,
    'deposit' => 0,
  ];

  /**
   * {@inheritdoc}
   */
  private $income = [
    'basic' => [
      'a1' => ['taxable' => 0],
      'a2' => ['taxable' => 0],
    ],
    'guaranteed' => [
      'a1' => ['taxable' => 0, 'nonTaxable' => 0],
      'a2' => ['taxable' => 0, 'nonTaxable' => 0],
    ],
    'regular' => [
      'a1' => ['taxable' => 0, 'nonTaxable' => 0],
      'a2' => ['taxable' => 0, 'nonTaxable' => 0],
    ],
  ];

  /**
   * {@inheritdoc}
   */
  private $commitments = [
    'creditCard' => 0,
    'loans' => 0,
    'outgoings' => 0,
  ];

  /**
   * {@inheritdoc}
   */
  private $questions = [
    'propertyValue' => 0,
    'debtConsolidation' => 0,
    'loanp1amount' => 0,
    'loanp2amount' => 0,
    'loanp3amount' => 0,
    'loanp4amount' => 0,
    'loanp5amount' => 0,
    'loanp1term' => 0,
    'loanp2term' => 0,
    'loanp3term' => 0,
    'loanp4term' => 0,
    'loanp5term' => 0,
    'loanp1Repayment' => 0,
    'loanp2Repayment' => 0,
    'loanp3Repayment' => 0,
    'loanp4Repayment' => 0,
    'loanp5Repayment' => 0,
    'newTerm' => 0,
  ];

  /**
   * Constructor.
   */
  public function __construct($version = 'v1') {
    $this->version = $version;
  }

  /**
   * {@inheritdoc}
   */
  public function getVersion() {
    return $this->version;
  }

  /**
   * Set Params.
   *
   * @param array $data
   *   Data.
   */
  public function setParams(array $data) {

    $this->mortgage['applicants'] = $data['applicants'];
    $this->mortgage['dependants'] = ($data['dependants'] == '+3') ? 3 : $data['dependants'];
    $this->mortgage['newTerm']    = $data['loan_term_years'] * 12 + $data['loan_term_months'];

    // Mortgage loan part questions.
    $this->questions['debtConsolidation'] = $data['debt_consolidation'];

    $this->questions['propertyValue'] = $data['property_value'];
    $this->questions['newTerm']       = $this->mortgage['newTerm'];

    // Loan parts in the form may be out of order and have empty parts. Make sure the array used for calcs has them sequentially (order doesn't matter)
    for ($t = 1, $i = 1; $i <= 5; $i++) {

      if ($data['loan_part_amount_' . $i] > 0) {

        $loanpart = 'loanp' . $t;

        $this->questions[$loanpart . 'amount']    = $data['loan_part_amount_' . $i];
        $this->questions[$loanpart . 'term']      = !empty($data['loan_part_amount_' . $i]) ? $data['loan_part_term_years_' . $i] * 12 + $data['loan_part_term_months_' . $i] : 0;
        $this->questions[$loanpart . 'Repayment'] = $data['loan_part_repayment_method_' . $i] === 'interest' ? 'IO' : 'C&I';

        $t++;
      }
    }

    // Applicant 1 income.
    $this->income['basic']['a1']['taxable']         = $data['income_applicant_1_annual_amount'];
    $this->income['guaranteed']['a1']['taxable']    = 0;
    $this->income['guaranteed']['a1']['nonTaxable'] = $data['income_applicant_1_other_annual_amount'];
    $this->income['regular']['a1']['taxable']       = $data['income_applicant_1_extra_annual_amount'];
    $this->income['regular']['a1']['nonTaxable']    = $data['income_applicant_1_csa_monthly_amount'];

    // Applicant 2 income.
    if ($data['applicants'] == 2) {
      $this->income['basic']['a2']['taxable']         = $data['income_applicant_2_annual_amount'];
      $this->income['guaranteed']['a2']['taxable']    = 0;
      $this->income['guaranteed']['a2']['nonTaxable'] = $data['income_applicant_2_other_annual_amount'];
      $this->income['regular']['a2']['taxable']       = $data['income_applicant_2_extra_annual_amount'];
      $this->income['regular']['a2']['nonTaxable']    = $data['income_applicant_2_csa_monthly_amount'];
    }
    else {
      $this->income['basic']['a2']['taxable']         = 0;
      $this->income['guaranteed']['a2']['taxable']    = 0;
      $this->income['guaranteed']['a2']['nonTaxable'] = 0;
      $this->income['regular']['a2']['taxable']       = 0;
      $this->income['regular']['a2']['nonTaxable']    = 0;
    }

    // Outgoings.
    $this->commitments['creditCard'] = $data['commitments_credit_card'];
    $this->commitments['loans']      = $data['commitments_loans'];
    $this->commitments['outgoings']  = $data['commitments_outgoings'];
  }

  /**
   * Validation.
   *
   * @param array $data
   *   Data.
   *
   * @return mixed
   *   Return
   */
  public function validate(array &$data) {
    $error = FALSE;

    $fields = [
      'applicants' => ['required' => TRUE, 'type' => 'numeric', 'min' => 1],
      'dependants' => ['required' => TRUE],
      'property_value' => ['required' => TRUE, 'type' => 'numeric', 'min' => 1],
      'loan_term_years' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 5,
        'max' => 40,
      ],
      'loan_term_months' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 0,
        'max' => 11,
      ],

      'loan_part_amount_1' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 1,
      ],
      'loan_part_term_years_1' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 1,
        'max' => 40,
      ],
      'loan_part_term_months_1' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 0,
        'max' => 11,
      ],
      'loan_part_repayment_method_1' => ['required' => TRUE],

      'loan_part_amount_2' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 1,
        'default' => 0,
      ],
      'loan_part_term_years_2' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 1,
        'max' => 40,
        'default' => 0,
      ],
      'loan_part_term_months_2' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 0,
        'max' => 11,
        'default' => 0,
      ],
      'loan_part_repayment_method_2' => ['required' => FALSE],

      'loan_part_amount_3' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 1,
        'default' => 0,
      ],
      'loan_part_term_years_3' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 1,
        'max' => 40,
        'default' => 0,
      ],
      'loan_part_term_months_3' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 0,
        'max' => 11,
        'default' => 0,
      ],
      'loan_part_repayment_method_3' => ['required' => FALSE],

      'loan_part_amount_4' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 1,
        'default' => 0,
      ],
      'loan_part_term_years_4' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 1,
        'max' => 40,
        'default' => 0,
      ],
      'loan_part_term_months_4' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 0,
        'max' => 11,
        'default' => 0,
      ],
      'loan_part_repayment_method_4' => ['required' => FALSE],

      'loan_part_amount_5' => [
        'required' => FALSE,
        'type' => 'numeric',
        '
        min' => 1, 'default' => 0,
      ],
      'loan_part_term_years_5' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 1,
        'max' => 40,
        'default' => 0,
      ],
      'loan_part_term_months_5' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 0,
        'max' => 11,
        'default' => 0,
      ],
      'loan_part_repayment_method_5' => ['required' => FALSE],

      'income_applicant_1_annual_amount' => [
        'required' => TRUE,
        'type' => 'numeric',
        'min' => 0,
      ],
      'income_applicant_1_other_annual_amount' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 0,
        'default' => 0,
      ],
      'income_applicant_1_extra_annual_amount' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 0,
        'default' => 0,
      ],
      'income_applicant_1_csa_monthly_amount' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 0,
        'default' => 0,
      ],

      'income_applicant_2_annual_amount' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 0,
        'default' => 0,
      ],
      'income_applicant_2_other_annual_amount' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 0,
        'default' => 0,
      ],
      'income_applicant_2_extra_annual_amount' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 0,
        'default' => 0,
      ],
      'income_applicant_2_csa_monthly_amount' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 0,
        'default' => 0,
      ],

      'commitments_credit_card' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 0,
        'default' => 0,
      ],
      'commitments_loans' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 0,
        'default' => 0,
      ],
      'commitments_outgoings' => [
        'required' => FALSE,
        'type' => 'numeric',
        'min' => 0,
        'default' => 0,
      ],
    ];

    $invalid = [];

    // For each input field.
    foreach ($fields as $name => $fieldInfo) {

      // If its missing but required.
      if (!isset($data[$name]) && $fieldInfo['required']) {
        $invalid[$name] = 'required';
      }

      // If the value is set then check its ok.
      if (!empty($data[$name])) {

        // If its supposed to be numeric.
        if (isset($fieldInfo['type']) && $fieldInfo['type'] == 'numeric') {

          // Make sure it is numeric.
          $data[$name] = floatval(preg_replace('/[^\d.]/', '', $data[$name]));

          // If it should have a minimum value.
          if (isset($fieldInfo['min']) && $data[$name] < $fieldInfo['min']) {
            $invalid[$name] = 'min';
          }

          // If it should have a maximum value.
          if (isset($fieldInfo['max']) && $data[$name] > $fieldInfo['max']) {
            $invalid[$name] = 'max';
          }
        }
      }
      else {
        if (isset($fieldInfo['default'])) {
          $data[$name] = $fieldInfo['default'];
        }
      }
    }

    if (count($invalid) > 0) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function calculate() {

    // Get the income of the applicants.
    $resultincome = new BorrowAdditionalResultIncome($this->income, $this->version);

    $income = $resultincome->getIncome();

    // Get the expenditure - this is worked out by magic based on the income of the applicant/s.
    $resultexpenditure = new BorrowAdditionalResultExpenditure($this->mortgage, $this->income, $income, $this->version);

    $expenditure = $resultexpenditure->getExpenditure();

    // Get the credit/commitments of the applicants.
    $resultcredit = new BorrowAdditionalResultCredit($this->commitments);

    $credit = $resultcredit->getCredit();

    // Get the amount we are prepared to loan.
    $resultloan = new BorrowAdditionalResultLoan($this->mortgage, $this->income, $this->commitments, $income, $expenditure, $credit, $this->questions, $this->version);

    $loan = $resultloan->getLoan();

    return [
      'income' => $income,
      'expenditure' => $expenditure,
      'credit' => $credit,
      'loan' => $loan,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function roundUp($n, $d = 0) {
    return round($n, $d);
  }

  /**
   * {@inheritdoc}
   */
  public static function sanMax(array $arr, $min = 0) {
    $m = max($arr);
    return $m > $min ? $m : $min;
  }

}
