<?php

namespace Drupal\santander_mortgage_calculator\Plugin\WebformHandler;

use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\Component\Utility\Xss;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "mortgage_comparison_submit_form_handler",
 *   label = @Translation("Mortgage Comparison Submit form handler"),
 *   category = @Translation("Form Handler"),
 *   description = @Translation("Updates session with form values"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class MortgageComparisonSubmit extends WebformHandlerBase {

  // Following demonstrates how to set the values of elements in the webform from arbitary query parameters in the URL.

  /**
   * Note: Element values set here will override any query parameters using the elements name.
   */
  public function postCreate(WebformSubmissionInterface $webform_submission) {

    // Get any existing query params, remove ones we don't care about and ensure the rest are safe
    // Mortgage comparison can accept - buyer type, property value, borrow amount, loan term, santander select and help to buy.
    $qparams = \Drupal::request()->query->all();

    $allowed = ['sanbt', 'sanpv', 'sanba', 'sanlt', 'sanss', 'sanhtb'];

    $qparams = array_intersect_key($qparams, array_flip($allowed));

    foreach ($qparams as $key => $qparam) {
      $qparams[$key] = Xss::filter($qparam);
    }

    // Get webform data.
    $data = $webform_submission->getData();

    // Update the webform element values.
    foreach ($qparams as $key => $qparam) {
      switch ($key) {
        case 'sanbt':  $data["buyer_type"] = $qparam;
          break;

        case 'sanpv':  $data["property_value"] = $qparam;
          break;

        case 'sanba':  $data["borrow_amount"] = $qparam;
          break;

        case 'sanlt':  $data["mortgage_term"] = $qparam;
          break;

        case 'sanss':  $data["santander_select"] = $qparam;
          break;

        case 'sanhtb': $data["htb_isa"] = $qparam;
          break;
      }
    }

    $webform_submission->setData($data);
  }

}
