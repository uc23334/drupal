<?php

namespace Drupal\santander_mortgage_calculator\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\Component\Utility\Xss;

use Drupal\santander_mortgage_calculator\BorrowAdditionalResult;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "mortgage_calculator_additional_submit_form_handler",
 *   label = @Translation("Mortgage Calculator Additonal Submit form handler"),
 *   category = @Translation("Form Handler"),
 *   description = @Translation("Updates session with form values"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class MortgageCalculatorAdditionalSubmit extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['additional_query_params'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Additional Query Parameters'),
      '#description' => $this->t("Enter any additional query paramaters for the URL in the form name=value. Seperate values using '&'."),
      '#default_value' => $this->configuration['additional_query_params'],
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {

    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['additional_query_params'] = trim($form_state->getValue('additional_query_params'));
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {

    // Get the page we are on.
    $currentPage = $form_state->get('current_page');

    // On the questions page so validate any answers and stop submit if any fields are invalid.
    if ($currentPage === 'page_questions') {

      // Get values from the form.
      $data = $form_state->getValues();

      $calcVersion = !empty($webform_submission->data['calculator_version']) ? $webform_submission->data['calculator_version'] : 'v2';

      // Validate and parse the data from the form making sure numerics are read correctly.
      $borrow = new BorrowAdditionalResult($calcVersion);

      if (!$borrow->validate($data)) {
        $form_state->setErrorByName('anything', "The form had errors. Please check the data you entered and try again.");
      }

      // On the results page so check the borrow amount is within bounds.
    }
    elseif ($currentPage === 'page_your_result') {

      $form_state->clearErrors();

      // Get the amount they want to borrow from the form.
      $formatter = new \NumberFormatter('en_EN', \NumberFormatter::DECIMAL);
      $borrowAmount = $formatter->parse($form_state->getValue('borrow_amount'));

      // Get calculated values from the tempstore.
      $tempstore = \Drupal::service('tempstore.private')->get('santander_mortgage_calculator_additional');

      $loanAmount = $tempstore->get('loanAmount');

      if ($borrowAmount < 5000 || $borrowAmount > $loanAmount) {
        $form_state->setErrorByName('borrow_amount', "Amount you wish to borrow is out of bounds.");
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {

    // Get the page we are on.
    $currentPage = $form_state->get('current_page');

    // If we are showing the results page then save the important values on the session object.
    if ($currentPage === 'page_your_result') {

      $trigger = $form_state->getTriggeringElement();

      if ($trigger['#attributes']['name'] === 'show-results') {

        // Get values from the form.
        $data = $form_state->getValues();

        $calcVersion = !empty($webform_submission->data['calculator_version']) ? $webform_submission->data['calculator_version'] : 'v2';

        // Validate and parse the data from the form making sure numerics are read correctly.
        $borrow = new BorrowAdditionalResult($calcVersion);

        // Validate and parse the data from the form making sure numerics are read correctly.
        $borrow->validate($data);
        $borrow->setParams($data);

        // Do the loan calcs.
        $result = $borrow->calculate();

        // Work out/collect values used for the display of the results.
        // The max amount we can loan.
        $loanAmount = $result['loan']['loan'];
        // Annual income of the applicants.
        $incomeAmount = $result['income']['a1']['total'] + $result['income']['a2']['total'];
        // Monthly expenses of the applicant.
        $expensesAmount = $result['credit'] + $result['expenditure']['final'];
        // Additional loan term.
        $mortgageTerm = $result['loan']['additionalLoanTerm'];
        // Mortgage balance.
        $mortgageBalance = $result['loan']['totalAmount'];
        // Total borrowing (how is this different?)
        $totalBorrowing = $result['loan']['totalAmount'];
        // Estimated property value.
        $propertyValue = $result['loan']['propertyValue'];

        // Set calculated values on the tempstore so they can be picked up by tokens referenced in the webform and by the final confirm form method.
        $tempstore = \Drupal::service('tempstore.private')->get('santander_mortgage_calculator_additional');

        $tempstore->set('loanAmount', $loanAmount);
        $tempstore->set('income', $incomeAmount);
        $tempstore->set('expenses', $expensesAmount);
        $tempstore->set('mortgageTerm', $mortgageTerm);
        $tempstore->set('mortgageBalance', $mortgageBalance);
        $tempstore->set('totalBorrowing', $totalBorrowing);
        $tempstore->set('propertyValue', $propertyValue);

        // Save loan amount temporarily so can be accessed in alterForm.
        $form_state->setTemporaryValue('loanAmount', $loanAmount);
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function alterForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {

    // Get the page we are on.
    $currentPage = $form_state->get('current_page');

    // If we are on the results page then update the displayed fields depending on the loan amount.
    if ($currentPage === 'page_your_result') {

      $loanAmount = $form_state->getTemporaryValue('loanAmount');

      $form['elements']['page_your_result']['container_loan_amount']['#access'] = TRUE;
      $form['elements']['page_your_result']['container_borrow_amount']['#access'] = TRUE;
      $form['elements']['page_your_result']['container_loan_amount_zero']['#access'] = TRUE;

      // If amount we can loan is 0 then show appropriate message and hide the range slider.
      if ($loanAmount === 0) {
        $form['elements']['page_your_result']['container_loan_amount']['#access'] = FALSE;
        $form['elements']['page_your_result']['container_borrow_amount']['#access'] = FALSE;
      }
      else {
        $form['elements']['page_your_result']['container_loan_amount_zero']['#access'] = FALSE;
      }

      // Make sure loan amount is accessable in clientside JS.
      $form['#attached']['drupalSettings']['borrowAdditional']['loanAmount'] = $loanAmount;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {

    // Existing URL = https://www.santander.co.uk/info/mortgages/compare-additional-loans?lend=203865
    // Get any existing query params.
    $qparams = \Drupal::request()->query->all();

    unset($qparams['ajax_form']);
    unset($qparams['_wrapper_format']);

    foreach ($qparams as $key => $qparam) {
      $qparams[$key] = Xss::filter($qparam);
    }

    // Get values entered in the form.
    $values = $webform_submission->getData();

    // Get the additional amount they want to borrow from the form.
    $formatter = new \NumberFormatter('en_EN', \NumberFormatter::DECIMAL);
    $borrowAmount = $formatter->parse($values['borrow_amount']);

    // Get the saved values from the tempstore.
    $tempstore = \Drupal::service('tempstore.private')->get('santander_mortgage_calculator_additional');

    $mortgageTerm    = $tempstore->get('mortgageTerm');
    $propertyValue   = $tempstore->get('propertyValue');
    $existingBalance = $tempstore->get('totalBorrowing');

    // Build query params for the calculator values.
    $params['query'] = [
      'sanba' => $borrowAmount,
      'sanlt' => intdiv($mortgageTerm, 12),
      'sanpv' => $propertyValue,
      'saneb' => $existingBalance,
    ];

    $params['query'] += $qparams;

    // Add any params specified in the handler settings.
    if (!empty($this->configuration['additional_query_params'])) {
      $pairs = [];
      parse_str($this->configuration['additional_query_params'], $pairs);
      $pairs = array_map('trim', $pairs);
      $params['query'] = $pairs + $params['query'];
    }

    // Set the destination from the webforms third party settings.
    $webform = $webform_submission->getWebform();
    $submissonUrl = $webform->getThirdPartySetting('santander_webform', 'mortgage_comparison_additional_url');

    // Get the actual node path (Must be a node for this to work)
    $path = \Drupal::service('path.alias_manager')->getPathByAlias($submissonUrl);
    $matches = [];
    if (preg_match('/node\/(\d+)/', $path, $matches)) {
      $form_state->setRedirect('entity.node.canonical', ['node' => $matches[1]], $params);
    }
  }

  // Following demonstrates how to set the values of elements in the webform from arbitary query parameters in the URL.

  /**
   * Note: Element values set here will override any query parameters using the elements name.
   */
  public function postCreate(WebformSubmissionInterface $webform_submission) {

    // Get any existing query params, remove ones we dont care about and ensure the rest are safe
    // Mortgage Calculator can accept - Dependents, Property value, borrow amount and loan term.
    $qparams = \Drupal::request()->query->all();

    $allowed = ['sanapp', 'sandep', 'sanpv', 'sanlt'];

    $qparams = array_intersect_key($qparams, array_flip($allowed));

    foreach ($qparams as $key => $qparam) {
      $qparams[$key] = Xss::filter($qparam);
    }

    // Get webform data.
    $data = $webform_submission->getData();

    // Update the webform element values.
    foreach ($qparams as $key => $qparam) {
      switch ($key) {
        case 'sanapp': $data["applicants"] = $qparam;
          break;

        case 'sanpv':  $data["property_value"] = $qparam;
          break;

        case 'sandep': $data["dependants"] = $qparam;
          break;

        case 'sanlt':  $data["loan_term_years"] = $qparam;
          break;
      }
    }

    $webform_submission->setData($data);
  }

}
