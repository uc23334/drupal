<?php

namespace Drupal\santander_mortgage_calculator\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\Component\Utility\Xss;

use Drupal\santander_mortgage_calculator\BorrowResult;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "mortgage_calculator_submit_form_handler",
 *   label = @Translation("Mortgage Calculator Submit form handler"),
 *   category = @Translation("Form Handler"),
 *   description = @Translation("Updates session with form values"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class MortgageCalculatorSubmit extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['additional_query_params'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Additional Query Parameters'),
      '#description' => $this->t("Enter any additional query paramaters for the URL in the form name=value. Seperate values using '&'."),
      '#default_value' => $this->configuration['additional_query_params'],
      '#required' => FALSE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['additional_query_params'] = trim($form_state->getValue('additional_query_params'));
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {

    // Get the page we are about to show (not really 'current' page at all)
    $targetpage = $form_state->get('current_page');

    // On the results page so save the important values on the session object.
    if ($targetpage === 'page_your_result') {

      $form_state->clearErrors();

      $borrowamount = $form_state->getValue('borrow_amount');
      $loanamount = $form_state->getValue('loan_amount');

      if ($borrowamount < 1000 || $borrowamount > $loanamount) {
        $form_state->setErrorByName('borrow_amount', "Amount you wish to borrow is out of bounds.");
      }

      // On the questions page so validate any answers and stop submit if any fields are invalid.
    }
    elseif ($targetpage === 'page_questions') {

      // Get values from the form.
      $data = $form_state->getValues();

      $calcVersion = !empty($webform_submission->data['calculator_version']) ? $webform_submission->data['calculator_version'] : 'v2';

      // Validate and parse the data from the form making sure numerics are read correctly.
      $borrow = new BorrowResult($calcVersion);

      if (!$borrow->validate($data)) {
        $form_state->setErrorByName('anything', "The form had errors. Please check the data you entered and try again.");
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {

    // Get the page we are about to show (not really 'current' page at all)
    $targetpage = $form_state->get('current_page');

    // If we are heading for the results page then save the important values on the session object.
    if ($targetpage === 'page_your_result') {

      $trigger = $form_state->getTriggeringElement();

      if ($trigger['#attributes']['name'] === 'show-results') {

        // Get values from the form.
        $data = $form_state->getValues();

        $calcVersion = !empty($webform_submission->data['calculator_version']) ? $webform_submission->data['calculator_version'] : 'v2';

        $borrow = new BorrowResult($calcVersion);

        // Validate and parse the data from the form making sure numerics are read correctly.
        $borrow->validate($data);
        $borrow->setParams($data);

        // Do the loan calcs.
        $result = $borrow->calculate();

        // Work out/collect values used for the display of the results.
        // The buyer type.
        $buyertype = $data['buyer_type_select'];
        // The deposit the applicant indicated.
        $depositamount = $data['deposit'];

        // The max amount we can loan.
        $loanamount = $result['loan']['loan'];
        // Annual income of the applicants.
        $incomeamount = $result['income']['a1']['total'] + $result['income']['a2']['total'];
        // Monthly expenses of the applicant.
        $expensesamount = $result['credit'] + $result['expenditure']['final'];

        // Set the limits for the range slider so we dont get validation errors.
        $form['elements']['page_your_result']['container_borrow_amount']['borrow_amount']['#min'] = 1000;
        $form['elements']['page_your_result']['container_borrow_amount']['borrow_amount']['#max'] = $loanamount;

        // Set hidden values on the form so we can setup the slider on the client side.
        $userinput = $form_state->getUserInput();

        $userinput["loan_amount"] = $loanamount;
        $userinput["deposit_amount"] = $depositamount;

        $form_state->setUserInput($userinput);

        // Set calculated values on the tempstore so they can be picked up by tokens and passed to the form.
        $tempstore = \Drupal::service('tempstore.private')->get('santander_mortgage_calculator');

        $tempstore->set('buyertype', $buyertype);
        $tempstore->set('loanAmount', $loanamount);
        $tempstore->set('deposit', $depositamount);
        $tempstore->set('income', $incomeamount);
        $tempstore->set('expenses', $expensesamount);

        // Save loan amount temporarily so can be accessed in alterForm.
        $form_state->setTemporaryValue('loanAmount', $loanamount);
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function alterForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {

    // Get the page we are on.
    $currentPage = $form_state->get('current_page');

    // If we are on the results page then update the displayed fields depending on the loan amount.
    if ($currentPage === 'page_your_result') {

      $loanAmount = $form_state->getTemporaryValue('loanAmount');

      $form['elements']['page_your_result']['container_loan_amount']['#access'] = TRUE;
      $form['elements']['page_your_result']['container_borrow_amount']['#access'] = TRUE;
      $form['elements']['page_your_result']['container_loan_amount_zero']['#access'] = TRUE;
      $form['elements']['page_your_result']['container_actions']['actions_results_bottom']['#access'] = TRUE;
      $form['elements']['page_your_result']['container_actions']['decision_in_principle']['#access'] = TRUE;

      // If amount we can loan is 0 then show appropriate message and hide the range slider.
      if ($loanAmount === 0) {
        $form['elements']['page_your_result']['container_loan_amount']['#access'] = FALSE;
        $form['elements']['page_your_result']['container_borrow_amount']['#access'] = FALSE;
        $form['elements']['page_your_result']['container_actions']['actions_results_bottom']['#access'] = FALSE;
        $form['elements']['page_your_result']['container_actions']['decision_in_principle']['#access'] = FALSE;
      }
      else {
        $form['elements']['page_your_result']['container_loan_amount_zero']['#access'] = FALSE;
      }

      // Make sure loan amount is accessable in clientside JS.
      $form['#attached']['drupalSettings']['borrowCalc']['loanAmount'] = $loanAmount;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {

    // Get any existing query params.
    $qparams = \Drupal::request()->query->all();

    unset($qparams['ajax_form']);
    unset($qparams['_wrapper_format']);

    foreach ($qparams as $key => $qparam) {
      $qparams[$key] = Xss::filter($qparam);
    }

    // Get values entered in the form.
    $values = $webform_submission->getData();

    $params['query'] = [
      'sanbt'  => $values['buyer_type_select'],
      'sanpv'  => $values['deposit_amount'] + $values['borrow_amount'],
      'sanba'  => $values['borrow_amount'],
      'sanlt'  => $values['terms'],
      'sanss'  => $values['santander_select'],
      'sanhtb' => $values['htb_isa'],
    ];

    $params['query'] += $qparams;

    // Add any params specified in the handler settings.
    if (!empty($this->configuration['additional_query_params'])) {
      $pairs = [];
      parse_str($this->configuration['additional_query_params'], $pairs);
      $pairs = array_map('trim', $pairs);
      $params['query'] = $pairs + $params['query'];
    }

    // Set the destination from the webforms third party settings.
    $webform = $webform_submission->getWebform();
    $submissonUrl = $webform->getThirdPartySetting('santander_webform', 'mortgage_comparison_url');

    // Get the actual node path (Must be a node for this to work)
    $path = \Drupal::service('path.alias_manager')->getPathByAlias($submissonUrl);
    $matches = [];
    if (preg_match('/node\/(\d+)/', $path, $matches)) {
      $form_state->setRedirect('entity.node.canonical', ['node' => $matches[1]], $params);
    }
  }

}
