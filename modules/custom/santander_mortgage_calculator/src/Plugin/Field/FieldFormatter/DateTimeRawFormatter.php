<?php

namespace Drupal\santander_mortgage_calculator\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\datetime\Plugin\Field\FieldFormatter\DateTimeFormatterBase;

/**
 * Plugin implementation of the 'Raw' formatter for 'datetime' fields.
 *
 * @FieldFormatter(
 *   id = "datetime_raw",
 *   label = @Translation("Raw"),
 *   field_types = {
 *     "datetime"
 *   }
 * )
 */
class DateTimeRawFormatter extends DateTimeFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];

    foreach ($items as $delta => $item) {
      if (!empty($item->date)) {
        $date = $item->date;

        $elements[$delta] = $this->buildDate($date);

      }
      else {

        $build = [
          '#markup' => $item->value,
          '#cache' => [
            'contexts' => [
              'timezone',
            ],
          ],
        ];

        $elements[$delta] = $build;
      }
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  protected function formatDate($date) {
    $format = $this->getFieldSetting('datetime_type') == DateTimeItem::DATETIME_TYPE_DATE ? DateTimeItemInterface::DATE_STORAGE_FORMAT : DateTimeItemInterface::DATETIME_STORAGE_FORMAT;
    $timezone = $this->getSetting('timezone_override');
    return $this->dateFormatter->format($date->getTimestamp(), 'custom', $format, $timezone != '' ? $timezone : NULL);
  }

}
