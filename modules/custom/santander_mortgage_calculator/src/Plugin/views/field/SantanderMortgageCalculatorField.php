<?php

namespace Drupal\santander_mortgage_calculator\Plugin\views\field;

use Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to flag the node type.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("santander_mortgage_calculator_field")
 */
class SantanderMortgageCalculatorField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * Define the available options.
   *
   * @return array
   *   Return array
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['mode_type'] = ['default' => 'checkbox'];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {

    $options['checkbox'] = 'Checkbox';
    $options['monthly_cost'] = 'Monthly cost';

    $form['mode_type'] = [
      '#title' => $this->t('Santander mortage calculator field?'),
      '#type' => 'select',
      '#default_value' => $this->options['mode_type'],
      '#options' => $options,
    ];

    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    $product = $values->_entity;
    $mode = $this->options['mode_type'];
    $session = Drupal::request()->getSession();
    if ($session) {
      $data = $session->get('santander_mortgage_calculator');
      // Form param.
      $borrow = $data['borrow'];
      // Form param.
      $years = $data['terms'];
    }

    // if(!$borrow && !$years){
    // return;
    // }.
    switch ($mode) {

      case 'monthly_cost':

        $initial_rate = $product->get('field_initial_rate')->getValue();

        // Temp until data import is fully loaded.
        if (!isset($initial_rate[0])) {
          $initial_rate[0]['value'] = 3.0;
        }

        $initial_rate = substr($initial_rate[0]['value'], 0, -2);
        // Will use field from entity.
        $n = $initial_rate / 100;
        $interest = ($n / 12) / (1 - (pow(1 + ($n / 12), (-1 * ($years * 12)))));
        $result = round($borrow * $interest);

        break;

      case 'checkbox':
        $product_id = $product->get('id')->getValue();
        $result = [
          '#theme' => 'santander_mortgage_calculator_field',
          '#pid' => $product_id[0]['value'],
          '#checked' => in_array($product_id[0]['value'], $data['mortgage_compare']) ? 'checked="checked"' : '',
          '#cache' => FALSE,
        ];
        break;
    }

    return $result;
  }

}
