<?php

namespace Drupal\santander_mortgage_calculator;

use Drupal\santander_mortgage_calculator\RateCards\BorrowAdditionalResultExpenditureV1;
use Drupal\santander_mortgage_calculator\RateCards\BorrowAdditionalResultExpenditureV2;
use Drupal\santander_mortgage_calculator\RateCards\BorrowAdditionalResultExpenditureV3;

/**
 * Expenditure model.
 */
class BorrowAdditionalResultExpenditure {

  /**
   * {@inheritdoc}
   *
   * Ratecard Constants/Data/Values.
   */
  private $quintiles;

  /**
   * {@inheritdoc}
   */
  private $rates;

  /**
   * {@inheritdoc}
   */
  private $scalingFactor;

  /**
   * {@inheritdoc}
   *
   * Mortgage.
   */
  private $mortgage = NULL;

  /**
   * {@inheritdoc}
   *
   * Income.
   */
  private $income = NULL;

  /**
   * {@inheritdoc}
   *
   * Income gross.
   */
  private $gross = NULL;

  /**
   * {@inheritdoc}
   *
   * Expenditure.
   */
  private $expenditure = [
  // £
    'gross' => 0,
  // £
    'net' => 0,
    'applicants' => 0,
    'dependants' => 0,
    'quintile' => 0,
    'concat' => '',
  // £
    'min' => 0,
  // £
    'max' => 0,
  // £
    'lowerLimit' => 0,
  // £
    'upperLimit' => 0,
  // £
    'quintileDifference' => 0,
  // £
    'incomeLowerQuintileDifference' => 0,
  // %
    'proportion' => 0,
  // £
    'difference' => 0,
  // £
    'raw' => 0,
  // £
    'final' => 0,
  ];

  /**
   * Constructor.
   */
  public function __construct(array $mortgage, array $income, array $gross, $version = 'v1') {

    $this->mortgage = $mortgage;
    $this->income = $income;
    $this->gross = $gross;

    // Load the ratecard data from the appropriate ratecard.
    if ($version == 'v1') {
      $ratecard = new BorrowAdditionalResultExpenditureV1();
    }
    elseif ($version == 'v3') {
      $ratecard = new BorrowAdditionalResultExpenditureV3();
    }
    else {
      $ratecard = new BorrowAdditionalResultExpenditureV2();
    }

    $this->quintiles = $ratecard->quintiles;
    $this->rates = $ratecard->rates;
    $this->scalingFactor = $ratecard->scalingFactor;
  }

  /**
   * Calculate low/medium expenditure.
   */
  public function getExpenditure() {
    // Calculate total joint gross income.
    $this->expenditure['gross'] = $this->totalJointGrossIncome([
      $this->income['basic']['a1']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['guaranteed']['a1']['nonTaxable'],
      $this->income['regular']['a1']['taxable'],
      $this->income['regular']['a1']['nonTaxable'],
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['guaranteed']['a2']['nonTaxable'],
      $this->income['regular']['a2']['taxable'],
      $this->income['regular']['a2']['nonTaxable'],
    ]);
    // Calculate total joint net income.
    $this->expenditure['net'] = $this->totalJointNetIncome([
      $this->gross['a1']['monthly']['total'],
      $this->gross['a2']['monthly']['total'],
    ]);
    // Calculate number of applicants.
    $this->expenditure['applicants'] = $this->numberOfApplicants($this->mortgage['applicants']);
    // Calculate number of dependants.
    $this->expenditure['dependants'] = $this->numberOfDependants($this->mortgage['dependants']);
    // Calculate income quintile.
    $this->expenditure['quintile'] = $this->incomeQuintile();
    // Concat.
    $this->expenditure['concat'] = $this->concat();
    // Calculate min/max expenditure.
    $this->expenditure['min'] = $this->minMaxExpenditure($this->expenditure['applicants'], 'min');
    $this->expenditure['max'] = $this->minMaxExpenditure($this->expenditure['applicants'], 'max');
    // Calculate quintile lower/upper limit.
    $this->expenditure['lowerLimit'] = $this->quintileLimit($this->expenditure['dependants'], 'lower');
    $this->expenditure['upperLimit'] = $this->quintileLimit($this->expenditure['dependants'], 'upper');
    // Calculate quintile differences.
    $this->expenditure['quintileDifference'] = $this->quintileDifferences();
    // Calculate income to lower quintile difference.
    $this->expenditure['incomeLowerQuintileDifference'] = $this->incomeLowerQuintileDifference();
    // Calculate proportion.
    $this->expenditure['proportion'] = $this->proportion();
    // Calculate expenditure difference.
    $this->expenditure['difference'] = $this->difference();
    // Calculate raw expenditure.
    $this->expenditure['raw'] = $this->raw();
    // Calculate final expenditure.
    $this->expenditure['final'] = $this->finalExpenditure();
    return $this->expenditure;
  }

  /**
   * Calculate total joint gross income.
   */
  public function totalJointGrossIncome(array $arr) {
    return array_sum($arr);
  }

  /**
   * Calculate total joint net income.
   */
  public function totalJointNetIncome(array $arr) {
    return array_sum($arr);
  }

  /**
   * Calculate number of applicants.
   */
  public function numberOfApplicants($a) {
    if ($a > 1) {
      return '2+';
    }
    else {
      return $a;
    }
  }

  /**
   * Calculate number of dependants.
   */
  public function numberOfDependants($a) {
    if ($a > 2) {
      return '3+';
    }
    else {
      return $a;
    }
  }

  /**
   * Calculate income quintile.
   */
  public function incomeQuintile() {
    $q = '';
    if ($this->expenditure['dependants'] == 0) {
      if ($this->expenditure['net'] > $this->quintiles['Q8']['without']['upper']) {
        $q = 'Q9';
      }
      else {
        if ($this->expenditure['net'] > $this->quintiles['Q7']['without']['upper']) {
          $q = 'Q8';
        }
        else {
          if ($this->expenditure['net'] > $this->quintiles['Q6']['without']['upper']) {
            $q = 'Q7';
          }
          else {
            if ($this->expenditure['net'] > $this->quintiles['Q5']['without']['upper']) {
              $q = 'Q6';
            }
            else {
              if ($this->expenditure['net'] > $this->quintiles['Q4']['without']['upper']) {
                $q = 'Q5';
              }
              else {
                if ($this->expenditure['net'] > $this->quintiles['Q3']['without']['upper']) {
                  $q = 'Q4';
                }
                else {
                  if ($this->expenditure['net'] > $this->quintiles['Q2']['without']['upper']) {
                    $q = 'Q3';
                  }
                  else {
                    if ($this->expenditure['net'] > $this->quintiles['Q1']['without']['upper']) {
                      $q = 'Q2';
                    }
                    else {
                      $q = 'Q1';
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    else {
      if ($this->expenditure['net'] > $this->quintiles['Q8']['with']['upper']) {
        $q = 'Q9';
      }
      else {
        if ($this->expenditure['net'] > $this->quintiles['Q7']['with']['upper']) {
          $q = 'Q8';
        }
        else {
          if ($this->expenditure['net'] > $this->quintiles['Q6']['with']['upper']) {
            $q = 'Q7';
          }
          else {
            if ($this->expenditure['net'] > $this->quintiles['Q5']['with']['upper']) {
              $q = 'Q6';
            }
            else {
              if ($this->expenditure['net'] > $this->quintiles['Q4']['with']['upper']) {
                $q = 'Q5';
              }
              else {
                if ($this->expenditure['net'] > $this->quintiles['Q3']['with']['upper']) {
                  $q = 'Q4';
                }
                else {
                  if ($this->expenditure['net'] > $this->quintiles['Q2']['with']['upper']) {
                    $q = 'Q3';
                  }
                  else {
                    if ($this->expenditure['net'] > $this->quintiles['Q1']['with']['upper']) {
                      $q = 'Q2';
                    }
                    else {
                      $q = 'Q1';
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return $q;
  }

  /**
   * Concat.
   */
  public function concat() {
    return $this->expenditure['applicants'] . '_' . $this->expenditure['dependants'] . '_' . $this->expenditure['quintile'];
  }

  /**
   * Calculate min/max expenditure.
   */
  public function minMaxExpenditure($a, $b) {
    if ($a == 0) {
      return 0;
    }
    else {
      return $this->rates[$this->expenditure['concat']][$b];
    }
  }

  /**
   * Calculate quintile lower/upper limit.
   */
  public function quintileLimit($a, $b) {
    if ($a == 0) {
      return $this->quintiles[$this->expenditure['quintile']]['without'][$b];
    }
    else {
      return $this->quintiles[$this->expenditure['quintile']]['with'][$b];
    }
  }

  /**
   * Calculate quintile differences.
   */
  public function quintileDifferences() {
    return $this->expenditure['upperLimit'] - $this->expenditure['lowerLimit'];
  }

  /**
   * Calculate income to lower quintile difference.
   */
  public function incomeLowerQuintileDifference() {
    return $this->expenditure['net'] - $this->expenditure['lowerLimit'];
  }

  /**
   * Calculate proportion.
   */
  public function proportion() {
    return $this->expenditure['incomeLowerQuintileDifference'] / ($this->expenditure['quintileDifference'] + 1);
  }

  /**
   * Calculate expenditure difference.
   */
  public function difference() {
    return $this->expenditure['max'] - $this->expenditure['min'];
  }

  /**
   * Calculate raw expenditure.
   */
  public function raw() {
    return BorrowAdditionalResult::roundUp(array_sum([
      $this->expenditure['proportion'] * $this->expenditure['difference'],
      $this->expenditure['min'],
    ]));
  }

  /**
   * Calculate final expenditure.
   */
  public function finalExpenditure() {
    return $this->expenditure['raw'] * $this->scalingFactor;
  }

}
