<?php

namespace Drupal\Tests\santander_mortgage_calculator\Unit;

/**
 *
 */
class SantanderBorrowAdditionalLoanResult {

  /**
   *
   */
  public static function round_up($n, $d = 0) {
    return round($n, $d);
  }

  /**
   *
   */
  public static function san_max(array $arr, $min = 0) {
    $m = max($arr);
    return $m > $min ? $m : $min;
  }

  /**
   * @var array
   */
  private $mortgage = [
    'applicants' => 1,
    'buyers' => '',
    'dependants' => 0,
    'term' => 25,
    'deposit' => 0,
  ];

  /**
   * @var array
   */
  private $income = [
    'basic' => [
      'a1' => ['taxable' => 0],
      'a2' => ['taxable' => 0],
    ],
    'guaranteed' => [
      'a1' => ['taxable' => 0, 'nonTaxable' => 0],
      'a2' => ['taxable' => 0, 'nonTaxable' => 0],
    ],
    'regular' => [
      'a1' => ['taxable' => 0, 'nonTaxable' => 0],
      'a2' => ['taxable' => 0, 'nonTaxable' => 0],
    ],
  ];

  /**
   * @var array
   */
  private $commitments = [
    'creditCard' => 0,
    'loans' => 0,
    'outgoings' => 0,
  ];

  /**
   * @var array
   */
  private $questions = [
    'propertyValue' => 0,
    'debtConsolidation' => 0,
    'loanp1amount' => 0,
    'loanp2amount' => 0,
    'loanp3amount' => 0,
    'loanp4amount' => 0,
    'loanp5amount' => 0,
    'loanp1term' => 0,
    'loanp2term' => 0,
    'loanp3term' => 0,
    'loanp4term' => 0,
    'loanp5term' => 0,
    'loanp1Repayment' => 0,
    'loanp2Repayment' => 0,
    'loanp3Repayment' => 0,
    'loanp4Repayment' => 0,
    'loanp5Repayment' => 0,
    'newTerm' => 0,
  ];

  /**
   * Set Params.
   *
   * @param $data
   */
  public function setParams($data) {
    $this->mortgage['applicants'] = $data['applicants'];
    $this->mortgage['dependants'] = ($data['dependants'] == '+3') ? 3 : $data['dependants'];
    $this->mortgage['newTerm'] = $data['newTerm'];

    $this->questions['propertyValue'] = $data['propertyValue'];
    $this->questions['newTerm'] = $data['newTerm'];

    $this->questions['loanp1amount'] = $data['p1Amount'];
    $this->questions['loanp2amount'] = $data['p2Amount'];
    $this->questions['loanp3amount'] = $data['p3Amount'];
    $this->questions['loanp4amount'] = $data['p4Amount'];
    $this->questions['loanp5amount'] = $data['p5Amount'];

    $this->questions['loanp1term'] = $data['p1Term'];
    $this->questions['loanp2term'] = $data['p2Term'];
    $this->questions['loanp3term'] = $data['p3Term'];
    $this->questions['loanp4term'] = $data['p4Term'];
    $this->questions['loanp5term'] = $data['p5Term'];

    $this->questions['loanp1Repayment'] = $data['p1Repayment'];
    $this->questions['loanp2Repayment'] = $data['p2Repayment'];
    $this->questions['loanp3Repayment'] = $data['p3Repayment'];
    $this->questions['loanp4Repayment'] = $data['p4Repayment'];
    $this->questions['loanp5Repayment'] = $data['p5Repayment'];

    $this->questions['debtConsolidation'] = $data['debtConRestriction'];

    $this->income['basic']['a1']['taxable'] = $data['basicA1taxable'];
    $this->income['basic']['a2']['taxable'] = $data['basicA2taxable'];

    $this->income['guaranteed']['a1']['taxable'] = $data['guaranteedA1taxable'];
    $this->income['guaranteed']['a2']['taxable'] = $data['guaranteedA2taxable'];

    $this->income['guaranteed']['a1']['nonTaxable'] = $data['guaranteedA1nonTaxable'];
    $this->income['guaranteed']['a2']['nonTaxable'] = $data['guaranteedA2nonTaxable'];

    $this->income['regular']['a1']['taxable'] = $data['regularA1taxable'];
    $this->income['regular']['a2']['taxable'] = $data['regularA2taxable'];

    $this->income['regular']['a1']['nonTaxable'] = $data['regularA1nonTaxable'];
    $this->income['regular']['a2']['nonTaxable'] = $data['regularA2nonTaxable'];

    $this->commitments['creditCard'] = $data['outstanding'];
    $this->commitments['loans'] = $data['creditCommitments'];
    $this->commitments['outgoings'] = $data['financialCommitments'];

    /*        $fileHandle = fopen("test.log", "a");
    fwrite($fileHandle,   "INCOME \n");
    fwrite($fileHandle, print_r($this->income, TRUE) . "\n");
    fwrite($fileHandle,   "MORTGAGE \n");
    fwrite($fileHandle, print_r($this->mortgage, TRUE) . "\n");
    fwrite($fileHandle,   "COMMITMENTS \n");
    fwrite($fileHandle, print_r($this->commitments, TRUE) . "\n");
    fwrite($fileHandle,   "QUESTIONS \n");
    fwrite($fileHandle, print_r($this->questions, TRUE) . "\n");
    fclose($fileHandle);*/

  }

  /**
   * Validation.
   *
   * @param $data
   *
   * @return mixed
   */
  public function validate(&$data) {
    $error = 0;

    // $fields = array(
    //   'applicants' => 'required|in:single,joint',
    //   'dependants' => 'required|in:0,1,2,+3',
    //   'propertyValue' => 'required|numeric|min:0',
    //   'newTerm' =>  'required|numeric|min:0',
    //   'p1Amount' => 'required|numeric|min:0',
    //   'p1Term' => 'required|numeric|min:0',
    //   'p1Repayment' => 'required|in:0,IO,C&I',
    //   'p2Amount' => 'required|numeric|min:0',
    //   'p2Term' => 'required|numeric|min:0',
    //   'p2Repayment' => 'required|in:0,IO,C&I',
    //   'p3Amount' => 'required|numeric|min:0',
    //   'p3Term' => 'required|numeric|min:0',
    //   'p3Repayment' => 'required|in:0,IO,C&I',
    //   'p4Amount' => 'required|numeric|min:0',
    //   'p4Term' => 'required|numeric|min:0',
    //   'p4Repayment' => 'required|in:0,IO,C&I',
    //   'p5Amount' => 'required|numeric|min:0',
    //   'p5Term' => 'required|numeric|min:0',
    //   'p5Repayment' => 'required|in:0,IO,C&I',
    //   'basicA1taxable' => 'required|numeric|min:0',
    //   'basicA2taxable' => 'required|numeric|min:0',
    //   'guaranteedA1taxable' => 'required|numeric|min:0',
    //   'guaranteedA2taxable' => 'required|numeric|min:0',
    //   'guaranteedA1nonTaxable' => 'required|numeric|min:0',
    //   'guaranteedA2nonTaxable' => 'required|numeric|min:0',
    //   'regularA1taxable' => 'required|numeric|min:0',
    //   'regularA2taxable' => 'required|numeric|min:0',
    //   'regularA1nonTaxable' => 'required|numeric|min:0',
    //   'regularA2nonTaxable' => 'required|numeric|min:0',
    //   'outstanding' => 'required|numeric|min:0',
    //   'creditCommitments' => 'required|numeric|min:0',
    //   'financialCommitments' => 'required|numeric|min:0'
    // );
    $fields = [
      'applicants' => ['required' => TRUE],
      'dependants' => ['required' => TRUE],
      'propertyValue' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'newTerm' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'p1Amount' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'p1Term' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'p1Repayment' => ['required' => TRUE],
      'p2Amount' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'p2Term' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'p2Repayment' => ['required' => TRUE],
      'p3Amount' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'p3Term' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'p3Repayment' => ['required' => TRUE],
      'p4Amount' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'p4Term' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'p4Repayment' => ['required' => TRUE],
      'p5Amount' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'p5Term' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'p5Repayment' => ['required' => TRUE],
      'basicA1taxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'basicA2taxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'guaranteedA1taxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'guaranteedA2taxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'guaranteedA1nonTaxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'guaranteedA2nonTaxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'regularA1taxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'regularA2taxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'regularA1nonTaxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'regularA2nonTaxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'outstanding' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'creditCommitments' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'financialCommitments' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
    ];

    foreach ($fields as $name => $fieldInfo) {
      if (!isset($data[$name]) && $fieldInfo['required']) {
        return FALSE;
      }

      if (isset($fieldInfo['type']) && $fieldInfo['type'] == 'numeric') {
        $data[$name] = floatval($data[$name]);
        if (isset($fieldInfo['min']) && $data[$name] < $fieldInfo['min']) {
          return FALSE;
        }
        if (isset($fieldInfo['max']) && $data[$name] > $fieldInfo['max']) {
          return FALSE;
        }
      }
    }

    return TRUE;
  }

  /**
   *
   */
  public function vals() {

    $i = $this->calculateIncome();
    $e = $this->calculateExpenditure($i);
    $c = $this->calculateCredit();
    $l = $this->calculateLoan($i, $e, $c);

    return [
      'income' => $i,
      'Expenditure' => $e,
      'Credit' => $c,
      'Loan' => $l,
    ];
  }

  /**
   *
   */
  public function calculate() {

    $i = $this->calculateIncome();
    $e = $this->calculateExpenditure($i);
    $c = $this->calculateCredit();
    $l = $this->calculateLoan($i, $e, $c);

    /*
    $fileHandle = fopen("test.log", "a");
    fwrite($fileHandle,   "i \n");
    fwrite($fileHandle, print_r($i, TRUE) . "\n");
    fwrite($fileHandle,   "e \n");
    fwrite($fileHandle, print_r($e, TRUE) . "\n");
    fwrite($fileHandle,   "c \n");
    fwrite($fileHandle, print_r($c, TRUE) . "\n");
    fwrite($fileHandle,   "l \n");
    fwrite($fileHandle, print_r($l, TRUE) . "\n");
    fclose($fileHandle);
     */

    return $l['loan'];
  }

  /**
   * Calculate Total NET monthly income.
   */
  private function calculateIncome() {
    $o = new SantanderBorrowAdditionalLoanResultIncome($this->income);
    return $o->getIncome();
  }

  /**
   * Calculate Expenditure (low/medium)
   */
  private function calculateExpenditure($i) {
    $o = new SantanderBorrowAdditionalLoanResultExpenditure($this->mortgage, $this->income, $i);
    return $o->getExpenditure();
  }

  /**
   * Calculate credit commitments.
   */
  private function calculateCredit() {
    $o = new SantanderBorrowAdditionalLoanResultCredit($this->commitments);
    return $o->getCredit();
  }

  /**
   * Calculate max loan.
   */
  private function calculateLoan($i, $e, $c) {
    $o = new SantanderBorrowAdditionalLoanResultLoan($this->mortgage, $this->income, $this->commitments, $i, $e, $c, $this->questions);
    return $o->getLoan();
  }

}
/**
 *
 */
class SantanderBorrowAdditionalLoanResultIncome {
  /**
   * Rates.
   */
  private $taxFreeAllowance = 11850; /**
                                      * ï¿½.
                                      */

  protected $rates = [
    'starting' => [
  // 10%
      'rate' => 0.1,
  // ï¿½.
      'low' => 0,
  // ï¿½.
      'high' => 0,
      /*'dividends' => 0*/
    ],
    'basic' => [
    // 20%
      'rate' => 0.2,
    // ï¿½.
      'low' => 0,
    // ï¿½.
      'high' => 34500,
      /*'dividends' => 0.1 // 10% for dividents DIP*/
    ],
    'higher' => [
    // 40%
      'rate' => 0.4,
    // ï¿½.
      'low' => 34501,
    // ï¿½.
      'high' => 150000,
      /*'dividends' => 0.325 // 32.5% for dividents DIP*/
    ],
    'additional' => [
    // 45%
      'rate' => 0.45,
    // ï¿½.
      'low' => 150001,
      /*'dividends' => 0.375 // 37.5% for dividents DIP*/
    ],
  ];

  /**
   * Private $director_tax_credit = 0.1; .
   */


  private $primaryNiThreshold = [
  // 12%
    'rate' => 0.12,
  // ï¿½.
    'low' => 8424,
  // ï¿½ //DIP upper limit is 41860.
    'high' => 46349,
  ];

  private $upperEarningsNiLimit = [
  // 2%
    'rate' => 0.02,
  // ï¿½ //DIP low limit is 41861.
    'low' => 46350,
  ];

  /**
   * 95%.
   */
  private $scalingFactor = 0.95;

  /**
   * Income.
   */
  private $income = NULL;

  /**
   * Secondary Weighting.
   */
  /**
   * 65%.
   */
  private $secondaryWeighting = 0.65;

  /**
   * Gross.
   */
  private $gross = [
    'a1' => [
  // ï¿½.
      'taxable' => 0,
  // ï¿½.
      'taxFreeAllowance' => 0,
  // ï¿½.
      'tax10' => 0,
  // ï¿½.
      'tax20' => 0,
  // ï¿½.
      'tax40' => 0,
  // ï¿½.
      'tax45' => 0,
  // ï¿½.
      'ni2' => 0,
  // ï¿½.
      'ni12' => 0,
  // ï¿½.
      'div10' => 0,
  // ï¿½.
      'div325' => 0,
  // ï¿½.
      'div375' => 0,
      'annual' => [
  // ï¿½.
        'taxable' => 0,
  // ï¿½.
        'nonTaxable' => 0,
        'dividends' => 0,
      ],
      'monthly' => [
        'taxable' => [
      // ï¿½.
          'total' => 0,
      // %
          'basic' => 0,
      // %
          'guaranteed' => 0,
      // %
          'regular' => 0,
      // ï¿½.
          'reduced' => 0,
        ],
        'nonTaxable' => [
        // ï¿½.
          'total' => 0,
        // %
          'basic' => 0,
        // %
          'guaranteed' => 0,
        // %
          'regular' => 0,
        // ï¿½.
          'reduced' => 0,
        ],
        // ï¿½.
        'total' => 0,
      ],
      // ï¿½.
      'total' => 0,
    ],
    'a2' => [
    // ï¿½.
      'taxable' => 0,
    // ï¿½.
      'taxFreeAllowance' => 0,
    // ï¿½.
      'tax10' => 0,
    // ï¿½.
      'tax20' => 0,
    // ï¿½.
      'tax40' => 0,
    // ï¿½.
      'tax45' => 0,
    // ï¿½.
      'ni2' => 0,
    // ï¿½.
      'ni12' => 0,
    // ï¿½.
      'div10' => 0,
    // ï¿½.
      'div325' => 0,
    // ï¿½.
      'div375' => 0,
      'annual' => [
    // ï¿½.
        'taxable' => 0,
    // ï¿½.
        'nonTaxable' => 0,
        'dividends' => 0,
      ],
      'monthly' => [
        'taxable' => [
      // ï¿½.
          'total' => 0,
      // %
          'basic' => 0,
      // %
          'guaranteed' => 0,
      // %
          'regular' => 0,
      // ï¿½.
          'reduced' => 0,
        ],
        'nonTaxable' => [
        // ï¿½.
          'total' => 0,
        // %
          'basic' => 0,
        // %
          'guaranteed' => 0,
        // %
          'regular' => 0,
        // ï¿½.
          'reduced' => 0,
        ],
        // ï¿½.
        'total' => 0,
      ],
      // ï¿½.
      'total' => 0,
    ],
    // ï¿½.
    'total' => 0,
    // ï¿½.
    'totalMonthly' => 0,
  ];

  /**
   * Constructor.
   */
  public function __construct(array $arr) {
    $this->income = $arr;
  }

  /**
   * Calculate Total NET monthly income.
   */
  public function getIncome() {
    // Calculate gross income for applicant.
    $this->gross['a1']['total'] = $this->grossApplicant([
      $this->income['basic']['a1']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['guaranteed']['a1']['nonTaxable'],
      $this->income['regular']['a1']['taxable'],
      $this->income['regular']['a1']['nonTaxable'],
    ]);
    $this->gross['a2']['total'] = $this->grossApplicant([
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['guaranteed']['a2']['nonTaxable'],
      $this->income['regular']['a2']['taxable'],
      $this->income['regular']['a2']['nonTaxable'],
    ]);
    // Calculate total gross income.
    $this->gross['total'] = $this->totalGross();
    // Calculate total gross taxable income for applicant.
    $this->gross['a1']['taxable'] = $this->grossTaxableApplicant([
      $this->income['basic']['a1']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['regular']['a1']['taxable'],
    ], 'a1');
    $this->gross['a2']['taxable'] = $this->grossTaxableApplicant([
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['regular']['a2']['taxable'],
    ], 'a2');
    // Calculate final tax free allowance.
    $this->gross['a1']['taxFreeAllowance'] = $this->finalTaxFreeAllowance('a1');
    $this->gross['a2']['taxFreeAllowance'] = $this->finalTaxFreeAllowance('a2');
    // Calculate tax at 10%.
    $this->gross['a1']['tax10'] = $this->tax10('a1');
    $this->gross['a2']['tax10'] = $this->tax10('a2');
    // Calculate tax at 20%.
    $this->gross['a1']['tax20'] = $this->tax20('a1');
    $this->gross['a2']['tax20'] = $this->tax20('a2');
    // Calculate tax at 40%.
    $this->gross['a1']['tax40'] = $this->tax40('a1');
    $this->gross['a2']['tax40'] = $this->tax40('a2');
    // Calculate tax at 45%.
    $this->gross['a1']['tax45'] = $this->tax45('a1');
    $this->gross['a2']['tax45'] = $this->tax45('a2');
    // Calculate NI at 12%.
    $this->gross['a1']['ni12'] = $this->ni12('a1');
    $this->gross['a2']['ni12'] = $this->ni12('a2');
    // Calculate NI at 2%.
    $this->gross['a1']['ni2'] = $this->ni2('a1');
    $this->gross['a2']['ni2'] = $this->ni2('a2');

    // Calculate dividends if applicable (DIP)
    /*if(isset($this->income['dividends'])) {
    //10% dividend rate
    $this->gross['a1']['div10'] = $this->div10('a1');
    $this->gross['a2']['div10'] = $this->div10('a2');
    //32.5% dividend rate
    $this->gross['a1']['div325'] = $this->div325('a1');
    $this->gross['a2']['div325'] = $this->div325('a2');
    //37.5% dividend rate
    $this->gross['a1']['div375'] = $this->div375('a1');
    $this->gross['a2']['div375'] = $this->div375('a2');

    // Calculate Net annual dividend
    $this->gross['a1']['annual']['dividends'] = $this->annualDividend('a1');
    $this->gross['a2']['annual']['dividends'] = $this->annualDividend('a2');
    }*/
    // Calculate Net annual income (taxable)
    $this->gross['a1']['annual']['taxable'] = $this->annualTaxable('a1');
    $this->gross['a2']['annual']['taxable'] = $this->annualTaxable('a2');
    // Calculate Net annual income (non taxable)
    $this->gross['a1']['annual']['nonTaxable'] = $this->annualNonTaxable([
      $this->income['guaranteed']['a1']['nonTaxable'],
      $this->income['regular']['a1']['nonTaxable'],
    ]);
    $this->gross['a2']['annual']['nonTaxable'] = $this->annualNonTaxable([
      $this->income['guaranteed']['a2']['nonTaxable'],
      $this->income['regular']['a2']['nonTaxable'],
    ]);
    // Calculate Net monthly income (taxable)
    $this->gross['a1']['monthly']['taxable']['total'] = $this->monthlyTaxable('a1');
    $this->gross['a2']['monthly']['taxable']['total'] = $this->monthlyTaxable('a2');
    // Calculate taxable proportion (%)
    $this->gross['a1']['monthly']['taxable']['basic'] = $this->taxableProportion([
      $this->income['basic']['a1']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['regular']['a1']['taxable'],
    ], $this->income['basic']['a1']['taxable']);
    $this->gross['a1']['monthly']['taxable']['guaranteed'] = $this->taxableProportion([
      $this->income['basic']['a1']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['regular']['a1']['taxable'],
    ], $this->income['guaranteed']['a1']['taxable']);
    $this->gross['a1']['monthly']['taxable']['regular'] = $this->taxableProportion([
      $this->income['basic']['a1']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['regular']['a1']['taxable'],
    ], $this->income['regular']['a1']['taxable']);
    $this->gross['a2']['monthly']['taxable']['basic'] = $this->taxableProportion([
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['regular']['a2']['taxable'],
    ], $this->income['basic']['a2']['taxable']);
    $this->gross['a2']['monthly']['taxable']['guaranteed'] = $this->taxableProportion([
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['regular']['a2']['taxable'],
    ], $this->income['guaranteed']['a2']['taxable']);
    $this->gross['a2']['monthly']['taxable']['regular'] = $this->taxableProportion([
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['regular']['a2']['taxable'],
    ], $this->income['regular']['a2']['taxable']);
    // Calculate Net monthly income (taxable reduced)
    $this->gross['a1']['monthly']['taxable']['reduced'] = $this->monthlyTaxableReduced('a1');
    $this->gross['a2']['monthly']['taxable']['reduced'] = $this->monthlyTaxableReduced('a2');
    // Calculate Net monthly income (non taxable)
    $this->gross['a1']['monthly']['nonTaxable']['total'] = $this->monthlyNonTaxable('a1');
    $this->gross['a2']['monthly']['nonTaxable']['total'] = $this->monthlyNonTaxable('a2');
    // Calculate Gross non taxable proportion (%)
    $this->gross['a1']['monthly']['nonTaxable']['guaranteed'] = $this->nonTaxableProportion('a1', $this->income['guaranteed']['a1']['nonTaxable']);
    $this->gross['a1']['monthly']['nonTaxable']['regular'] = $this->nonTaxableProportion('a1', $this->income['regular']['a1']['nonTaxable']);
    $this->gross['a2']['monthly']['nonTaxable']['guaranteed'] = $this->nonTaxableProportion('a2', $this->income['guaranteed']['a2']['nonTaxable']);
    $this->gross['a2']['monthly']['nonTaxable']['regular'] = $this->nonTaxableProportion('a2', $this->income['regular']['a2']['nonTaxable']);
    // Calculate Net monthly income (non taxable reduced)
    $this->gross['a1']['monthly']['nonTaxable']['reduced'] = $this->monthlyNonTaxableReduced('a1');
    $this->gross['a2']['monthly']['nonTaxable']['reduced'] = $this->monthlyNonTaxableReduced('a2');
    // Calculate total Net monthly income per applicant.
    $this->gross['a1']['monthly']['total'] = $this->totalMonthlyIncomeApplicant('a1');
    $this->gross['a2']['monthly']['total'] = $this->totalMonthlyIncomeApplicant('a2');
    $this->gross['totalMonthly'] = $this->totalMonthlyIncome();
    return $this->gross;
  }

  /**
   * Calculate gross income for applicant.
   */
  public function grossApplicant(array $arr) {
    return array_sum($arr);
  }

  /**
   * Calculate total gross income.
   */
  public function totalGross() {
    return array_sum([
      $this->gross['a1']['total'],
      $this->gross['a2']['total'],
    ]);
  }

  /**
   * Calculate total gross taxable income for applicant.
   */
  public function grossTaxableApplicant(array $arr, $applicant = NULL) {
    $result = SantanderBorrowAdditionalLoanResult::san_max([array_sum($arr)]);
    if (isset($this->income['deduction'])) {
      return $result - (SantanderBorrowResult::round_up($this->income['deduction'][$applicant]['taxFree'], 2) * 12);
    }
    else {
      return $result;
    }
  }

  /**
   * Calculate final tax free allowance.
   */
  public function finalTaxFreeAllowance($a) {
    if (isset($this->income['dividends'])) {
      return SantanderBorrowAdditionalLoanResult::san_max([
        $this->taxFreeAllowance - (0.5 * SantanderBorrowAdditionalLoanResult::san_max([
          ($this->gross[$a]['taxable'] + (10 / 9) * $this->income['dividends'][$a]) - 100000,
        ])),
      ]);
    }
    else {
      return SantanderBorrowAdditionalLoanResult::san_max([
        $this->taxFreeAllowance - (0.5 * SantanderBorrowAdditionalLoanResult::san_max([
          $this->gross[$a]['taxable'] - 100000,
        ])),
      ]);
    }
  }

  /**
   * Calculate tax at 10%.
   */
  public function tax10($a) {
    if ($this->gross[$a]['taxable'] > $this->gross[$a]['taxFreeAllowance']) {
      if (($this->gross[$a]['taxable'] - $this->gross[$a]['taxFreeAllowance']) > $this->rates['starting']['high']) {
        return $this->rates['starting']['high'] * $this->rates['starting']['rate'];
      }
      else {
        return ($this->gross[$a]['taxable'] - $this->gross[$a]['taxFreeAllowance']) * $this->rates['starting']['rate'];
      }
    }
    else {
      return 0;
    }
  }

  /**
   * Calculate tax at 20%.
   */
  public function tax20($a) {
    if ($this->gross[$a]['taxable'] > $this->gross[$a]['taxFreeAllowance']) {
      if (($this->gross[$a]['taxable'] - $this->gross[$a]['taxFreeAllowance']) > $this->rates['basic']['high']) {
        return $this->rates['basic']['high'] * $this->rates['basic']['rate'];
      }
      else {
        return ($this->gross[$a]['taxable'] - $this->gross[$a]['taxFreeAllowance']) * $this->rates['basic']['rate'];
      }
    }
    else {
      return 0;
    }
  }

  /**
   * Calculate tax at 40%.
   */
  public function tax40($a) {
    if ($this->gross[$a]['taxable'] > ($this->gross[$a]['taxFreeAllowance'] + $this->rates['basic']['high'])) {
      if (($this->gross[$a]['taxable'] - $this->gross[$a]['taxFreeAllowance'] - $this->rates['basic']['high']) > ($this->rates['higher']['high'] - $this->rates['basic']['high'])) {
        return ($this->rates['higher']['high'] - $this->rates['basic']['high']) * $this->rates['higher']['rate'];
      }
      else {
        return ($this->gross[$a]['taxable'] - $this->gross[$a]['taxFreeAllowance'] - $this->rates['basic']['high']) * $this->rates['higher']['rate'];
      }
    }
    else {
      return 0;
    }
  }

  /**
   * Calculate tax at 45%.
   */
  public function tax45($a) {
    if ($this->gross[$a]['taxable'] > ($this->rates['higher']['high'] + $this->gross[$a]['taxFreeAllowance'])) {
      return ($this->gross[$a]['taxable'] - $this->rates['higher']['high'] - $this->gross[$a]['taxFreeAllowance']) * $this->rates['additional']['rate'];
    }
    else {
      return 0;
    }
  }

  /**
   * Calculate NI at 2%.
   */
  public function ni2($a) {
    if (isset($this->income['pension'])) {
      $ni = $this->gross[$a]['taxable'] - $this->income['pension'][$a];
    }
    else {
      $ni = $this->gross[$a]['taxable'];
    }
    if ($ni > $this->primaryNiThreshold['high']) {
      return ($ni - $this->primaryNiThreshold['high']) * $this->upperEarningsNiLimit['rate'];
    }
    else {
      return 0;
    }
  }

  /**
   * Calculate NI at 12%.
   */
  public function ni12($a) {
    if (isset($this->income['pension'])) {
      $ni = $this->gross[$a]['taxable'] - $this->income['pension'][$a];
    }
    else {
      $ni = $this->gross[$a]['taxable'];
    }
    if ($ni > $this->primaryNiThreshold['low']) {
      if ($ni > $this->primaryNiThreshold['high']) {
        return ($this->primaryNiThreshold['high'] - $this->primaryNiThreshold['low']) * $this->primaryNiThreshold['rate'];
      }
      else {
        return ($ni - $this->primaryNiThreshold['low']) * $this->primaryNiThreshold['rate'];
      }
    }
    else {
      return 0;
    }
  }

  /*
   * calculate 10% dividends
   * return Float
   */
  /*public function div10($a)
  {
  if($this->income['dividends'][$a] > $this->rates['basic']['high'])
  {
  return $this->applyTax($this->rates['basic']['high'] , 'basic');
  } else {
  return $this->applyTax($this->income['dividends'][$a] , 'basic');
  }
  }*/

  /*
   * calculate 32.5% dividends
   * return Float
   */
  /*public function div325($a)
  {
  $tax = 0;
  if($this->income['dividends'][$a] > 0)
  {
  if($this->upper_amt_to_tax($a) > 0)
  {
  $tax = $this->applyTax( ((10/9) * $this->income['dividends'][$a] - $this->upper_amt_to_tax($a)) , 'higher' );
  } else {
  if($this->gross[$a]['taxable'] > $this->rates['basic']['high'])
  {
  $n1 = ((10/9) * $this->income['dividends'][$a]);
  $n2 = ($this->gross[$a]['taxable'] + (10/9) *  $this->income['dividends'][$a] - $this->rates['basic']['high'] - $this->gross[$a]['taxFreeAllowance']);
  if($n1 < $n2){
  $tax = $this->applyTax($n1 , 'higher');
  } else {
  $tax = $this->applyTax($n2 , 'higher');
  }
  } else {
  $tax = 0;
  }
  }
  }
  if($tax > 0)
  {
  return $tax;
  } else {
  return 0;
  }
  }*/

  /*
   * calculate 37.5% dividends
   * return Float
   */
  /*public function div375($a)
  {
  if($this->gross[$a]['taxable'] > $this->rates['additional']['low'])
  {
  return $this->applyTax( ((10/9) * $this->income['dividends'][$a]) , 'additional' );
  } else {
  return 0;
  }
  }*/

  /*
   * work out value of amount that needs to be taxed
   * return Float
   */
  /*private function upper_amt_to_tax($a)
  {
  return ($this->gross[$a]['taxable'] + ((10/9) * $this->income['dividends'][$a])) - $this->rates['higher']['high'] - $this->gross[$a]['taxFreeAllowance'];
  }*/

  /*
   * takes the sum of what to tax and taxes it
   * return Float.
   */
  /*private function applyTax($sum , $rate)
  {
  return $sum * ($this->rates[$rate]['dividends'] - $this->director_tax_credit);
  }

  /*
   * calculate annual dividends minus tax
   * reuturn Float
   */
  /*public function annualDividend($a)
  {
  return $this->income['dividends'][$a] - $this->annualDividendTax($a);
  } */

  /*
   * sum of all annual dividens (fixed for tax)
   * reuturn Float
   */
  /*private function annualDividendTax($a)
  {
  return array_sum(array(
  $this->gross[$a]['div10'],
  $this->gross[$a]['div325'],
  $this->gross[$a]['div375'],
  ));
  }*/

  /**
   * Calculate Net annual income (taxable)
   */
  public function annualTaxable($a) {
    return $this->gross[$a]['taxable'] - array_sum([
      $this->gross[$a]['tax10'],
      $this->gross[$a]['tax20'],
      $this->gross[$a]['tax40'],
      $this->gross[$a]['tax45'],
      $this->gross[$a]['ni12'],
      $this->gross[$a]['ni2'],
    ]);
  }

  /**
   * Calculate Net annual income (non taxable)
   */
  public function annualNonTaxable(array $arr) {
    return array_sum($arr);
  }

  /**
   * Calculate Net monthly income (taxable)
   */
  public function monthlyTaxable($a) {
    return SantanderBorrowAdditionalLoanResult::san_max([
      SantanderBorrowAdditionalLoanResult::san_max([
        $this->gross[$a]['annual']['taxable'] / 12,
      ]),
    ]);
  }

  /**
   * Calculate Gross taxable proportion (%)
   */
  public function taxableProportion(array $arr, $a) {
    $v = array_sum($arr);
    if ($v == 0) {
      return 0;
    }
    else {
      return $a / $v;
    }
  }

  /**
   * Calculate Net monthly income (taxable reduced)
   */
  public function monthlyTaxableReduced($a) {
    return array_sum([
      $this->gross[$a]['monthly']['taxable']['basic'],
      $this->gross[$a]['monthly']['taxable']['guaranteed'],
      $this->gross[$a]['monthly']['taxable']['regular'] * $this->secondaryWeighting,
    ]) * $this->gross[$a]['monthly']['taxable']['total'];
  }

  /**
   * Calculate Net monthly income (non taxable)
   */
  public function monthlyNonTaxable($a) {
    if (isset($this->income['dividends'])) {
      return SantanderBorrowAdditionalLoanResult::san_max([
        ($this->gross[$a]['annual']['nonTaxable'] / 12) - ($this->annualDividendTax($a) / 12),
      ]);
    }
    else {
      return SantanderBorrowAdditionalLoanResult::san_max([
        ($this->gross[$a]['annual']['nonTaxable'] / 12),
      ]);
    }
  }

  /**
   * Calculate Gross non taxable proportion (%)
   */
  public function nonTaxableProportion($a, $b) {
    if ($this->gross[$a]['annual']['nonTaxable'] == 0) {
      return 0;
    }
    else {
      return $b / $this->gross[$a]['annual']['nonTaxable'];
    }
  }

  /**
   * Calculate Net monthly income (non taxable reduced)
   */
  public function monthlyNonTaxableReduced($a) {
    return array_sum([
      $this->gross[$a]['monthly']['nonTaxable']['basic'],
      $this->gross[$a]['monthly']['nonTaxable']['guaranteed'],
      $this->gross[$a]['monthly']['nonTaxable']['regular'] * $this->secondaryWeighting,
    ]) * $this->gross[$a]['monthly']['nonTaxable']['total'];
  }

  /**
   * Calculate total Net monthly income per applicant.
   */
  public function totalMonthlyIncomeApplicant($a) {
    return array_sum([
      $this->gross[$a]['monthly']['nonTaxable']['reduced'],
      $this->gross[$a]['monthly']['taxable']['reduced'],
    ]);
  }

  /**
   * Calculate total Net monthly income.
   */
  public function totalMonthlyIncome() {
    // Ensure there is no sclaing factor when it comes to the DIP calculator.
    $total = array_sum([
      $this->gross['a1']['monthly']['total'],
      $this->gross['a2']['monthly']['total'],
    ]);
    if (isset($this->income['deduction'])) {
      return SantanderBorrowAdditionalLoanResult::round_up($total, 2)
        - SantanderBorrowAdditionalLoanResult::round_up($this->income['deduction']['a1']['postTax'], 2)
        - SantanderBorrowAdditionalLoanResult::round_up($this->income['deduction']['a2']['postTax'], 2)
        - SantanderBorrowAdditionalLoanResult::round_up($this->income['deduction']['a1']['taxFree'], 2)
        - SantanderBorrowAdditionalLoanResult::round_up($this->income['deduction']['a2']['taxFree'], 2);
    }
    else {
      return SantanderBorrowAdditionalLoanResult::round_up($total) * $this->scalingFactor;
    }
  }

}
/**
 *
 */
class SantanderBorrowAdditionalLoanResultExpenditure {
  private $quintiles = [
    'Q1' => [
      'without' => [
        'lower' => 0,
        'upper' => 991,
      ],
      'with' => [
        'lower' => 0,
        'upper' => 840,
      ],
    ],
    'Q2' => [
      'without' => [
        'lower' => 992,
        'upper' => 1822,
      ],
      'with' => [
        'lower' => 841,
        'upper' => 1559,
      ],
    ],
    'Q3' => [
      'without' => [
        'lower' => 1823,
        'upper' => 2654,
      ],
      'with' => [
        'lower' => 1560,
        'upper' => 2309,
      ],
    ],
    'Q4' => [
      'without' => [
        'lower' => 2655,
        'upper' => 3700,
      ],
      'with' => [
        'lower' => 2310,
        'upper' => 3327,
      ],
    ],
    'Q5' => [
      'without' => [
        'lower' => 3701,
        'upper' => 5707,
      ],
      'with' => [
        'lower' => 3328,
        'upper' => 5247,
      ],
    ],
    'Q6' => [
      'without' => [
        'lower' => 5708,
        'upper' => 7199,
      ],
      'with' => [
        'lower' => 5248,
        'upper' => 7199,
      ],
    ],
    'Q7' => [
      'without' => [
        'lower' => 7200,
        'upper' => 12318,
      ],
      'with' => [
        'lower' => 7200,
        'upper' => 12318,
      ],
    ],
    'Q8' => [
      'without' => [
        'lower' => 12319,
        'upper' => 99999,
      ],
      'with' => [
        'lower' => 12319,
        'upper' => 99999,
      ],
    ],
    'Q9' => [
      'without' => [
        'lower' => 100000,
        'upper' => 999999,
      ],
      'with' => [
        'lower' => 100000,
        'upper' => 999999,
      ],
    ],
  ];

  private $rates = [
    '1_0_Q1' => [
      'min' => 613,
      'max' => 613,
    ],
    '1_1_Q1' => [
      'min' => 644,
      'max' => 644,
    ],
    '1_2_Q1' => [
      'min' => 684,
      'max' => 684,
    ],
    '1_3+_Q1' => [
      'min' => 718,
      'max' => 718,
    ],
    '1_0_Q2' => [
      'min' => 613,
      'max' => 613,
    ],
    '1_1_Q2' => [
      'min' => 644,
      'max' => 648,
    ],
    '1_2_Q2' => [
      'min' => 684,
      'max' => 687,
    ],
    '1_3+_Q2' => [
      'min' => 718,
      'max' => 720,
    ],
    '1_0_Q3' => [
      'min' => 613,
      'max' => 777,
    ],
    '1_1_Q3' => [
      'min' => 648,
      'max' => 747,
    ],
    '1_2_Q3' => [
      'min' => 687,
      'max' => 789,
    ],
    '1_3+_Q3' => [
      'min' => 720,
      'max' => 817,
    ],
    '1_0_Q4' => [
      'min' => 777,
      'max' => 949,
    ],
    '1_1_Q4' => [
      'min' => 747,
      'max' => 1036,
    ],
    '1_2_Q4' => [
      'min' => 789,
      'max' => 1095,
    ],
    '1_3+_Q4' => [
      'min' => 817,
      'max' => 1135,
    ],
    '1_0_Q5' => [
      'min' => 949,
      'max' => 1278,
    ],
    '1_1_Q5' => [
      'min' => 1036,
      'max' => 1274,
    ],
    '1_2_Q5' => [
      'min' => 1095,
      'max' => 1342,
    ],
    '1_3+_Q5' => [
      'min' => 1135,
      'max' => 1394,
    ],
    '1_0_Q6' => [
      'min' => 1278,
      'max' => 1468,
    ],
    '1_1_Q6' => [
      'min' => 1274,
      'max' => 1523,
    ],
    '1_2_Q6' => [
      'min' => 1342,
      'max' => 1604,
    ],
    '1_3+_Q6' => [
      'min' => 1394,
      'max' => 1667,
    ],
    '1_0_Q7' => [
      'min' => 1468,
      'max' => 1958,
    ],
    '1_1_Q7' => [
      'min' => 1523,
      'max' => 2031,
    ],
    '1_2_Q7' => [
      'min' => 1604,
      'max' => 2139,
    ],
    '1_3+_Q7' => [
      'min' => 1667,
      'max' => 2222,
    ],
    '1_0_Q8' => [
      'min' => 1958,
      'max' => 10351,
    ],
    '1_1_Q8' => [
      'min' => 2031,
      'max' => 10732,
    ],
    '1_2_Q8' => [
      'min' => 2139,
      'max' => 11303,
    ],
    '1_3+_Q8' => [
      'min' => 2222,
      'max' => 11728,
    ],
    '1_0_Q9' => [
      'min' => 10351,
      'max' => 10351,
    ],
    '1_1_Q9' => [
      'min' => 10732,
      'max' => 10732,
    ],
    '1_2_Q9' => [
      'min' => 11303,
      'max' => 11303,
    ],
    '1_3+_Q9' => [
      'min' => 11728,
      'max' => 11728,
    ],
    '2+_0_Q1' => [
      'min' => 1093,
      'max' => 1093,
    ],
    '2+_1_Q1' => [
      'min' => 1130,
      'max' => 1130,
    ],
    '2+_2_Q1' => [
      'min' => 1156,
      'max' => 1156,
    ],
    '2+_3+_Q1' => [
      'min' => 1177,
      'max' => 1177,
    ],
    '2+_0_Q2' => [
      'min' => 1093,
      'max' => 1093,
    ],
    '2+_1_Q2' => [
      'min' => 1130,
      'max' => 1130,
    ],
    '2+_2_Q2' => [
      'min' => 1156,
      'max' => 1156,
    ],
    '2+_3+_Q2' => [
      'min' => 1177,
      'max' => 1177,
    ],
    '2+_0_Q3' => [
      'min' => 1093,
      'max' => 1184,
    ],
    '2+_1_Q3' => [
      'min' => 1130,
      'max' => 1161,
    ],
    '2+_2_Q3' => [
      'min' => 1156,
      'max' => 1195,
    ],
    '2+_3+_Q3' => [
      'min' => 1177,
      'max' => 1218,
    ],
    '2+_0_Q4' => [
      'min' => 1184,
      'max' => 1400,
    ],
    '2+_1_Q4' => [
      'min' => 1161,
      'max' => 1404,
    ],
    '2+_2_Q4' => [
      'min' => 1195,
      'max' => 1463,
    ],
    '2+_3+_Q4' => [
      'min' => 1218,
      'max' => 1503,
    ],
    '2+_0_Q5' => [
      'min' => 1400,
      'max' => 1665,
    ],
    '2+_1_Q5' => [
      'min' => 1404,
      'max' => 1648,
    ],
    '2+_2_Q5' => [
      'min' => 1463,
      'max' => 1716,
    ],
    '2+_3+_Q5' => [
      'min' => 1503,
      'max' => 1764,
    ],
    '2+_0_Q6' => [
      'min' => 1665,
      'max' => 1913,
    ],
    '2+_1_Q6' => [
      'min' => 1648,
      'max' => 1970,
    ],
    '2+_2_Q6' => [
      'min' => 1716,
      'max' => 2051,
    ],
    '2+_3+_Q6' => [
      'min' => 1764,
      'max' => 2109,
    ],
    '2+_0_Q7' => [
      'min' => 1913,
      'max' => 2551,
    ],
    '2+_1_Q7' => [
      'min' => 1970,
      'max' => 2627,
    ],
    '2+_2_Q7' => [
      'min' => 2051,
      'max' => 2735,
    ],
    '2+_3+_Q7' => [
      'min' => 2109,
      'max' => 2812,
    ],
    '2+_0_Q8' => [
      'min' => 2551,
      'max' => 13479,
    ],
    '2+_1_Q8' => [
      'min' => 2627,
      'max' => 13880,
    ],
    '2+_2_Q8' => [
      'min' => 2735,
      'max' => 14451,
    ],
    '2+_3+_Q8' => [
      'min' => 2812,
      'max' => 14853,
    ],
    '2+_0_Q9' => [
      'min' => 13479,
      'max' => 13479,
    ],
    '2+_1_Q9' => [
      'min' => 13880,
      'max' => 13880,
    ],
    '2+_2_Q9' => [
      'min' => 14451,
      'max' => 14451,
    ],
    '2+_3+_Q9' => [
      'min' => 14853,
      'max' => 14853,
    ],
  ];

  private $scalingFactor = 1.00;

  /**
   * Mortgage.
   */
  private $mortgage = NULL;

  /**
   * Income.
   */
  private $income = NULL;

  /**
   * Income gross.
   */
  private $gross = NULL;

  /**
   * Expenditure.
   */
  private $expenditure = [
  // Â£.
    'gross' => 0,
  // Â£.
    'net' => 0,
    'applicants' => 0,
    'dependants' => 0,
    'quintile' => 0,
    'concat' => '',
  // Â£.
    'min' => 0,
  // Â£.
    'max' => 0,
  // Â£.
    'lowerLimit' => 0,
  // Â£.
    'upperLimit' => 0,
  // Â£.
    'quintileDifference' => 0,
  // Â£.
    'incomeLowerQuintileDifference' => 0,
  // %.
    'proportion' => 0,
  // Â£.
    'difference' => 0,
  // Â£.
    'raw' => 0,
  // Â£.
    'final' => 0,
  ];

  /**
   * Constructor.
   */
  public function __construct(array $mortgage, array $income, array $gross) {
    $this->mortgage = $mortgage;
    $this->income = $income;
    $this->gross = $gross;
  }

  /**
   * Calculate low/medium expenditure.
   */
  public function getExpenditure() {
    // Calculate total joint gross income.
    $this->expenditure['gross'] = $this->totalJointGrossIncome([
      $this->income['basic']['a1']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['guaranteed']['a1']['nonTaxable'],
      $this->income['regular']['a1']['taxable'],
      $this->income['regular']['a1']['nonTaxable'],
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['guaranteed']['a2']['nonTaxable'],
      $this->income['regular']['a2']['taxable'],
      $this->income['regular']['a2']['nonTaxable'],
    ]);
    // Calculate total joint net income.
    $this->expenditure['net'] = $this->totalJointNetIncome([
      $this->gross['a1']['monthly']['total'],
      $this->gross['a2']['monthly']['total'],
    ]);
    // Calculate number of applicants.
    $this->expenditure['applicants'] = $this->numberOfApplicants($this->mortgage['applicants']);
    // Calculate number of dependants.
    $this->expenditure['dependants'] = $this->numberOfDependants($this->mortgage['dependants']);
    // Calculate income quintile.
    $this->expenditure['quintile'] = $this->incomeQuintile();
    // Concat.
    $this->expenditure['concat'] = $this->concat();
    // Calculate min/max expenditure.
    $this->expenditure['min'] = $this->minMaxExpenditure($this->expenditure['applicants'], 'min');
    $this->expenditure['max'] = $this->minMaxExpenditure($this->expenditure['applicants'], 'max');
    // Calculate quintile lower/upper limit.
    $this->expenditure['lowerLimit'] = $this->quintileLimit($this->expenditure['dependants'], 'lower');
    $this->expenditure['upperLimit'] = $this->quintileLimit($this->expenditure['dependants'], 'upper');
    // Calculate quintile differences.
    $this->expenditure['quintileDifference'] = $this->quintileDifferences();
    // Calculate income to lower quintile difference.
    $this->expenditure['incomeLowerQuintileDifference'] = $this->incomeLowerQuintileDifference();
    // Calculate proportion.
    $this->expenditure['proportion'] = $this->proportion();
    // Calculate expenditure difference.
    $this->expenditure['difference'] = $this->difference();
    // Calculate raw expenditure.
    $this->expenditure['raw'] = $this->raw();
    // Calculate final expenditure.
    $this->expenditure['final'] = $this->finalExpenditure();
    return $this->expenditure;
  }

  /**
   * Calculate total joint gross income.
   */
  public function totalJointGrossIncome(array $arr) {
    return array_sum($arr);
  }

  /**
   * Calculate total joint net income.
   */
  public function totalJointNetIncome(array $arr) {
    return array_sum($arr);
  }

  /**
   * Calculate number of applicants.
   */
  public function numberOfApplicants($a) {
    if ($a > 1) {
      return '2+';
    }
    else {
      return $a;
    }
  }

  /**
   * Calculate number of dependants.
   */
  public function numberOfDependants($a) {
    if ($a > 2) {
      return '3+';
    }
    else {
      return $a;
    }
  }

  /**
   * Calculate income quintile.
   */
  public function incomeQuintile() {
    $q = '';
    if ($this->expenditure['dependants'] == 0) {
      if ($this->expenditure['net'] > $this->quintiles['Q8']['without']['upper']) {
        $q = 'Q9';
      }
      else {
        if ($this->expenditure['net'] > $this->quintiles['Q7']['without']['upper']) {
          $q = 'Q8';
        }
        else {
          if ($this->expenditure['net'] > $this->quintiles['Q6']['without']['upper']) {
            $q = 'Q7';
          }
          else {
            if ($this->expenditure['net'] > $this->quintiles['Q5']['without']['upper']) {
              $q = 'Q6';
            }
            else {
              if ($this->expenditure['net'] > $this->quintiles['Q4']['without']['upper']) {
                $q = 'Q5';
              }
              else {
                if ($this->expenditure['net'] > $this->quintiles['Q3']['without']['upper']) {
                  $q = 'Q4';
                }
                else {
                  if ($this->expenditure['net'] > $this->quintiles['Q2']['without']['upper']) {
                    $q = 'Q3';
                  }
                  else {
                    if ($this->expenditure['net'] > $this->quintiles['Q1']['without']['upper']) {
                      $q = 'Q2';
                    }
                    else {
                      $q = 'Q1';
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    else {
      if ($this->expenditure['net'] > $this->quintiles['Q8']['with']['upper']) {
        $q = 'Q9';
      }
      else {
        if ($this->expenditure['net'] > $this->quintiles['Q7']['with']['upper']) {
          $q = 'Q8';
        }
        else {
          if ($this->expenditure['net'] > $this->quintiles['Q6']['with']['upper']) {
            $q = 'Q7';
          }
          else {
            if ($this->expenditure['net'] > $this->quintiles['Q5']['with']['upper']) {
              $q = 'Q6';
            }
            else {
              if ($this->expenditure['net'] > $this->quintiles['Q4']['with']['upper']) {
                $q = 'Q5';
              }
              else {
                if ($this->expenditure['net'] > $this->quintiles['Q3']['with']['upper']) {
                  $q = 'Q4';
                }
                else {
                  if ($this->expenditure['net'] > $this->quintiles['Q2']['with']['upper']) {
                    $q = 'Q3';
                  }
                  else {
                    if ($this->expenditure['net'] > $this->quintiles['Q1']['with']['upper']) {
                      $q = 'Q2';
                    }
                    else {
                      $q = 'Q1';
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return $q;
  }

  /**
   * Concat.
   */
  public function concat() {
    return $this->expenditure['applicants'] . '_' . $this->expenditure['dependants'] . '_' . $this->expenditure['quintile'];
  }

  /**
   * Calculate min/max expenditure.
   */
  public function minMaxExpenditure($a, $b) {
    if ($a == 0) {
      return 0;
    }
    else {
      return $this->rates[$this->expenditure['concat']][$b];
    }
  }

  /**
   * Calculate quintile lower/upper limit.
   */
  public function quintileLimit($a, $b) {
    if ($a == 0) {
      return $this->quintiles[$this->expenditure['quintile']]['without'][$b];
    }
    else {
      return $this->quintiles[$this->expenditure['quintile']]['with'][$b];
    }
  }

  /**
   * Calculate quintile differences.
   */
  public function quintileDifferences() {
    return $this->expenditure['upperLimit'] - $this->expenditure['lowerLimit'];
  }

  /**
   * Calculate income to lower quintile difference.
   */
  public function incomeLowerQuintileDifference() {
    return $this->expenditure['net'] - $this->expenditure['lowerLimit'];
  }

  /**
   * Calculate proportion.
   */
  public function proportion() {
    return $this->expenditure['incomeLowerQuintileDifference'] / ($this->expenditure['quintileDifference'] + 1);
  }

  /**
   * Calculate expenditure difference.
   */
  public function difference() {
    return $this->expenditure['max'] - $this->expenditure['min'];
  }

  /**
   * Calculate raw expenditure.
   */
  public function raw() {
    return SantanderBorrowAdditionalLoanResult::round_up(array_sum([
      array_product([
        $this->expenditure['proportion'],
        $this->expenditure['difference'],
      ]),
      $this->expenditure['min'],
    ]));
  }

  /**
   * Calculate final expenditure.
   */
  public function finalExpenditure() {
    return array_product([
      $this->expenditure['raw'],
      $this->scalingFactor,
    ]);
  }

}
/**
 *
 */
class SantanderBorrowAdditionalLoanResultCredit {
  /**
   * Commitments.
   */
  private $commitments = NULL;

  /**
   * Credit.
   */
  /**
   * Â£.
   */
  private $credit = 0;

  /**
   * Constructor.
   */
  public function __construct(array $commitments) {
    $this->commitments = $commitments;
  }

  /**
   * Calculate credit commitments.
   */
  public function getCredit() {
    $this->credit = $this->monthlyCreditCommitments();
    return $this->credit;
  }

  /**
   * Calculate credit commitments.
   */
  public function monthlyCreditCommitments() {
    return SantanderBorrowAdditionalLoanResult::round_up(
      array_sum([
        $this->commitments['loans'],
        array_product([
          0.03,
          $this->commitments['creditCard'],
        ]),
      ])
    );
  }

}
/**
 *
 */
class SantanderBorrowAdditionalLoanResultLoan {
  /**
   * LTV.
   */
  private $ltv = [
    'low' => [
  // %
      'rate' => 0.07,
      'single' => 5.5,
      'joint' => 5.5,
    ],
    'high' => [
    // %
      'rate' => 0.07,
      'single' => 5,
      'joint' => 5,
    ],
    'higher' => [
    // %
      'rate' => 0.07,
      'single' => 4.45,
      'joint' => 4.45,
    ],
  ];

  /**
   * Minimum Loan Amount Policy.
   */
  private $minAmountPolicy = 5000;

  /**
   * Mortgage.
   */
  private $mortgage = NULL;

  /**
   * Income.
   */
  private $income = NULL;

  /**
   * Secondary Weighting.
   */
  /**
   * 65%.
   */
  private $secondaryWeighting = 0.65;

  /**
   * Commitments.
   */
  private $commitments = NULL;

  /**
   * Questions.
   */
  private $questions = NULL;

  /**
   * Gross.
   */
  private $gross = NULL;

  /**
   * Expenditure.
   */
  private $expenditure = NULL;

  /**
   * Credit.
   */
  private $credit = NULL;

  /**
   * Debt consolidation.
   */
  protected $debtConsolidation = NULL;

  /**
   * LTV restriction.
   */
  protected $LTVRestriction = NULL;

  /**
   * Loan result.
   */
  protected $loan = [
  // Â£.
    'allowable' => 0,
    'propertyValue' => 0,
    'additionalLoanTerm' => 0,
  // Â£.
    'disposable' => 0,
    'p1Amount' => 0,
    'p2Amount' => 0,
    'p3Amount' => 0,
    'p4Amount' => 0,
    'p5Amount' => 0,
    'p1Term' => 0,
    'p2Term' => 0,
    'p3Term' => 0,
    'p4Term' => 0,
    'p5Term' => 0,
    'p1Repayment' => 0,
    'p2Repayment' => 0,
    'p3Repayment' => 0,
    'p4Repayment' => 0,
    'p5Repayment' => 0,
    'totalAmount' => 0,
    'initialMaxLoanAmount' => 0,
    'ltv' => [
      'low' => [
  // Â£.
        'affordable' => 0,
  // Â£.
        'multiple' => 0,
  // Â£.
        'ltv' => 0,
  // Â£.
        'restricted' => 0,
  // %.
        'percentage' => 0,
      ],
      'high' => [
      // Â£.
        'affordable' => 0,
      // Â£.
        'multiple' => 0,
      // Â£.
        'ltv' => 0,
      // Â£.
        'restricted' => 0,
      // %.
        'percentage' => 0,
      ],
    ],
    // Â£.
    'loan' => 0,
  ];

  /**
   * Constructor.
   */
  public function __construct($mortgage, $income, $commitments, $i, $e, $c, $questions) {
    $this->gross = $i;

    $this->loan['propertyValue'] = $questions['propertyValue'];
    $this->debtConsolidation = $questions['debtConsolidation'];

    $this->loan['additionalLoanTerm'] = $questions['newTerm'];

    $this->loan['p1Amount'] = $questions['loanp1amount'];
    $this->loan['p2Amount'] = $questions['loanp2amount'];
    $this->loan['p3Amount'] = $questions['loanp3amount'];
    $this->loan['p4Amount'] = $questions['loanp4amount'];
    $this->loan['p5Amount'] = $questions['loanp5amount'];

    $this->loan['p1Term'] = $questions['loanp1term'];
    $this->loan['p2Term'] = $questions['loanp2term'];
    $this->loan['p3Term'] = $questions['loanp3term'];
    $this->loan['p4Term'] = $questions['loanp4term'];
    $this->loan['p5Term'] = $questions['loanp5term'];

    $this->loan['p1Repayment'] = $questions['loanp1Repayment'];
    $this->loan['p2Repayment'] = $questions['loanp2Repayment'];
    $this->loan['p3Repayment'] = $questions['loanp3Repayment'];
    $this->loan['p4Repayment'] = $questions['loanp4Repayment'];
    $this->loan['p5Repayment'] = $questions['loanp5Repayment'];

    $this->mortgage = $mortgage;
    $this->income = $income;
    $this->commitments = $commitments;
    $this->expenditure = $e;
    $this->credit = $c;

  }

  /**
   * Calculate max loan.
   */
  public function getLoan() {
    $this->loan['totalAmount'] = $this->totalpartsLoansLoans();
    $this->loan['allowable'] = $this->allowableIncome();
    $this->loan['disposable'] = $this->disposableIncome();

    $this->LTVRestriction = $this->LTVRestriction();

    $this->loan['ltv']['low']['affordable'] = $this->affordable('low');
    $this->loan['ltv']['low']['multiple'] = $this->maxAmountMultiple('low');
    $this->loan['ltv']['low']['ltv'] = $this->lowMaxAmountLTV();
    $this->loan['ltv']['low']['restricted'] = $this->restrictedAmount('low');
    $this->loan['ltv']['low']['percentage'] = $this->maxAmountLTVPercentage('low');

    $this->loan['ltv']['high']['affordable'] = $this->affordable('high');
    $this->loan['ltv']['high']['multiple'] = $this->maxAmountMultiple('high');
    $this->loan['ltv']['high']['ltv'] = $this->highMaxAmountLTV();
    $this->loan['ltv']['high']['restricted'] = $this->restrictedAmount('high');
    $this->loan['ltv']['high']['percentage'] = $this->maxAmountLTVPercentage('high');

    $this->loan['ltv']['higher']['affordable'] = $this->affordable('higher');
    $this->loan['ltv']['higher']['multiple'] = $this->maxAmountMultiple('higher');
    $this->loan['ltv']['higher']['ltv'] = $this->higherMaxAmountLTV();
    $this->loan['ltv']['higher']['restricted'] = $this->restrictedAmount('higher');
    $this->loan['ltv']['higher']['percentage'] = $this->maxAmountLTVPercentage('higher');

    $this->loan['initialMaxLoanAmount'] = $this->initialMaxLoanAmount();

    $this->loan['loan'] = $this->maxLoanAmount();

    return $this->loan;
  }

  /**
   * Calculate allowable income.
   */
  public function allowableIncome() {
    return array_sum([
      $this->income['basic']['a1']['taxable'],
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['guaranteed']['a1']['nonTaxable'],
      $this->income['guaranteed']['a2']['nonTaxable'],
    ]) + $this->secondaryWeighting * 0.5 * array_sum([
      $this->income['regular']['a1']['taxable'],
      $this->income['regular']['a1']['nonTaxable'],
      $this->income['regular']['a2']['taxable'],
      $this->income['regular']['a2']['nonTaxable'],
    ]);
  }

  /**
   * Calculate total parts amount.
   */
  public function totalpartsLoansLoans() {
    return $this->loan['p1Amount'] + $this->loan['p2Amount'] + $this->loan['p3Amount'] + $this->loan['p4Amount'] + $this->loan['p5Amount'];
  }

  /**
   * Calculate disposable income.
   */
  public function disposableIncome() {
    return $this->gross['totalMonthly'] - $this->expenditure['final'] - $this->credit - $this->commitments['outgoings'];
  }

  /**
   * Calculate Existing loan part 1.
   */
  public function loan1($a) {
    if ($this->loan['p1Amount'] == 0 || $this->loan['p1Term'] == 0 || $this->loan['p1Repayment'] === 0) {
      return 0;
    }
    if ($this->loan['p1Repayment'] == "IO") {
      return ($this->loan['p1Amount'] * $this->ltv[$a]['rate'] / 12) / (1 - pow((1 + ($this->ltv[$a]['rate'] / 12)), (-1 * (max($this->loan['p1Term'], 300)))));
    }

    return $this->loan['p1Amount'] * ($this->ltv[$a]['rate'] / 12) / (1 - (pow(1 + ($this->ltv[$a]['rate'] / 12), (-1 * ($this->loan['p1Term'])))));
  }

  /**
   * Calculate Existing loan part 2.
   */
  public function loan2($a) {
    if ($this->loan['p2Amount'] == 0 || $this->loan['p2Term'] == 0 || $this->loan['p2Repayment'] === 0) {
      return 0;
    }

    if ($this->loan['p2Repayment'] == "IO") {
      return ($this->loan['p2Amount'] * $this->ltv[$a]['rate'] / 12) / (1 - pow((1 + ($this->ltv[$a]['rate'] / 12)), (-1 * (max($this->loan['p2Term'], 300)))));
    }

    return $this->loan['p2Amount'] * ($this->ltv[$a]['rate'] / 12) / (1 - (pow(1 + ($this->ltv[$a]['rate'] / 12), (-1 * ($this->loan['p2Term'])))));
  }

  /**
   * Calculate Existing loan part 3.
   */
  public function loan3($a) {
    if ($this->loan['p3Amount'] == 0 || $this->loan['p3Term'] == 0 || $this->loan['p3Repayment'] === 0) {
      return 0;
    }

    if ($this->loan['p3Repayment'] == "IO") {
      return ($this->loan['p3Amount'] * $this->ltv[$a]['rate'] / 12) / (1 - pow((1 + ($this->ltv[$a]['rate'] / 12)), (-1 * (max($this->loan['p3Term'], 300)))));
    }

    return $this->loan['p3Amount'] * ($this->ltv[$a]['rate'] / 12) / (1 - (pow(1 + ($this->ltv[$a]['rate'] / 12), (-1 * ($this->loan['p3Term'])))));
  }

  /**
   * Calculate low Existing loan part 4.
   */
  public function loan4($a) {
    if ($this->loan['p4Amount'] == 0 || $this->loan['p4Term'] == 0 || $this->loan['p4Repayment'] === 0) {
      return 0;
    }

    if ($this->loan['p4Repayment'] == "IO") {
      return ($this->loan['p4Amount'] * $this->ltv[$a]['rate'] / 12) / (1 - pow((1 + ($this->ltv[$a]['rate'] / 12)), (-1 * (max($this->loan['p4Term'], 300)))));
    }

    return $this->loan['p4Amount'] * ($this->ltv[$a]['rate'] / 12) / (1 - (pow(1 + ($this->ltv[$a]['rate'] / 12), (-1 * ($this->loan['p4Term'])))));
  }

  /**
   * Calculate low Existing loan part 5.
   */
  public function loan5($a) {
    if ($this->loan['p5Amount'] == 0 || $this->loan['p5Term'] == 0 || $this->loan['p5Repayment'] === 0) {
      return 0;
    }

    if ($this->loan['p5Repayment'] == "IO") {
      return ($this->loan['p5Amount'] * $this->ltv[$a]['rate'] / 12) / (1 - pow((1 + ($this->ltv[$a]['rate'] / 12)), (-1 * (max($this->loan['p5Term'], 300)))));
    }

    return $this->loan['p5Amount'] * ($this->ltv[$a]['rate'] / 12) / (1 - (pow(1 + ($this->ltv[$a]['rate'] / 12), (-1 * ($this->loan['p5Term'])))));
  }

  /**
   * Calculate low/high Existing loan total.
   */
  public function totalLoans($a) {
    return $this->loan1($a) + $this->loan2($a) + $this->loan3($a) + $this->loan4($a) + $this->loan5($a);
  }

  /**
   * Calculate high LTV max amount affordable.
   */
  public function affordable($a) {
    $totalStressedPayments = $this->totalLoans($a);
    return max(($this->loan['disposable'] - $totalStressedPayments) * (1 - pow(1 + ($this->ltv[$a]['rate'] / 12), -1 * $this->loan['additionalLoanTerm'])) / ($this->ltv[$a]['rate'] / 12), 0);
  }

  /**
   * Calculate max amount multiple.
   */
  public function maxAmountMultiple($a) {
    if ($this->mortgage['applicants'] == 1) {
      $maxAmount = $this->ltv[$a]['single'] * $this->loan['allowable'] - $this->loan['totalAmount'];
    }
    else {
      $maxAmount = $this->ltv[$a]['joint'] * $this->loan['allowable'] - $this->loan['totalAmount'];
    }
    return max($maxAmount, 0);
  }

  /**
   * Calculate low max amount LTV.
   */
  public function lowMaxAmountLTV() {
    return max((0.75 * $this->loan['propertyValue']) - $this->loan['totalAmount'], 0);
  }

  /**
   * Calculate high max amount LTV.
   */
  public function highMaxAmountLTV() {
    return max((0.85 * $this->loan['propertyValue']) - $this->loan['totalAmount'], 0);
  }

  /**
   * Calculate higher max amount LTV.
   */
  public function higherMaxAmountLTV() {
    return max((0.85 * $this->loan['propertyValue']) - $this->loan['totalAmount'], 0);
  }

  /**
   * Calculate  restricted amount.
   */
  public function restrictedAmount($a) {
    return min($this->loan['ltv'][$a]['affordable'], $this->loan['ltv'][$a]['multiple'], $this->loan['ltv'][$a]['ltv']);
  }

  /**
   * Calculate max amount LTV (percentage)
   */
  public function maxAmountLTVPercentage($a) {
    if ($this->loan['propertyValue'] == 0) {
      return '';
    }
    return ($this->loan['ltv'][$a]['restricted'] + $this->loan['totalAmount']) / $this->loan['propertyValue'];
  }

  /**
   * Calculate Initial Maximum Loan Amount.
   */
  public function initialMaxLoanAmount() {
    if ($this->loan['ltv']['high']['percentage'] > 0.75 && $this->gross['total'] >= 45000) {
      return floor($this->loan['ltv']['high']['restricted']);
    }
    if ($this->loan['ltv']['higher']['percentage'] >= 0.75) {
      return floor($this->loan['ltv']['higher']['restricted']);
    }
    if ($this->gross['total'] >= 100000) {
      return floor($this->loan['ltv']['low']['restricted']);
    }
    if ($this->gross['total'] >= 45000 && $this->loan['ltv']['high']['percentage'] > $this->loan['ltv']['higher']['percentage']) {
      return floor($this->loan['ltv']['high']['restricted']);
    }
    return floor($this->loan['ltv']['higher']['restricted']);
  }

  /**
   *
   */
  public function LTVRestriction() {
    return floor(max(0.85 * $this->loan['propertyValue'] - $this->loan['totalAmount'], 0));
  }

  /**
   *
   */
  public function debtConsolidationRestriction() {
    return min(35000, max(0.35 / 0.65 * $this->totalpartsLoansLoans(), 0.5 * $this->loan['propertyValue'] - $this->totalpartsLoansLoans()));
  }

  /**
   * Maximum Loan Amount within Policy.
   */
  public function maxAmountPolicy() {
    if ($this->debtConsolidation == 'yes') {
      return floor(min($this->loan['initialMaxLoanAmount'], $this->LTVRestriction, $this->special_function_c39(), $this->special_function_c40(), $this->debtConsolidationRestriction()));
    }
    return floor(min($this->loan['initialMaxLoanAmount'], $this->LTVRestriction, $this->special_function_c39(), $this->special_function_c40()));
  }

  // For all future developers, don't try to understand these two functions.

  /**
   * They represent 2 conditions added to the client's spreadsheet.
   */
  public function special_function_c39() {
    $sum = 0;
    for ($i = 1; $i <= 5; $i++) {
      if ($this->loan['p' . $i . 'Amount'] == 0 || $this->loan['p' . $i . 'Term'] == 0 || $this->loan['p' . $i . 'Repayment'] === 0) {
        continue;
      }

      if ($this->loan['p' . $i . 'Repayment'] == 'IO') {
        $sum += $this->loan['p' . $i . 'Amount'];
      }
    }

    if ($sum) {
      return $this->lowMaxAmountLTV();
    }
    return $this->highMaxAmountLTV();

  }

  /**
   *
   */
  public function special_function_c40() {
    $sum = 0;
    for ($i = 1; $i <= 5; $i++) {
      if ($this->loan['p' . $i . 'Amount'] == 0 || $this->loan['p' . $i . 'Term'] == 0 || $this->loan['p' . $i . 'Repayment'] === 0) {
        continue;
      }

      if ($this->loan['p' . $i . 'Repayment'] == 'IO') {
        $sum += $this->loan['p' . $i . 'Amount'];
      }
    }

    if (($sum / $this->loan['propertyValue']) > 0.5) {
      return $this->lowMaxAmountLTV();
    }
    return $this->loan['initialMaxLoanAmount'];

  }

  /**
   * Calculate maximum loan amount.
   */
  public function maxLoanAmount() {
    $maxAmountPolicy = $this->maxAmountPolicy();
    if ($maxAmountPolicy < $this->minAmountPolicy) {
      return 0;
    }
    return $maxAmountPolicy;
  }

}

/**
 *
 */
function santander_borrow_get_result_additional_loan() {
  $data = $_POST;
  $result = new SantanderBorrowAdditionalLoanResult();
  $validated = $result->validate($data);
  $result->setParams($data);

  $loan = $result->calculate();

  print drupal_json_encode(['loan' => $loan, 'validated' => $validated]);
  drupal_exit();
}
