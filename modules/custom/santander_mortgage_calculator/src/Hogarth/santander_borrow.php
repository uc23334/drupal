<?php

namespace Drupal\Tests\santander_mortgage_calculator\Unit;

/**
 * For the purpose of testing
 * Equivalent Front-End fields to the testing spreadsheet.
 *
 * Question 1 - main annual income
 * FRONTEND: - all dropdown options
 * CODE: basic taxable
 * TESTSHEET: Annual Basic Income.
 *
 * Question 2 - other employed annual income
 * FRONTEND: employed allowances
 * CODE: guaranteed taxable
 * TESTSHEET: primary taxable
 *
 * FRONTEND: bonus, commision, overtime
 * CODE: regular taxable
 * TESTSHEET: secondary taxable
 *
 * Question 3 - other annual income
 * FRONTEND: working tax and child tax credit
 * CODE: guaranteed nonTaxable
 * TESTSHEET: primary nonTaxable
 *
 * FRONTEND: child benefit
 * CODE: guaranteed nonTaxable
 * TESTSHEET: primary nonTaxable
 *
 * FRONTEND: child maintenance or csa payments
 * CODE: regular nonTaxable
 * TESTSHEET: secondary nonTaxable
 */
class SantanderBorrowResult {

  /**
   *
   */
  public static function round_up($n, $d = 0) {
    return round($n, $d);
  }

  /**
   * @var array
   */
  private $mortgage = [
    'applicants' => 1,
    'buyers' => '',
    'dependants' => 0,
    'term' => 25,
    'deposit' => 0,
  ];

  /**
   * @var array
   */
  private $income = [
    'basic' => [
      'a1' => ['taxable' => 0],
      'a2' => ['taxable' => 0],
    ],
    'guaranteed' => [
      'a1' => ['taxable' => 0, 'nonTaxable' => 0],
      'a2' => ['taxable' => 0, 'nonTaxable' => 0],
    ],
    'regular' => [
      'a1' => ['taxable' => 0, 'nonTaxable' => 0],
      'a2' => ['taxable' => 0, 'nonTaxable' => 0],
    ],
  ];

  /**
   * @var array
   */
  private $commitments = [
    'creditCard' => 0,
    'loans' => 0,
    'outgoings' => 0,
  ];

  /**
   * Set Params.
   *
   * @param $data
   */
  public function setParams($data) {
    $this->mortgage['applicants'] = $data['applicants'];
    $this->mortgage['buyers'] = ($data['buyers'] == 'yes' || $data['buyers'] == '1') ? 'yes' : 'no';
    $this->mortgage['dependants'] = ($data['dependants'] == '+3') ? 3 : $data['dependants'];
    $this->mortgage['term'] = $data['term'];
    $this->mortgage['deposit'] = $data['deposit'];
    $this->mortgage['buyer_type'] = $data['buyer_type'];
    $this->mortgage['extra'] = $data['extra'];
    $this->mortgage['current_mortgage_balance'] = $data['current_mortgage_balance'];
    $this->income['basic']['a1']['taxable'] = $data['basicA1Taxable'];
    $this->income['basic']['a2']['taxable'] = $data['basicA2Taxable'];
    $this->income['guaranteed']['a1']['taxable'] = $data['guaranteedA1Taxable'];
    $this->income['guaranteed']['a1']['nonTaxable'] = $data['guaranteedA1NonTaxable'];
    $this->income['guaranteed']['a2']['taxable'] = $data['guaranteedA2Taxable'];
    $this->income['guaranteed']['a2']['nonTaxable'] = $data['guaranteedA2NonTaxable'];
    $this->income['regular']['a1']['taxable'] = $data['regularA1Taxable'];
    $this->income['regular']['a1']['nonTaxable'] = $data['regularA1NonTaxable'];
    $this->income['regular']['a2']['taxable'] = $data['regularA2Taxable'];
    $this->income['regular']['a2']['nonTaxable'] = $data['regularA2NonTaxable'];
    $this->commitments['creditCard'] = $data['creditCard'];
    $this->commitments['loans'] = $data['loans'];
    $this->commitments['outgoings'] = $data['outgoings'];
  }

  /**
   *
   */
  public function validate(&$data) {
    $error = FALSE;

    $fields = [
      'applicants' => ['required' => TRUE],
      'buyers' => ['required' => TRUE],
      'dependants' => ['required' => TRUE],
      'dependants' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0, 'max' => 4],
      'term' => ['required' => TRUE, 'type' => 'numeric', 'min' => 5, 'max' => 35],
      'deposit' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'basicA1Taxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'basicA2Taxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'guaranteedA1Taxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'guaranteedA1NonTaxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'guaranteedA2Taxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'guaranteedA2NonTaxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'regularA1Taxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'regularA1NonTaxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'regularA2Taxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'regularA2NonTaxable' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'creditCard' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'loans' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
      'outgoings' => ['required' => TRUE, 'type' => 'numeric', 'min' => 0],
    ];

    foreach ($fields as $name => $fieldInfo) {
      if (!isset($data[$name]) && $fieldInfo['required']) {
        return FALSE;
      }

      if (isset($fieldInfo['type']) && $fieldInfo['type'] == 'numeric') {
        $data[$name] = floatval($data[$name]);
        if (isset($fieldInfo['min']) && $data[$name] < $fieldInfo['min']) {
          return FALSE;
        }
        if (isset($fieldInfo['max']) && $data[$name] > $fieldInfo['max']) {
          return FALSE;
        }
      }
    }

    return TRUE;
    /*
    'applicants' => 'required|in:single,joint',
    'buyers' => 'required|in:yes,no',
    'dependants' => 'required|in:0,1,2,+3',
    'term' => 'required|numeric|between:5,35',
    'deposit' => 'required|numeric|min:0',
    'basicA1Taxable' => 'required|numeric|min:0',
    'basicA2Taxable' => 'required|numeric|min:0',
    'guaranteedA1Taxable' => 'required|numeric|min:0',
    'guaranteedA1NonTaxable' => 'required|numeric|min:0',
    'guaranteedA2Taxable' => 'required|numeric|min:0',
    'guaranteedA2NonTaxable' => 'required|numeric|min:0',
    'regularA1Taxable' => 'required|numeric|min:0',
    'regularA1NonTaxable' => 'required|numeric|min:0',
    'regularA2Taxable' => 'required|numeric|min:0',
    'regularA2NonTaxable' => 'required|numeric|min:0',
    'creditCard' => 'required|numeric|min:0',
    'loans' => 'required|numeric|min:0',
    'outgoings' => 'required|numeric|min:0'
     */
  }

  /**
   *
   */
  public function calculate() {
    $i = $this->calculateIncome();
    $e = $this->calculateExpenditure($i);
    $c = $this->calculateCredit();
    $l = $this->calculateLoan($i, $e, $c);

    /*
    $fileHandle = fopen("test.log", "a");
    fwrite($fileHandle,   "MORTGAGE \n");
    fwrite($fileHandle, print_r($this->mortgage, TRUE) . "\n");
    fwrite($fileHandle,   "INCOME \n");
    fwrite($fileHandle, print_r($this->income, TRUE) . "\n");
    fclose($fileHandle);
     */

    return $l['loan'];
  }

  /**
   * Calculate Total NET monthly income.
   */
  private function calculateIncome() {
    $o = new SantanderBorrowResultIncome($this->income);
    return $o->getIncome();
  }

  /**
   * Calculate Expenditure (low/medium)
   */
  private function calculateExpenditure($i) {
    $o = new SantanderBorrowResultExpenditure($this->mortgage, $this->income, $i);
    return $o->getExpenditure();
  }

  /**
   * Calculate credit commitments.
   */
  private function calculateCredit() {
    $o = new SantanderBorrowResultCredit($this->commitments);
    return $o->getCredit();
  }

  /**
   * Calculate max loan.
   */
  private function calculateLoan($i, $e, $c) {
    $o = new SantanderBorrowResultLoan($this->mortgage, $this->income, $this->commitments, $i, $e, $c);
    return $o->getLoan();
  }

}
/**
 *
 */
class SantanderBorrowResultIncome {
  /**
   * Rates.
   */
  private $taxFreeAllowance = 11850; /**
                                      * ďż˝.
                                      */

  private $rates = [
    'starting' => [
  // 10%
      'rate' => 0.1,
  // ďż˝.
      'low' => 0,
  // ďż˝.
      'high' => 0,
      'dividends' => 0,
    ],
    'basic' => [
    // 20%
      'rate' => 0.2,
    // ďż˝.
      'low' => 0,
    // ďż˝.
      'high' => 34500,
    // 10% for dividents DIP
      'dividends' => 0.1,
    ],
    'higher' => [
    // 40%
      'rate' => 0.4,
    // ďż˝.
      'low' => 34501,
    // ďż˝.
      'high' => 150000,
    // 32.5% for dividents DIP
      'dividends' => 0.325,
    ],
    'additional' => [
    // 45%
      'rate' => 0.45,
    // ďż˝.
      'low' => 150001,
    // 37.5% for dividents DIP
      'dividends' => 0.375,
    ],
  ];

  private $director_tax_credit = 0.1;

  private $primaryNiThreshold = [
  // 12%
    'rate' => 0.12,
  // ďż˝.
    'low' => 8424,
  // ďż˝ //DIP upper limit is 41860.
    'high' => 46349,
  ];

  private $upperEarningsNiLimit = [
  // 2%
    'rate' => 0.02,
  // ďż˝ //DIP low limit is 41861.
    'low' => 46350,
  ];

  /**
   * 95%.
   */
  private $scalingFactor = 0.95;
  /**
   * Income.
   */

  private $income = NULL;

  /**
   * Secondary Weighting.
   */
  /**
   * 65%.
   */
  private $secondaryWeighting = 0.65;

  /**
   * Gross.
   */
  private $gross = [
    'a1' => [
  // ďż˝.
      'taxable' => 0,
  // ďż˝.
      'taxFreeAllowance' => 0,
  // ďż˝.
      'tax10' => 0,
  // ďż˝.
      'tax20' => 0,
  // ďż˝.
      'tax40' => 0,
  // ďż˝.
      'tax45' => 0,
  // ďż˝.
      'ni2' => 0,
  // ďż˝.
      'ni12' => 0,
  // ďż˝.
      'div10' => 0,
  // ďż˝.
      'div325' => 0,
  // ďż˝.
      'div375' => 0,
      'annual' => [
  // ďż˝.
        'taxable' => 0,
  // ďż˝.
        'nonTaxable' => 0,
        'dividends' => 0,
      ],
      'monthly' => [
        'taxable' => [
      // ďż˝.
          'total' => 0,
      // %
          'basic' => 0,
      // %
          'guaranteed' => 0,
      // %
          'regular' => 0,
      // ďż˝.
          'reduced' => 0,
        ],
        'nonTaxable' => [
        // ďż˝.
          'total' => 0,
        // %
          'basic' => 0,
        // %
          'guaranteed' => 0,
        // %
          'regular' => 0,
        // ďż˝.
          'reduced' => 0,
        ],
        // ďż˝.
        'total' => 0,
      ],
      // ďż˝.
      'total' => 0,
    ],
    'a2' => [
    // ďż˝.
      'taxable' => 0,
    // ďż˝.
      'taxFreeAllowance' => 0,
    // ďż˝.
      'tax10' => 0,
    // ďż˝.
      'tax20' => 0,
    // ďż˝.
      'tax40' => 0,
    // ďż˝.
      'tax45' => 0,
    // ďż˝.
      'ni2' => 0,
    // ďż˝.
      'ni12' => 0,
    // ďż˝.
      'div10' => 0,
    // ďż˝.
      'div325' => 0,
    // ďż˝.
      'div375' => 0,
      'annual' => [
    // ďż˝.
        'taxable' => 0,
    // ďż˝.
        'nonTaxable' => 0,
        'dividends' => 0,
      ],
      'monthly' => [
        'taxable' => [
      // ďż˝.
          'total' => 0,
      // %
          'basic' => 0,
      // %
          'guaranteed' => 0,
      // %
          'regular' => 0,
      // ďż˝.
          'reduced' => 0,
        ],
        'nonTaxable' => [
        // ďż˝.
          'total' => 0,
        // %
          'basic' => 0,
        // %
          'guaranteed' => 0,
        // %
          'regular' => 0,
        // ďż˝.
          'reduced' => 0,
        ],
        // ďż˝.
        'total' => 0,
      ],
      // ďż˝.
      'total' => 0,
    ],
    // ďż˝.
    'total' => 0,
    // ďż˝.
    'totalMonthly' => 0,
  ];

  /**
   * Constructor.
   */
  public function __construct(array $arr) {
    $this->income = $arr;
  }

  /**
   * Calculate Total NET monthly income.
   */
  public function getIncome() {
    // Calculate gross income for applicant.
    $this->gross['a1']['total'] = $this->grossApplicant([
      $this->income['basic']['a1']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['guaranteed']['a1']['nonTaxable'],
      $this->income['regular']['a1']['taxable'],
      $this->income['regular']['a1']['nonTaxable'],
    ]);
    $this->gross['a2']['total'] = $this->grossApplicant([
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['guaranteed']['a2']['nonTaxable'],
      $this->income['regular']['a2']['taxable'],
      $this->income['regular']['a2']['nonTaxable'],
    ]);
    // Calculate total gross income.
    $this->gross['total'] = $this->totalGross();
    // Calculate total gross taxable income for applicant.
    $this->gross['a1']['taxable'] = $this->grossTaxableApplicant([
      $this->income['basic']['a1']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['regular']['a1']['taxable'],
    ], 'a1');
    $this->gross['a2']['taxable'] = $this->grossTaxableApplicant([
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['regular']['a2']['taxable'],
    ], 'a2');
    // Calculate final tax free allowance.
    $this->gross['a1']['taxFreeAllowance'] = $this->finalTaxFreeAllowance('a1');
    $this->gross['a2']['taxFreeAllowance'] = $this->finalTaxFreeAllowance('a2');
    // Calculate tax at 10%.
    $this->gross['a1']['tax10'] = $this->tax10('a1');
    $this->gross['a2']['tax10'] = $this->tax10('a2');
    // Calculate tax at 20%.
    $this->gross['a1']['tax20'] = $this->tax20('a1');
    $this->gross['a2']['tax20'] = $this->tax20('a2');
    // Calculate tax at 40%.
    $this->gross['a1']['tax40'] = $this->tax40('a1');
    $this->gross['a2']['tax40'] = $this->tax40('a2');
    // Calculate tax at 45%.
    $this->gross['a1']['tax45'] = $this->tax45('a1');
    $this->gross['a2']['tax45'] = $this->tax45('a2');
    // Calculate NI at 12%.
    $this->gross['a1']['ni12'] = $this->ni12('a1');
    $this->gross['a2']['ni12'] = $this->ni12('a2');
    // Calculate NI at 2%.
    $this->gross['a1']['ni2'] = $this->ni2('a1');
    $this->gross['a2']['ni2'] = $this->ni2('a2');
    // Calculate dividends if applicable (DIP)
    if (isset($this->income['dividends'])) {
      // 10% dividend rate
      $this->gross['a1']['div10'] = $this->div10('a1');
      $this->gross['a2']['div10'] = $this->div10('a2');
      // 32.5% dividend rate
      $this->gross['a1']['div325'] = $this->div325('a1');
      $this->gross['a2']['div325'] = $this->div325('a2');
      // 37.5% dividend rate
      $this->gross['a1']['div375'] = $this->div375('a1');
      $this->gross['a2']['div375'] = $this->div375('a2');

      // Calculate Net annual dividend.
      $this->gross['a1']['annual']['dividends'] = $this->annualDividend('a1');
      $this->gross['a2']['annual']['dividends'] = $this->annualDividend('a2');
    }
    // Calculate Net annual income (taxable)
    $this->gross['a1']['annual']['taxable'] = $this->annualTaxable('a1');
    $this->gross['a2']['annual']['taxable'] = $this->annualTaxable('a2');
    // Calculate Net annual income (non taxable)
    $this->gross['a1']['annual']['nonTaxable'] = $this->annualNonTaxable([
      $this->income['guaranteed']['a1']['nonTaxable'],
      $this->income['regular']['a1']['nonTaxable'],
    ]);
    $this->gross['a2']['annual']['nonTaxable'] = $this->annualNonTaxable([
      $this->income['guaranteed']['a2']['nonTaxable'],
      $this->income['regular']['a2']['nonTaxable'],
    ]);
    // Calculate Net monthly income (taxable)
    $this->gross['a1']['monthly']['taxable']['total'] = $this->monthlyTaxable('a1');
    $this->gross['a2']['monthly']['taxable']['total'] = $this->monthlyTaxable('a2');
    // Calculate taxable proportion (%)
    $this->gross['a1']['monthly']['taxable']['basic'] = $this->taxableProportion([
      $this->income['basic']['a1']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['regular']['a1']['taxable'],
    ], $this->income['basic']['a1']['taxable']);
    $this->gross['a1']['monthly']['taxable']['guaranteed'] = $this->taxableProportion([
      $this->income['basic']['a1']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['regular']['a1']['taxable'],
    ], $this->income['guaranteed']['a1']['taxable']);
    $this->gross['a1']['monthly']['taxable']['regular'] = $this->taxableProportion([
      $this->income['basic']['a1']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['regular']['a1']['taxable'],
    ], $this->income['regular']['a1']['taxable']);
    $this->gross['a2']['monthly']['taxable']['basic'] = $this->taxableProportion([
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['regular']['a2']['taxable'],
    ], $this->income['basic']['a2']['taxable']);
    $this->gross['a2']['monthly']['taxable']['guaranteed'] = $this->taxableProportion([
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['regular']['a2']['taxable'],
    ], $this->income['guaranteed']['a2']['taxable']);
    $this->gross['a2']['monthly']['taxable']['regular'] = $this->taxableProportion([
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['regular']['a2']['taxable'],
    ], $this->income['regular']['a2']['taxable']);
    // Calculate Net monthly income (taxable reduced)
    $this->gross['a1']['monthly']['taxable']['reduced'] = $this->monthlyTaxableReduced('a1');
    $this->gross['a2']['monthly']['taxable']['reduced'] = $this->monthlyTaxableReduced('a2');
    // Calculate Net monthly income (non taxable)
    $this->gross['a1']['monthly']['nonTaxable']['total'] = $this->monthlyNonTaxable('a1');
    $this->gross['a2']['monthly']['nonTaxable']['total'] = $this->monthlyNonTaxable('a2');
    // Calculate Gross non taxable proportion (%)
    $this->gross['a1']['monthly']['nonTaxable']['guaranteed'] = $this->nonTaxableProportion('a1', $this->income['guaranteed']['a1']['nonTaxable']);
    $this->gross['a1']['monthly']['nonTaxable']['regular'] = $this->nonTaxableProportion('a1', $this->income['regular']['a1']['nonTaxable']);
    $this->gross['a2']['monthly']['nonTaxable']['guaranteed'] = $this->nonTaxableProportion('a2', $this->income['guaranteed']['a2']['nonTaxable']);
    $this->gross['a2']['monthly']['nonTaxable']['regular'] = $this->nonTaxableProportion('a2', $this->income['regular']['a2']['nonTaxable']);
    // Calculate Net monthly income (non taxable reduced)
    $this->gross['a1']['monthly']['nonTaxable']['reduced'] = $this->monthlyNonTaxableReduced('a1');
    $this->gross['a2']['monthly']['nonTaxable']['reduced'] = $this->monthlyNonTaxableReduced('a2');
    // Calculate total Net monthly income per applicant.
    $this->gross['a1']['monthly']['total'] = $this->totalMonthlyIncomeApplicant('a1');
    $this->gross['a2']['monthly']['total'] = $this->totalMonthlyIncomeApplicant('a2');
    $this->gross['totalMonthly'] = $this->totalMonthlyIncome();

    /*
    $fileHandle = fopen("test.log", "a");
    fwrite($fileHandle,   "\n\n GROSS \n");
    fwrite($fileHandle, print_r($this->gross, TRUE) . "\n");
     */

    return $this->gross;
  }

  /**
   * Calculate gross income for applicant.
   */
  public function grossApplicant(array $arr) {
    return array_sum($arr);
  }

  /**
   * Calculate total gross income.
   */
  public function totalGross() {
    return array_sum([
      $this->gross['a1']['total'],
      $this->gross['a2']['total'],
    ]);
  }

  /**
   * Calculate total gross taxable income for applicant.
   */
  public function grossTaxableApplicant(array $arr, $applicant = NULL) {
    $result = max(array_sum($arr), 0);
    if (isset($this->income['deduction'])) {
      return $result - (SantanderBorrowResult::round_up($this->income['deduction'][$applicant]['taxFree'], 2) * 12);
    }
    else {
      return $result;
    }
  }

  /**
   * Calculate final tax free allowance.
   */
  public function finalTaxFreeAllowance($a) {
    if (isset($this->income['dividends'])) {
      return max([
        $this->taxFreeAllowance - (0.5 * max([
          ($this->gross[$a]['taxable'] + (10 / 9) * $this->income['dividends'][$a]) - 100000, 0,
        ])),
        0,
      ]);
    }
    else {
      return max([
        $this->taxFreeAllowance - (0.5 * max([
          $this->gross[$a]['taxable'] - 100000, 0,
        ])),
        0,
      ]);
    }
  }

  /**
   * Calculate tax at 10%.
   */
  public function tax10($a) {
    if ($this->gross[$a]['taxable'] > $this->gross[$a]['taxFreeAllowance']) {
      if (($this->gross[$a]['taxable'] - $this->gross[$a]['taxFreeAllowance']) > $this->rates['starting']['high']) {
        return $this->rates['starting']['high'] * $this->rates['starting']['rate'];
      }
      else {
        return ($this->gross[$a]['taxable'] - $this->gross[$a]['taxFreeAllowance']) * $this->rates['starting']['rate'];
      }
    }
    else {
      return 0;
    }
  }

  /**
   * Calculate tax at 20%.
   */
  public function tax20($a) {
    if ($this->gross[$a]['taxable'] > $this->gross[$a]['taxFreeAllowance']) {
      if (($this->gross[$a]['taxable'] - $this->gross[$a]['taxFreeAllowance']) > $this->rates['basic']['high']) {
        return $this->rates['basic']['high'] * $this->rates['basic']['rate'];
      }
      else {
        return ($this->gross[$a]['taxable'] - $this->gross[$a]['taxFreeAllowance']) * $this->rates['basic']['rate'];
      }
    }
    else {
      return 0;
    }
  }

  /**
   * Calculate tax at 40%.
   */
  public function tax40($a) {
    if ($this->gross[$a]['taxable'] > ($this->gross[$a]['taxFreeAllowance'] + $this->rates['basic']['high'])) {
      if (($this->gross[$a]['taxable'] - $this->gross[$a]['taxFreeAllowance'] - $this->rates['basic']['high']) > ($this->rates['higher']['high'] - $this->rates['basic']['high'])) {
        return ($this->rates['higher']['high'] - $this->rates['basic']['high']) * $this->rates['higher']['rate'];
      }
      else {
        return ($this->gross[$a]['taxable'] - $this->gross[$a]['taxFreeAllowance'] - $this->rates['basic']['high']) * $this->rates['higher']['rate'];
      }
    }
    else {
      return 0;
    }
  }

  /**
   * Calculate tax at 45%.
   */
  public function tax45($a) {
    if ($this->gross[$a]['taxable'] > ($this->rates['higher']['high'] + $this->gross[$a]['taxFreeAllowance'])) {
      return ($this->gross[$a]['taxable'] - $this->rates['higher']['high'] - $this->gross[$a]['taxFreeAllowance']) * $this->rates['additional']['rate'];
    }
    else {
      return 0;
    }
  }

  /**
   * Calculate NI at 2%.
   */
  public function ni2($a) {
    if (isset($this->income['pension'])) {
      $ni = $this->gross[$a]['taxable'] - $this->income['pension'][$a];
    }
    else {
      $ni = $this->gross[$a]['taxable'];
    }
    if ($ni > $this->primaryNiThreshold['high']) {
      return ($ni - $this->primaryNiThreshold['high']) * $this->upperEarningsNiLimit['rate'];
    }
    else {
      return 0;
    }
  }

  /**
   * Calculate NI at 12%.
   */
  public function ni12($a) {
    if (isset($this->income['pension'])) {
      $ni = $this->gross[$a]['taxable'] - $this->income['pension'][$a];
    }
    else {
      $ni = $this->gross[$a]['taxable'];
    }
    if ($ni > $this->primaryNiThreshold['low']) {
      if ($ni > $this->primaryNiThreshold['high']) {
        return ($this->primaryNiThreshold['high'] - $this->primaryNiThreshold['low']) * $this->primaryNiThreshold['rate'];
      }
      else {
        return ($ni - $this->primaryNiThreshold['low']) * $this->primaryNiThreshold['rate'];
      }
    }
    else {
      return 0;
    }
  }

  /**
   * Calculate 10% dividends
   * return Float.
   */
  public function div10($a) {
    if ($this->income['dividends'][$a] > $this->rates['basic']['high']) {
      return $this->applyTax($this->rates['basic']['high'], 'basic');
    }
    else {
      return $this->applyTax($this->income['dividends'][$a], 'basic');
    }
  }

  /**
   * Calculate 32.5% dividends
   * return Float.
   */
  public function div325($a) {
    $tax = 0;
    if ($this->income['dividends'][$a] > 0) {
      if ($this->upper_amt_to_tax($a) > 0) {
        $tax = $this->applyTax(((10 / 9) * $this->income['dividends'][$a] - $this->upper_amt_to_tax($a)), 'higher');
      }
      else {
        if ($this->gross[$a]['taxable'] > $this->rates['basic']['high']) {
          $n1 = ((10 / 9) * $this->income['dividends'][$a]);
          $n2 = ($this->gross[$a]['taxable'] + (10 / 9) * $this->income['dividends'][$a] - $this->rates['basic']['high'] - $this->gross[$a]['taxFreeAllowance']);
          if ($n1 < $n2) {
            $tax = $this->applyTax($n1, 'higher');
          }
          else {
            $tax = $this->applyTax($n2, 'higher');
          }
        }
        else {
          $tax = 0;
        }
      }
    }
    if ($tax > 0) {
      return $tax;
    }
    else {
      return 0;
    }
  }

  /**
   * Calculate 37.5% dividends
   * return Float.
   */
  public function div375($a) {
    if ($this->gross[$a]['taxable'] > $this->rates['additional']['low']) {
      return $this->applyTax(((10 / 9) * $this->income['dividends'][$a]), 'additional');
    }
    else {
      return 0;
    }
  }

  /**
   * Work out value of amount that needs to be taxed
   * return Float.
   */
  private function upper_amt_to_tax($a) {
    return ($this->gross[$a]['taxable'] + ((10 / 9) * $this->income['dividends'][$a])) - $this->rates['higher']['high'] - $this->gross[$a]['taxFreeAllowance'];
  }

  /**
   * Takes the sum of what to tax and taxes it
   * return Float.
   */
  private function applyTax($sum, $rate) {
    return $sum * ($this->rates[$rate]['dividends'] - $this->director_tax_credit);
  }

  /**
   * Calculate annual dividends minus tax
   * reuturn Float.
   */
  public function annualDividend($a) {
    return $this->income['dividends'][$a] - $this->annualDividendTax($a);
  }

  /**
   * Sum of all annual dividens (fixed for tax)
   * reuturn Float.
   */
  private function annualDividendTax($a) {
    return array_sum([
      $this->gross[$a]['div10'],
      $this->gross[$a]['div325'],
      $this->gross[$a]['div375'],
    ]);
  }

  /**
   * Calculate Net annual income (taxable)
   */
  public function annualTaxable($a) {
    return $this->gross[$a]['taxable'] - array_sum([
      $this->gross[$a]['tax10'],
      $this->gross[$a]['tax20'],
      $this->gross[$a]['tax40'],
      $this->gross[$a]['tax45'],
      $this->gross[$a]['ni12'],
      $this->gross[$a]['ni2'],
    ]);
  }

  /**
   * Calculate Net annual income (non taxable)
   */
  public function annualNonTaxable(array $arr) {
    return array_sum($arr);
  }

  /**
   * Calculate Net monthly income (taxable)
   */
  public function monthlyTaxable($a) {
    return max([
      max([$this->gross[$a]['annual']['taxable'] / 12, 0]),
    ]);
  }

  /**
   * Calculate Gross taxable proportion (%)
   */
  public function taxableProportion(array $arr, $a) {
    $v = array_sum($arr);
    if ($v == 0) {
      return 0;
    }
    else {
      return $a / $v;
    }
  }

  /**
   * Calculate Net monthly income (taxable reduced)
   */
  public function monthlyTaxableReduced($a) {
    return array_sum([
      $this->gross[$a]['monthly']['taxable']['basic'],
      $this->gross[$a]['monthly']['taxable']['guaranteed'],
      $this->gross[$a]['monthly']['taxable']['regular'] * $this->secondaryWeighting,
    ]) * $this->gross[$a]['monthly']['taxable']['total'];
  }

  /**
   * Calculate Net monthly income (non taxable)
   */
  public function monthlyNonTaxable($a) {
    if (isset($this->income['dividends'])) {
      return max([
        ($this->gross[$a]['annual']['nonTaxable'] / 12) - ($this->annualDividendTax($a) / 12), 0,
      ]);
    }
    else {
      return max([
        ($this->gross[$a]['annual']['nonTaxable'] / 12), 0,
      ]);
    }
  }

  /**
   * Calculate Gross non taxable proportion (%)
   */
  public function nonTaxableProportion($a, $b) {
    if ($this->gross[$a]['annual']['nonTaxable'] == 0) {
      return 0;
    }
    else {
      return $b / $this->gross[$a]['annual']['nonTaxable'];
    }
  }

  /**
   * Calculate Net monthly income (non taxable reduced)
   */
  public function monthlyNonTaxableReduced($a) {
    return array_sum([
      $this->gross[$a]['monthly']['nonTaxable']['basic'],
      $this->gross[$a]['monthly']['nonTaxable']['guaranteed'],
      $this->gross[$a]['monthly']['nonTaxable']['regular'] * $this->secondaryWeighting,
    ]) * $this->gross[$a]['monthly']['nonTaxable']['total'];
  }

  /**
   * Calculate total Net monthly income per applicant.
   */
  public function totalMonthlyIncomeApplicant($a) {
    return array_sum([
      $this->gross[$a]['monthly']['nonTaxable']['reduced'],
      $this->gross[$a]['monthly']['taxable']['reduced'],
    ]);
  }

  /**
   * Calculate total Net monthly income.
   */
  public function totalMonthlyIncome() {
    // Ensure there is no sclaing factor when it comes to the DIP calculator.
    $total = array_sum([
      $this->gross['a1']['monthly']['total'],
      $this->gross['a2']['monthly']['total'],
    ]);
    if (isset($this->income['deduction'])) {
      return SantanderBorrowResult::round_up($total, 2)
      - SantanderBorrowResult::round_up($this->income['deduction']['a1']['postTax'], 2)
      - SantanderBorrowResult::round_up($this->income['deduction']['a2']['postTax'], 2)
      - SantanderBorrowResult::round_up($this->income['deduction']['a1']['taxFree'], 2)
      - SantanderBorrowResult::round_up($this->income['deduction']['a2']['taxFree'], 2);
    }
    else {
      return SantanderBorrowResult::round_up($total) * $this->scalingFactor;
    }
  }

}


/**
 * Expenditure model.
 */
class SantanderBorrowResultExpenditure {

  private $quintiles = [
    'Q1' => [
      'without' => [
        'lower' => 0,
        'upper' => 991,
      ],
      'with' => [
        'lower' => 0,
        'upper' => 840,
      ],
    ],
    'Q2' => [
      'without' => [
        'lower' => 992,
        'upper' => 1822,
      ],
      'with' => [
        'lower' => 841,
        'upper' => 1559,
      ],
    ],
    'Q3' => [
      'without' => [
        'lower' => 1823,
        'upper' => 2654,
      ],
      'with' => [
        'lower' => 1560,
        'upper' => 2309,
      ],
    ],
    'Q4' => [
      'without' => [
        'lower' => 2655,
        'upper' => 3700,
      ],
      'with' => [
        'lower' => 2310,
        'upper' => 3327,
      ],
    ],
    'Q5' => [
      'without' => [
        'lower' => 3701,
        'upper' => 5707,
      ],
      'with' => [
        'lower' => 3328,
        'upper' => 5247,
      ],
    ],
    'Q6' => [
      'without' => [
        'lower' => 5708,
        'upper' => 7199,
      ],
      'with' => [
        'lower' => 5248,
        'upper' => 7199,
      ],
    ],
    'Q7' => [
      'without' => [
        'lower' => 7200,
        'upper' => 12318,
      ],
      'with' => [
        'lower' => 7200,
        'upper' => 12318,
      ],
    ],
    'Q8' => [
      'without' => [
        'lower' => 12319,
        'upper' => 99999,
      ],
      'with' => [
        'lower' => 12319,
        'upper' => 99999,
      ],
    ],
    'Q9' => [
      'without' => [
        'lower' => 100000,
        'upper' => 999999,
      ],
      'with' => [
        'lower' => 100000,
        'upper' => 999999,
      ],
    ],
  ];

  private $rates = [
    '1_0_Q1' => [
      'min' => 613,
      'max' => 613,
    ],
    '1_1_Q1' => [
      'min' => 644,
      'max' => 644,
    ],
    '1_2_Q1' => [
      'min' => 684,
      'max' => 684,
    ],
    '1_3+_Q1' => [
      'min' => 718,
      'max' => 718,
    ],
    '1_0_Q2' => [
      'min' => 613,
      'max' => 613,
    ],
    '1_1_Q2' => [
      'min' => 644,
      'max' => 648,
    ],
    '1_2_Q2' => [
      'min' => 684,
      'max' => 687,
    ],
    '1_3+_Q2' => [
      'min' => 718,
      'max' => 720,
    ],
    '1_0_Q3' => [
      'min' => 613,
      'max' => 777,
    ],
    '1_1_Q3' => [
      'min' => 648,
      'max' => 747,
    ],
    '1_2_Q3' => [
      'min' => 687,
      'max' => 789,
    ],
    '1_3+_Q3' => [
      'min' => 720,
      'max' => 817,
    ],
    '1_0_Q4' => [
      'min' => 777,
      'max' => 949,
    ],
    '1_1_Q4' => [
      'min' => 747,
      'max' => 1036,
    ],
    '1_2_Q4' => [
      'min' => 789,
      'max' => 1095,
    ],
    '1_3+_Q4' => [
      'min' => 817,
      'max' => 1135,
    ],
    '1_0_Q5' => [
      'min' => 949,
      'max' => 1278,
    ],
    '1_1_Q5' => [
      'min' => 1036,
      'max' => 1274,
    ],
    '1_2_Q5' => [
      'min' => 1095,
      'max' => 1342,
    ],
    '1_3+_Q5' => [
      'min' => 1135,
      'max' => 1394,
    ],
    '1_0_Q6' => [
      'min' => 1278,
      'max' => 1468,
    ],
    '1_1_Q6' => [
      'min' => 1274,
      'max' => 1523,
    ],
    '1_2_Q6' => [
      'min' => 1342,
      'max' => 1604,
    ],
    '1_3+_Q6' => [
      'min' => 1394,
      'max' => 1667,
    ],
    '1_0_Q7' => [
      'min' => 1468,
      'max' => 1958,
    ],
    '1_1_Q7' => [
      'min' => 1523,
      'max' => 2031,
    ],
    '1_2_Q7' => [
      'min' => 1604,
      'max' => 2139,
    ],
    '1_3+_Q7' => [
      'min' => 1667,
      'max' => 2222,
    ],
    '1_0_Q8' => [
      'min' => 1958,
      'max' => 10351,
    ],
    '1_1_Q8' => [
      'min' => 2031,
      'max' => 10732,
    ],
    '1_2_Q8' => [
      'min' => 2139,
      'max' => 11303,
    ],
    '1_3+_Q8' => [
      'min' => 2222,
      'max' => 11728,
    ],
    '1_0_Q9' => [
      'min' => 10351,
      'max' => 10351,
    ],
    '1_1_Q9' => [
      'min' => 10732,
      'max' => 10732,
    ],
    '1_2_Q9' => [
      'min' => 11303,
      'max' => 11303,
    ],
    '1_3+_Q9' => [
      'min' => 11728,
      'max' => 11728,
    ],
    '2+_0_Q1' => [
      'min' => 1093,
      'max' => 1093,
    ],
    '2+_1_Q1' => [
      'min' => 1130,
      'max' => 1130,
    ],
    '2+_2_Q1' => [
      'min' => 1156,
      'max' => 1156,
    ],
    '2+_3+_Q1' => [
      'min' => 1177,
      'max' => 1177,
    ],
    '2+_0_Q2' => [
      'min' => 1093,
      'max' => 1093,
    ],
    '2+_1_Q2' => [
      'min' => 1130,
      'max' => 1130,
    ],
    '2+_2_Q2' => [
      'min' => 1156,
      'max' => 1156,
    ],
    '2+_3+_Q2' => [
      'min' => 1177,
      'max' => 1177,
    ],
    '2+_0_Q3' => [
      'min' => 1093,
      'max' => 1184,
    ],
    '2+_1_Q3' => [
      'min' => 1130,
      'max' => 1161,
    ],
    '2+_2_Q3' => [
      'min' => 1156,
      'max' => 1195,
    ],
    '2+_3+_Q3' => [
      'min' => 1177,
      'max' => 1218,
    ],
    '2+_0_Q4' => [
      'min' => 1184,
      'max' => 1400,
    ],
    '2+_1_Q4' => [
      'min' => 1161,
      'max' => 1404,
    ],
    '2+_2_Q4' => [
      'min' => 1195,
      'max' => 1463,
    ],
    '2+_3+_Q4' => [
      'min' => 1218,
      'max' => 1503,
    ],
    '2+_0_Q5' => [
      'min' => 1400,
      'max' => 1665,
    ],
    '2+_1_Q5' => [
      'min' => 1404,
      'max' => 1648,
    ],
    '2+_2_Q5' => [
      'min' => 1463,
      'max' => 1716,
    ],
    '2+_3+_Q5' => [
      'min' => 1503,
      'max' => 1764,
    ],
    '2+_0_Q6' => [
      'min' => 1665,
      'max' => 1913,
    ],
    '2+_1_Q6' => [
      'min' => 1648,
      'max' => 1970,
    ],
    '2+_2_Q6' => [
      'min' => 1716,
      'max' => 2051,
    ],
    '2+_3+_Q6' => [
      'min' => 1764,
      'max' => 2109,
    ],
    '2+_0_Q7' => [
      'min' => 1913,
      'max' => 2551,
    ],
    '2+_1_Q7' => [
      'min' => 1970,
      'max' => 2627,
    ],
    '2+_2_Q7' => [
      'min' => 2051,
      'max' => 2735,
    ],
    '2+_3+_Q7' => [
      'min' => 2109,
      'max' => 2812,
    ],
    '2+_0_Q8' => [
      'min' => 2551,
      'max' => 13479,
    ],
    '2+_1_Q8' => [
      'min' => 2627,
      'max' => 13880,
    ],
    '2+_2_Q8' => [
      'min' => 2735,
      'max' => 14451,
    ],
    '2+_3+_Q8' => [
      'min' => 2812,
      'max' => 14853,
    ],
    '2+_0_Q9' => [
      'min' => 13479,
      'max' => 13479,
    ],
    '2+_1_Q9' => [
      'min' => 13880,
      'max' => 13880,
    ],
    '2+_2_Q9' => [
      'min' => 14451,
      'max' => 14451,
    ],
    '2+_3+_Q9' => [
      'min' => 14853,
      'max' => 14853,
    ],
  ];

  private $scalingFactor = 1.00;

  /**
   * Mortgage.
   */
  private $mortgage = NULL;

  /**
   * Income.
   */
  private $income = NULL;

  /**
   * Income gross.
   */
  private $gross = NULL;

  /**
   * Expenditure.
   */
  private $expenditure = [
  // ÂŁ.
    'gross' => 0,
  // ÂŁ.
    'net' => 0,
    'applicants' => 0,
    'dependants' => 0,
    'quintile' => 0,
    'concat' => '',
  // ÂŁ.
    'min' => 0,
  // ÂŁ.
    'max' => 0,
  // ÂŁ.
    'lowerLimit' => 0,
  // ÂŁ.
    'upperLimit' => 0,
  // ÂŁ.
    'quintileDifference' => 0,
  // ÂŁ.
    'incomeLowerQuintileDifference' => 0,
  // %.
    'proportion' => 0,
  // ÂŁ.
    'difference' => 0,
  // ÂŁ.
    'raw' => 0,
  // ÂŁ.
    'final' => 0,
  ];

  /**
   * Constructor.
   */
  public function __construct(array $mortgage, array $income, array $gross) {
    $this->mortgage = $mortgage;
    $this->income = $income;
    $this->gross = $gross;
  }

  /**
   * Calculate low/medium expenditure.
   */
  public function getExpenditure() {
    // Calculate total joint gross income.
    $this->expenditure['gross'] = $this->totalJointGrossIncome([
      $this->income['basic']['a1']['taxable'],
      $this->income['guaranteed']['a1']['taxable'],
      $this->income['guaranteed']['a1']['nonTaxable'],
      $this->income['regular']['a1']['taxable'],
      $this->income['regular']['a1']['nonTaxable'],
      $this->income['basic']['a2']['taxable'],
      $this->income['guaranteed']['a2']['taxable'],
      $this->income['guaranteed']['a2']['nonTaxable'],
      $this->income['regular']['a2']['taxable'],
      $this->income['regular']['a2']['nonTaxable'],
    ]);
    // Calculate total joint net income.
    $this->expenditure['net'] = $this->totalJointNetIncome([
      $this->gross['a1']['monthly']['total'],
      $this->gross['a2']['monthly']['total'],
    ]);
    // Calculate number of applicants.
    $this->expenditure['applicants'] = $this->numberOfApplicants($this->mortgage['applicants']);
    // Calculate number of dependants.
    $this->expenditure['dependants'] = $this->numberOfDependants($this->mortgage['dependants']);
    // Calculate income quintile.
    $this->expenditure['quintile'] = $this->incomeQuintile();
    // Concat.
    $this->expenditure['concat'] = $this->concat();
    // Calculate min/max expenditure.
    $this->expenditure['min'] = $this->minMaxExpenditure($this->expenditure['applicants'], 'min');
    $this->expenditure['max'] = $this->minMaxExpenditure($this->expenditure['applicants'], 'max');
    // Calculate quintile lower/upper limit.
    $this->expenditure['lowerLimit'] = $this->quintileLimit($this->expenditure['dependants'], 'lower');
    $this->expenditure['upperLimit'] = $this->quintileLimit($this->expenditure['dependants'], 'upper');
    // Calculate quintile differences.
    $this->expenditure['quintileDifference'] = $this->quintileDifferences();
    // Calculate income to lower quintile difference.
    $this->expenditure['incomeLowerQuintileDifference'] = $this->incomeLowerQuintileDifference();
    // Calculate proportion.
    $this->expenditure['proportion'] = $this->proportion();
    // Calculate expenditure difference.
    $this->expenditure['difference'] = $this->difference();
    // Calculate raw expenditure.
    $this->expenditure['raw'] = $this->raw();
    // Calculate final expenditure.
    $this->expenditure['final'] = $this->finalExpenditure();
    return $this->expenditure;
  }

  /**
   * Calculate total joint gross income.
   */
  public function totalJointGrossIncome(array $arr) {
    return array_sum($arr);
  }

  /**
   * Calculate total joint net income.
   */
  public function totalJointNetIncome(array $arr) {
    return array_sum($arr);
  }

  /**
   * Calculate number of applicants.
   */
  public function numberOfApplicants($a) {
    if ($a > 1) {
      return '2+';
    }
    else {
      return $a;
    }
  }

  /**
   * Calculate number of dependants.
   */
  public function numberOfDependants($a) {
    if ($a > 2) {
      return '3+';
    }
    else {
      return $a;
    }
  }

  /**
   * Calculate income quintile.
   */
  public function incomeQuintile() {
    $q = '';
    if ($this->expenditure['dependants'] == 0) {
      if ($this->expenditure['net'] > $this->quintiles['Q8']['without']['upper']) {
        $q = 'Q9';
      }
      else {
        if ($this->expenditure['net'] > $this->quintiles['Q7']['without']['upper']) {
          $q = 'Q8';
        }
        else {
          if ($this->expenditure['net'] > $this->quintiles['Q6']['without']['upper']) {
            $q = 'Q7';
          }
          else {
            if ($this->expenditure['net'] > $this->quintiles['Q5']['without']['upper']) {
              $q = 'Q6';
            }
            else {
              if ($this->expenditure['net'] > $this->quintiles['Q4']['without']['upper']) {
                $q = 'Q5';
              }
              else {
                if ($this->expenditure['net'] > $this->quintiles['Q3']['without']['upper']) {
                  $q = 'Q4';
                }
                else {
                  if ($this->expenditure['net'] > $this->quintiles['Q2']['without']['upper']) {
                    $q = 'Q3';
                  }
                  else {
                    if ($this->expenditure['net'] > $this->quintiles['Q1']['without']['upper']) {
                      $q = 'Q2';
                    }
                    else {
                      $q = 'Q1';
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    else {
      if ($this->expenditure['net'] > $this->quintiles['Q8']['with']['upper']) {
        $q = 'Q9';
      }
      else {
        if ($this->expenditure['net'] > $this->quintiles['Q7']['with']['upper']) {
          $q = 'Q8';
        }
        else {
          if ($this->expenditure['net'] > $this->quintiles['Q6']['with']['upper']) {
            $q = 'Q7';
          }
          else {
            if ($this->expenditure['net'] > $this->quintiles['Q5']['with']['upper']) {
              $q = 'Q6';
            }
            else {
              if ($this->expenditure['net'] > $this->quintiles['Q4']['with']['upper']) {
                $q = 'Q5';
              }
              else {
                if ($this->expenditure['net'] > $this->quintiles['Q3']['with']['upper']) {
                  $q = 'Q4';
                }
                else {
                  if ($this->expenditure['net'] > $this->quintiles['Q2']['with']['upper']) {
                    $q = 'Q3';
                  }
                  else {
                    if ($this->expenditure['net'] > $this->quintiles['Q1']['with']['upper']) {
                      $q = 'Q2';
                    }
                    else {
                      $q = 'Q1';
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return $q;
  }

  /**
   * Concat.
   */
  public function concat() {
    return $this->expenditure['applicants'] . '_' . $this->expenditure['dependants'] . '_' . $this->expenditure['quintile'];
  }

  /**
   * Calculate min/max expenditure.
   */
  public function minMaxExpenditure($a, $b) {
    if ($a == 0) {
      return 0;
    }
    else {
      return $this->rates[$this->expenditure['concat']][$b];
    }
  }

  /**
   * Calculate quintile lower/upper limit.
   */
  public function quintileLimit($a, $b) {
    if ($a == 0) {
      return $this->quintiles[$this->expenditure['quintile']]['without'][$b];
    }
    else {
      return $this->quintiles[$this->expenditure['quintile']]['with'][$b];
    }
  }

  /**
   * Calculate quintile differences.
   */
  public function quintileDifferences() {
    return $this->expenditure['upperLimit'] - $this->expenditure['lowerLimit'];
  }

  /**
   * Calculate income to lower quintile difference.
   */
  public function incomeLowerQuintileDifference() {
    return $this->expenditure['net'] - $this->expenditure['lowerLimit'];
  }

  /**
   * Calculate proportion.
   */
  public function proportion() {
    return $this->expenditure['incomeLowerQuintileDifference'] / ($this->expenditure['quintileDifference'] + 1);
  }

  /**
   * Calculate expenditure difference.
   */
  public function difference() {
    return $this->expenditure['max'] - $this->expenditure['min'];
  }

  /**
   * Calculate raw expenditure.
   */
  public function raw() {
    return SantanderBorrowResult::round_up(array_sum([
      $this->expenditure['proportion'] * $this->expenditure['difference'],
      $this->expenditure['min'],
    ]));
  }

  /**
   * Calculate final expenditure.
   */
  public function finalExpenditure() {
    return $this->expenditure['raw'] * $this->scalingFactor;
  }

}

/**
 * Credit model.
 */
class SantanderBorrowResultCredit {

  /**
   * Commitments.
   */
  private $commitments = NULL;

  /**
   * Credit.
   */
  /**
   * ÂŁ.
   */
  private $credit = 0;

  /**
   * Constructor.
   */
  public function __construct(array $commitments) {
    $this->commitments = $commitments;
  }

  /**
   * Calculate credit commitments.
   */
  public function getCredit() {
    $this->credit = $this->monthlyCreditCommitments();
    return $this->credit;
  }

  /**
   * Calculate credit commitments.
   */
  public function monthlyCreditCommitments() {
    return SantanderBorrowResult::round_up(
      array_sum([
        $this->commitments['loans'],
        0.03 * $this->commitments['creditCard'],
      ])
    );
  }

}

/**
 * Loan model.
 */
class SantanderBorrowResultLoan {

  /**
   * LTV.
   */
  private $ltv = [
    'p4p' => [
  // %
      'rate' => 0.07,
      'single' => 5.5,
      'joint' => 5.5,
      'ftb_rate' => 4.45,
    ],
    'low' => [
    // %
      'rate' => 0.07,
      'single' => 5.5,
      'joint' => 5.5,
      'ftb_rate' => 4.45,
    ],
    'medium' => [
    // %
      'rate' => 0.07,
      'single' => 5.5,
      'joint' => 5.5,
      'ftb_rate' => 4.45,
    ],
    'high' => [
    // %
      'rate' => 0.07,
      'single' => 5,
      'joint' => 5,
      'ftb_rate' => 4.45,
    ],
    'higher' => [
    // %
      'rate' => 0.07,
      'single' => 4.45,
      'joint' => 4.45,
      'ftb_rate' => 4.45,
    ],
  ];

  /**
   * Mortgage.
   */
  private $mortgage = NULL;

  /**
   * Income.
   */
  private $income = NULL;

  /**
   * Secondary Weighting.
   */
  /**
   * 65%.
   */
  private $secondaryWeighting = 0.65;

  /**
   * Commitments.
   */
  private $commitments = NULL;

  /**
   * Gross.
   */
  private $gross = NULL;

  /**
   * Expenditure.
   */
  private $expenditure = NULL;

  /**
   * Credit.
   */
  private $credit = NULL;

  /**
   * Loan.
   */
  private $loan = [
  // ÂŁ.
    'allowable' => 0,
  // ÂŁ.
    'disposable' => 0,
    'ltv' => [
      'low' => [
  // ÂŁ.
        'affordable' => 0,
  // ÂŁ.
        'multiple' => 0,
  // ÂŁ.
        'ltv' => 0,
  // ÂŁ.
        'restricted' => 0,
  // %.
        'percentage' => 0,
      ],
      'medium' => [
      // ÂŁ.
        'affordable' => 0,
      // ÂŁ.
        'multiple' => 0,
      // ÂŁ.
        'ltv' => 0,
      // ÂŁ.
        'restricted' => 0,
      // %.
        'percentage' => 0,
      ],
      'high' => [
      // ÂŁ.
        'affordable' => 0,
      // ÂŁ.
        'multiple' => 0,
      // ÂŁ.
        'ltv' => 0,
      // ÂŁ.
        'restricted' => 0,
      // %.
        'percentage' => 0,
      ],
      'higher' => [
      // ÂŁ.
        'affordable' => 0,
      // ÂŁ.
        'multiple' => 0,
      // ÂŁ.
        'restricted' => 0,
      // %
        'percentage' => 0,
      ],
    ],
    // ÂŁ.
    'loan' => 0,
  ];

  /**
   * Constructor.
   */
  public function __construct($mortgage, $income, $commitments, $i, $e, $c) {
    $this->mortgage = $mortgage;
    $this->income = $income;
    $this->commitments = $commitments;
    $this->gross = $i;
    $this->expenditure = $e;
    $this->credit = $c;
  }

  /**
   * Calculate max loan.
   */
  public function getLoan() {
    $this->loan['allowable'] = $this->allowableIncome();
    $this->loan['disposable'] = $this->disposableIncome();

    $this->loan['ltv']['p4p']['affordable'] = $this->p4pAffortable();
    $this->loan['ltv']['p4p']['multiple'] = $this->maxAmountMultiple('p4p');
    $this->loan['ltv']['p4p']['ltv'] = $this->lowMaxAmountLTV();
    $this->loan['ltv']['p4p']['restricted'] = $this->p4pRestrictedAmount();
    $this->loan['ltv']['p4p']['percentage'] = $this->maxAmountLTVPercentage('p4p');

    $this->loan['ltv']['low']['affordable'] = $this->lowAffortable();
    $this->loan['ltv']['low']['multiple'] = $this->maxAmountMultiple('low');
    $this->loan['ltv']['low']['ltv'] = $this->lowMaxAmountLTV();
    $this->loan['ltv']['low']['restricted'] = $this->lowRestrictedAmount();
    $this->loan['ltv']['low']['percentage'] = $this->maxAmountLTVPercentage('low');

    $this->loan['ltv']['medium']['affordable'] = $this->mediumAffortable();
    $this->loan['ltv']['medium']['multiple'] = $this->maxAmountMultiple('medium');
    $this->loan['ltv']['medium']['ltv'] = $this->mediumMaxAmountLTV();
    $this->loan['ltv']['medium']['restricted'] = $this->mediumRestrictedAmount();
    $this->loan['ltv']['medium']['percentage'] = $this->maxAmountLTVPercentage('medium');

    $this->loan['ltv']['high']['affordable'] = $this->highAffortable();
    $this->loan['ltv']['high']['multiple'] = $this->maxAmountMultiple('high');
    $this->loan['ltv']['high']['ltv'] = $this->highMaxAmountLTV();
    $this->loan['ltv']['high']['restricted'] = $this->highRestrictedAmount();
    $this->loan['ltv']['high']['percentage'] = $this->maxAmountLTVPercentage('high');

    $this->loan['ltv']['higher']['affordable'] = $this->higherAffortable();
    $this->loan['ltv']['higher']['multiple'] = $this->maxAmountMultiple('higher');
    $this->loan['ltv']['higher']['restricted'] = $this->higherRestrictedAmount();
    $this->loan['ltv']['higher']['percentage'] = $this->maxAmountLTVPercentage('higher');

    $this->loan['loan'] = $this->maxLoanAmount();

    /*
    $fileHandle = fopen("test.log", "a");
    fwrite($fileHandle,   "\n\n LOAN \n");
    fwrite($fileHandle, print_r($this->loan, TRUE) . "\n");
     */

    return $this->loan;
  }

  /**
   * Calculate allowable income.
   */
  public function allowableIncome() {
    $sum1 = $this->income['basic']['a1']['taxable']
      + $this->income['basic']['a2']['taxable']
      + $this->income['guaranteed']['a1']['taxable']
      + $this->income['guaranteed']['a2']['taxable']
      + $this->income['guaranteed']['a1']['nonTaxable']
      + $this->income['guaranteed']['a2']['nonTaxable'];

    $sum2 = $this->income['regular']['a1']['taxable']
      + $this->income['regular']['a1']['nonTaxable']
      + $this->income['regular']['a2']['taxable']
      + $this->income['regular']['a2']['nonTaxable'];

    $final = $sum1 + $this->secondaryWeighting * $sum2;

    return $final;

  }

  /**
   * Calculate disposable income.
   */
  public function disposableIncome() {
    /*
    $fileHandle = fopen("test.log", "a");
    fwrite($fileHandle, '$this->grosstotalMonthly '.$this->gross['totalMonthly'].'  -  final:'.$this->expenditure['final'].'  - credit:'.$this->credit.' - outgoings:'.$this->commitments['outgoings']. "\n");
    fclose($fileHandle);
     */
    return $this->gross['totalMonthly'] - $this->expenditure['final'] - $this->credit - $this->commitments['outgoings'];
  }

  /**
   * Calculate low LTV max amount affordable.
   */
  public function p4pAffortable() {
    return max([
      (
        $this->loan['disposable'] *
        (1 - pow((1 + ($this->ltv['p4p']['rate'] / 12)), (-1 * (12 * $this->mortgage['term']))))
      ) / ($this->ltv['p4p']['rate'] / 12),
    ]);
  }

  /**
   * Calculate low LTV max amount affordable.
   */
  public function lowAffortable() {
    return max([
      (
            $this->loan['disposable'] *
            (1 - pow((1 + ($this->ltv['low']['rate'] / 12)), (-1 * (12 * $this->mortgage['term']))))
      ) / ($this->ltv['low']['rate'] / 12),
    ]);
  }

  /**
   * Calculate medium LTV max amount affordable.
   */
  public function mediumAffortable() {
    return max([
      (
            $this->loan['disposable'] *
            (1 - pow((1 + ($this->ltv['medium']['rate'] / 12)), (-1 * (12 * $this->mortgage['term']))))
      ) / ($this->ltv['medium']['rate'] / 12),
    ]);
  }

  /**
   * Calculate high LTV max amount affordable.
   */
  public function highAffortable() {
    $affordable = max([
      (
            $this->loan['disposable'] *
            (1 - pow((1 + ($this->ltv['high']['rate'] / 12)), (-1 * (12 * $this->mortgage['term']))))
      ) / ($this->ltv['high']['rate'] / 12),
    ]);

    return $affordable;
  }

  /**
   * Calculate higher LTV max amount affordable.
   */
  public function higherAffortable() {
    $affordable = max([
      (
            $this->loan['disposable'] *
            (1 - pow((1 + ($this->ltv['higher']['rate'] / 12)), (-1 * (12 * $this->mortgage['term']))))
      ) / ($this->ltv['higher']['rate'] / 12),
    ]);

    return $affordable;
  }

  /**
   * Calculate max amount multiple.
   */
  public function maxAmountMultiple($a) {
    if ($this->mortgage['buyers'] == 'yes') {
      $maxAmount = $this->ltv[$a]['ftb_rate'] * $this->loan['allowable'];
    }
    else {
      if ($this->mortgage['applicants'] == 1) {
        $maxAmount = $this->ltv[$a]['single'] * $this->loan['allowable'];
      }
      else {
        $maxAmount = $this->ltv[$a]['joint'] * $this->loan['allowable'];
      }
    }

    return $maxAmount;
  }

  /**
   * Calculate low max amount LTV.
   */
  public function lowMaxAmountLTV() {
    return (0.75 / 0.25) * $this->mortgage['deposit'];
  }

  /**
   * Calculate medium max amount LTV.
   */
  public function mediumMaxAmountLTV() {
    return (0.85 / 0.15) * $this->mortgage['deposit'];
  }

  /**
   * Calculate high max amount LTV.
   */
  public function highMaxAmountLTV() {
    return (0.9 / 0.1) * $this->mortgage['deposit'];
  }

  /**
   * Calculate p4p restricted amount.
   */
  public function p4pRestrictedAmount() {
    return min([
      $this->loan['ltv']['p4p']['affordable'],
      $this->loan['ltv']['p4p']['multiple'],
      $this->mortgage['current_mortgage_balance'],
    ]);
  }

  /**
   * Calculate low restricted amount.
   */
  public function lowRestrictedAmount() {
    return min([
      $this->loan['ltv']['low']['affordable'],
      $this->loan['ltv']['low']['multiple'],
      $this->loan['ltv']['low']['ltv'],
    ]);
  }

  /**
   * Calculate medium restricted amount.
   */
  public function mediumRestrictedAmount() {
    return min([
      $this->loan['ltv']['medium']['affordable'],
      $this->loan['ltv']['medium']['multiple'],
      $this->loan['ltv']['medium']['ltv'],
    ]);
  }

  /**
   * Calculate high restricted amount.
   */
  public function highRestrictedAmount() {
    return min([
      $this->loan['ltv']['high']['affordable'],
      $this->loan['ltv']['high']['multiple'],
      $this->loan['ltv']['high']['ltv'],
    ]);
  }

  /**
   * Calculate higher restricted amount.
   */
  public function higherRestrictedAmount() {
    return min([
      $this->loan['ltv']['higher']['affordable'],
      $this->loan['ltv']['higher']['multiple'],
    ]);
  }

  /**
   * Calculate max amount LTV (percentage)
   */
  public function maxAmountLTVPercentage($a) {
    if (($this->mortgage['deposit'] + $this->loan['ltv'][$a]['restricted']) == 0) {
      return '';
    }

    return $this->loan['ltv'][$a]['restricted'] / ($this->loan['ltv'][$a]['restricted'] + $this->mortgage['deposit']);
  }

  /**
   * Calculate maximum loan amount.
   */
  public function maxLoanAmount() {
    if ($this->mortgage['buyer_type'] == 'remortgage' && $this->mortgage['extra'] == 0) {
      return floor($this->loan['ltv']['p4p']['restricted']);
    }
    if ($this->loan['ltv']['higher']['percentage'] > 0.9) {
      return floor($this->loan['ltv']['higher']['restricted']);
    }
    if ($this->gross['total'] >= 45000 && $this->loan['ltv']['high']['percentage'] > 0.75) {
      return floor($this->loan['ltv']['high']['restricted']);
    }
    if ($this->gross['total'] >= 100000) {
      return floor($this->loan['ltv']['low']['restricted']);
    }
    if ($this->gross['total'] >= 45000 && $this->loan['ltv']['high']['percentage'] > $this->loan['ltv']['high']['higher']) {
      return floor($this->loan['ltv']['high']['restricted']);
    }

    return floor($this->loan['ltv']['higher']['restricted']);
  }

}

/**
 *
 */
function santander_borrow_get_result() {
  $data = $_POST;
  $result = new SantanderBorrowResult();
  $validated = $result->validate($data);
  $result->setParams($data);

  $loan = $result->calculate();

  print drupal_json_encode(['loan' => $loan, 'validated' => $validated]);
  drupal_exit();
}
