<?php

namespace Drupal\santander_mortgage_calculator;

/**
 * Credit model.
 */
class BorrowResultCredit {

  /**
   * {@inheritdoc}
   *
   * Commitments.
   */
  private $commitments = NULL;

  /**
   * {@inheritdoc}
   *
   * Credit.
   */
  private $credit = 0;

  /**
   * Constructor.
   */
  public function __construct(array $commitments) {
    $this->commitments = $commitments;
  }

  /**
   * Calculate credit commitments.
   */
  public function getCredit() {
    $this->credit = $this->monthlyCreditCommitments();
    return $this->credit;
  }

  /**
   * Calculate credit commitments.
   */
  public function monthlyCreditCommitments() {
    return BorrowResult::roundUp(
      array_sum([
        $this->commitments['loans'],
        0.03 * $this->commitments['creditCard'],
      ])
    );
  }

}
