(function ($, Drupal) {

  "use strict";

  Drupal.behaviors.santander_webforms_mortgage_calculator_additional = {

    attach: function (context, settings) {

      // Initialisation on page/load/transitions etc..
      $('.mortgage-calculator-additional').once('santander_webforms_mortgage_calculator_additional_init').each(function(index) {

        // Make sure any loan parts with values are visible - Loan parts are hidden when page is loaded so when returning from results page
        // to question page we need to unhide any hidden ones with values.
        $('.container-loan-parts .loan-part:hidden', this).each(function(index) {
          if ($(this).find('input[type=text]').val() != '') {
            $(this).show(0);
          }
        });

      });

      // Handle enabling and disabling of the form submit buttons
      $('.mortgage-calculator-additional input, .mortgage-calculator-additional select').once('santander_webforms_mortgage_calculator_additional_gates').each(function(index) {

        // http://jsfiddle.net/bjpv0hak/4/
        // https://stackoverflow.com/questions/15146110/is-there-a-good-cross-browser-way-to-detect-changes-to-an-input-yet

        $(this).on('input change', function(event) {

          // Ignore input events on radio and select elements as these will be handled by the change
          if ((event.type === 'input')) {
            if ($(this).is(':radio') || $(this).is('select')) {
              return;
            }
          // Ignore change events on text elements as these will be handled by the input
          } else if ((event.type === 'change')) {
            if ($(this).is('input:text')) {
              return;
            };
          }

          // Not on the questions page so quit
          if (!($('.progress-tracker-full .progress-step.is-active').data('webform-page') === 'page_questions')) {
            return;
          }

          var $questions = $(".mortgage-calculator-additional .page-questions");

          var isOk = true;

          // Number of dependents chosen.
          if ($('input[name="dependants"]:checked', $questions).length === 0) {
            isOk = false;
          }

          // Property value entered
          if ($('input[name="property_value"]', $questions).val() == 0) {
            isOk = false;
          }

          // Debt consolidation. (Not selected)
          if ($('input[name="debt_consolidation"]:checked', $questions).length === 0) {
            isOk = false;
          }

          // Debt consolidation. (Yes choosen)
          if ($('input[name="debt_consolidation"]:checked', $questions).val() == 0) {
            isOk = false;
          }

          // Depending on number of applicants check income field
          if ($('input[name="income_applicant_1_annual_amount"]', $questions).val() == 0) {
            isOk = false;
          }

          if ($('input[name="applicants"]:checked', $questions).val() === '2') {
            if ($('input[name="income_applicant_2_annual_amount"]', $questions).val() == 0) {
              isOk = false;
            }
          }

          // Loan part 1 must have a balance and repayment method choosen
          if ($('input[name="loan_part_amount_1"]', $questions).val() == 0) {
            isOk = false;
          } else {
            if ($('input[name="loan_part_repayment_method_1"]:checked', $questions).length === 0) {
              isOk = false;
            }
          }

          // Loan part 2,3,4,5 must have repayment method selected if the balance has a value
          for (var i = 2; i <= 5; i++) {
            var amount = $('input[name="loan_part_amount_' + i + '"]', $questions).val();
            if (amount != undefined && amount != '' && amount != 0) {
              if ($('input[name="loan_part_repayment_method_' + i + '"]:checked', $questions).length === 0) {
                isOk = false;
                break;
              }
            }
          }

          // If any loan part is interest only and any loan part term is more than 25 years then show message limiting loan term
          var loanTerm = (parseInt($('select[name="loan_term_years"]', $questions).val()) * 12) + parseInt($('select[name="loan_term_months"]', $questions).val());
          var loanPartTerm = 0;
          var maxLoanPartTerm = 0;
          var hasIoPart = false;

          for (var i = 1; i <= 5; i++) {

            if ($('input[name="loan_part_repayment_method_' + i + '"]:checked', $questions).val() === 'interest') {
              hasIoPart = true;
            }

            loanPartTerm = (parseInt($('select[name="loan_part_term_years_' + i + '"]', $questions).val()) * 12) + parseInt($('select[name="loan_part_term_months_' + i + '"]', $questions).val());

            if (loanPartTerm > maxLoanPartTerm) {
              maxLoanPartTerm = loanPartTerm;
            }
          }

          // Show message if required
//          if (hasIoPart && loanTerm > (25 * 12)) {
          if (hasIoPart && maxLoanPartTerm > (25 * 12)) {
//            isOk = false;
            $('.message-box-io-25-years', $questions).removeClass('message-box-hidden');
          } else {
            $('.message-box-io-25-years', $questions).addClass('message-box-hidden');
          }

          // If ltv > 85% then show warning and disable results button
          // First workout total exsiting loan amount
          var totalLoan = 0;

          for (var i = 1; i <= 5; i++) {

            var amount = parseFloat($('input[name="loan_part_amount_' + i + '"]', $questions).val().replace(/,/g, ''));
            if (!isNaN(amount)) {
              totalLoan += amount;
            }
          }

          var propertyValue = parseFloat($('input[name="property_value"]', $questions).val().replace(/,/g, ''));

          if (!isNaN(propertyValue)) {
            var ltv = (totalLoan * 100) / propertyValue;
            if (ltv > 85) {
              isOk = false;
              $('.message-box-ltv-85-plus', $questions).removeClass('message-box-hidden');
            } else {
              $('.message-box-ltv-85-plus', $questions).addClass('message-box-hidden');
            }
          }

          // If all ok then enable the button
          if (isOk) {
            $('button[name="show-results"]', $questions).prop('disabled', false);
          } else {
            $('button[name="show-results"]', $questions).prop('disabled', true);
          };
        });
      });

      // Handle the range fields
      $('.mortgage-calculator-additional input[type="range"]').once('santander_webforms_mortgage_calculator_additional_range_update').each(function(index) {

        var $range = $(this);

        // Move the input field above the range and add + and - buttons
        $($range.next()).insertBefore($range);
        $('<span class="step-update decrease-step-amount circle minus"></span>').insertBefore($range.prevAll('output.range-input'));
        $('<span class="step-update increase-step-amount circle plus"></span>').insertAfter($range.prevAll('output.range-input'));

        $range.on('input change', function () {

          // Cache this for efficiency
          var  el = $(this);
          var valueIn = el.val();

          // Set output text with prefix and suffix.
          var text = (el.prevAll('output').attr('data-field-prefix') || '') + valueIn + (el.prevAll('output').attr('data-field-suffix') || '');

          el.prevAll('output').text(text);

        }).trigger('input');
      }).change();

      // Handle the amount range
      $('.mortgage-calculator-additional input[name="borrow_amount"]').once('santander_webforms_mortgage_calculator_range_borrow').each(function(index) {

        var $range = $(this);

        $range.on('input change', function() {

          var value = webformComponents.formatMoney($(this).val(), 0);

          $(this).prevAll('output').text(value);

        });

        // Initialise the range value, size and step value
        // Slider should be rounded down to multiple of 1000 (step value)
        var loan = Math.floor(drupalSettings.borrowAdditional.loanAmount / 1000) * 1000;

        $range.attr('min', 5000);
        $range.attr('max', loan);
        $range.attr('step', 1000);
//        $range.attr('value', (loan + 5000) / 2);      // Set loan value to slider middle
        $range.attr('value', loan);                     // Set loan value to slider max
        $range.trigger('change');
      });

      // If the plus or minus buttons are clicked then update the range
      $('.step-update').once('santander_webforms_mortgage_calculator_range_step_update').click(function(){

        var $slider = $(this).nextAll('input');

        if($(this).hasClass('increase-step-amount')){
          $slider.val(parseInt($slider.val()) + parseInt($slider.attr('step')));
        } else {
          $slider.val(parseInt($slider.val()) - parseInt($slider.attr('step')));
        }

        $('.mortgage-calculator input[type="range"]').trigger('change');

      });

      // Adding a loan part
      $('.btn-add-loan-part').once('santander_webforms_mortgage_calculator_loan_part-add').click(function(e) {

        e.preventDefault();

        // Find the first loan part that is not visible
        var $loanPart = $('.container-loan-parts .loan-part:hidden:first');

        // Show it
        $loanPart.show(100);

        // Set focus on first input
        $loanPart.find(':input:enabled:visible:first').focus();

        // Disable Add loan part if all 5 showing
        var $loanPartCount = $('.container-loan-parts .loan-part:visible').length;

        if ($loanPartCount >= 5) {
          $(this).prop('disabled', true);
        }

      });

      // Removing a loan part
      $('.btn-remove-loan-part').once('santander_webforms_mortgage_calculator_loan_part-add').click(function(e) {

        e.preventDefault();

        var $container = $(this).closest('.loan-part');

        $container.find('select')[0].selectedIndex = 0;
        $container.find('select')[1].selectedIndex = 0;
        $container.find('input[type=radio]').prop('checked', false);
        $container.find('input[type=text]').val('').trigger("input");

        $container.hide(100);

        // Move the removed element to the end of the loan parts so if added it appears at the bottom
        $('.container-loan-parts').append($container);

        // Enable Add loan part
        $('.btn-add-loan-part').prop('disabled', false);
      });

      // Trigger input event on the loan part amount 1 field (any would do) to force evaluation of form state
      $('.mortgage-calculator-additional input[name="loan_part_amount_1"]').trigger('input');
    }
  };
})(jQuery, Drupal);
