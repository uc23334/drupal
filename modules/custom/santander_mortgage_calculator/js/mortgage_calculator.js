(function ($, Drupal) {

  "use strict";

  Drupal.behaviors.santander_webforms_mortgage_calculator = {

    attach: function (context, settings) {

      // Handle enabling and disabling of the form submit buttons
      $('.mortgage-calculator input, .mortgage-calculator select').once('santander_webforms_mortgage_calculator_gates').each(function(index) {

        $(this).on('input change', function(event) {

          // Ignore input events on radio and select elements as these will be handled by the change
          if ((event.type === 'input')) {
            if ($(this).is(':radio') || $(this).is('select')) {
              return;
            }
          // Ignore change events on text elements as these will be handled by the input
          } else if ((event.type === 'change')) {
            if ($(this).is('input:text')) {
              return;
            };
          }

          // Not on the questions page so quit
          if (!($('.progress-tracker-full .progress-step.is-active').data('webform-page') === 'page_questions')) {
            return;
          }

          var isOk = true;

          var buyerType = $('select[name="buyer_type_select"] option:selected').val();

          // Buyer type has been selected
          if (buyerType === '') {
            var isOk = false;
          };

          // If Remortgaging
          if (buyerType === 'remortgage') {

            // Must indictae whether borrowing more
            if ($('input[name="extra"]:checked').length === 0) {
              var isOk = false;
            }

            // If remortgaging and not borrowing more then must indicate whether borrowers are same or not
            if ($('input[name="extra"]:checked').val() === '0' ) {
              if ($('input[name="same_borrowers"]:checked').length === 0) {
                var isOk = false;
              }
            }
          }

          // Number of dependents chosen.
          if ($('input[name="dependants"]:checked').length === 0) {
            var isOk = false;
          }

          // Depending on number of applicants check income field
          if ($('input[name="applicants"]:checked').val() === '1') {
            if ($('input[name="income_single_annual_amount"]').val() == 0) {
              var isOk = false;
            }
          } else {
            if ($('input[name="income_combined_annual_amount"]').val() == 0) {
              var isOk = false;
            }
          }

          // Must have some deposit/equity
          if ($('input[name="deposit"]').val() == 0) {
            var isOk = false;
          }

          // Must specify a mortgage term
          if ($('select[name="terms"] option:selected').val() === '') {
            var isOk = false;
          };

          // If all ok then enable the button
          if (isOk) {
            $('button[name="show-results"]').prop('disabled', false);
          } else {
            $('button[name="show-results"]').prop('disabled', true);
          };
        });
      });

      // Handle the range fields
      $('.mortgage-calculator input[type="range"]').once('santander_webforms_mortgage_calculator_range_update').each(function( index ) {

        // Move the input field above the range and add + and - buttons
        $($(this).next()).insertBefore(this);
        $('<span class="step-update decrease-step-amount circle minus"></span>').insertBefore($(this).prevAll('output.range-input'));
        $('<span class="step-update increase-step-amount circle plus"></span>').insertAfter($(this).prevAll('output.range-input'));

        // Initialise the range value, size and step value
        // var loan = parseInt($('input[name="loan_amount"]').val());

        // $(this).attr('min', 5000);
        // $(this).attr('max', loan);
        // $(this).attr('value', (loan + 5000) / 2);
        // $(this).attr('step', 1000);

        $(this).on('input change', function () {

          // Cache this for efficiency
          var  el = $(this);
          var valueIn = el.val();

          // Set output text with prefix and suffix.
          var text = (el.prevAll('output').attr('data-field-prefix') || '') + valueIn + (el.prevAll('output').attr('data-field-suffix') || '');

          el.prevAll('output').text(text);

          // if(el.attr('id') === 'edit-amount') {
          //   positionLabel(el);
          // }
        }).trigger('input');
      }).change();

      // Handle the amount range
      $('.mortgage-calculator input[name="borrow_amount"]').once('santander_webforms_mortgage_calculator_range_borrow').each(function(index) {

        var $range = $(this);

        $range.on('input change', function() {

          var value = webformComponents.formatMoney($(this).val(), 0);

          $(this).prevAll('output').text(value);

          // Work out the percentage value of the loan amount as part of the total purchase price.
          // Price is the chosen loan amount + any deposit
          var deposit = parseInt($('input[name="deposit_amount"]').val());
          var loan = parseInt($(this).val());
          var price = loan + deposit;

          // Deposit as a percentage
          var depositPercentage = deposit / price *  100;

          // Update the house fill graphic and percentage values
          var $house = $('#house-indicator');

          $house.find('.house-fill').css('height', depositPercentage + '%');
          $house.find('.loan .percentage').text((100 - depositPercentage).toFixed());
          $house.find('.deposit .percentage').text(depositPercentage.toFixed());

          // Deposit percentage < 5?
          $('.results-wrapper div.disclaimer').text('');
          if (depositPercentage < 5){
            $(".results-wrapper div.disclaimer").text("We need at least a 5% deposit. Please change what you're looking for if you wish to see available mortgages.");
          }
        });

        // Try to make a sensible stab at the step size
        var step;
        var loanrange = drupalSettings.borrowCalc.loanAmount - 6000;

        switch(true) {
          case  (loanrange > 2994000): step = 5000; break;
          case  (loanrange > 1994000): step = 2000; break;
          case  (loanrange >  994000): step = 1000; break;
          case  (loanrange >  494000): step =  500; break;
          case  (loanrange >   94000): step =  250; break;
          case  (loanrange >   44000): step =  100; break;
          case  (loanrange >    4000): step =   50; break;
          case  (loanrange >       0): step =   10; break;
        }

        // Initialise the range value, size and step value
        // Slider should be rounded down to multiple of (step value)
        var loan = Math.floor(drupalSettings.borrowCalc.loanAmount / step) * step;

        $range.attr('min', 6000);
        $range.attr('max', loan);
        $range.attr('step', step);
//        $range.attr('value', (loan + 6000) / 2);      // Set loan value to slider middle
        $range.attr('value', loan);                     // Set loan value to slider max
        $range.trigger('change');
      });

      // If the plus or minus buttons are clicked then update the range
      $('.step-update').once('santander_webforms_mortgage_calculator_range_step_update').click(function(){

        var slider = $(this).nextAll('input');

        if($(this).hasClass('increase-step-amount')){
          $(slider).val(parseInt(slider.val()) + parseInt($(slider).attr('step')));
        } else {
          $(slider).val(parseInt(slider.val()) - parseInt($(slider).attr('step')));
        }

        $('.mortgage-calculator input[type="range"]').trigger('change');

      });

      // Trigger input event on the income field (any would do) to force evaluation of form state
      $('.mortgage-calculator input[name="income_applicant_1_annual_amount"]').trigger('input');
    }
  };
})(jQuery, Drupal);
