/* Tasks/questions
 *
 * Comparison table will display yearly value if been selected in product table - maybe ok?
 * Performance in IE (attribute selectors???)
*/

(function ($, Drupal) {

  "use strict";

  function formatUnicorn(value, literalObject) {

    "use strict";

    var str = value.toString();

    if (literalObject.length) {

      var t = typeof literalObject[0];
      var key;
      var args = ("string" === t || "number" === t) ? Array.prototype.slice.call(literalObject) : literalObject[0];

      for (key in args) {
        if( key != null){
          str = str.replace(new RegExp("\\{" + key + "\\}", "gi"), args[key]);
        }
      }
    }

    return str;
  };

  Drupal.mortgageComparisonAdditional = {

    currentState: 'questions',
    propertyValue: 0,
    borrowAmount: 0,
    loanTerm: 0,
    existingBalance: 0,
    productCount: 0,
    productsShown: 0,
    showAll: false,
    costPeriod: 'month',

    // Called from webform formula element
    setInputs: function(params) {

      this.propertyValue = params['propertyValue'];
      this.borrowAmount = params['borrowAmount'];
      this.loanTerm = params['loanTerm'];
      this.existingBalance = params['existingBalance'];

    },

    // Called from webform formula element
    getInputs: function() {

      var propertyValue = parseFloat(jQuery('input[name="property_value"]').val().replace(/,/g, ''));
      var borrowAmount = parseFloat(jQuery('input[name="borrow_amount"]').val().replace(/,/g, ''));
      var loanTerm = parseFloat(jQuery('select[name="loan_term"]').val());
      var existingBalance = parseFloat(jQuery('input[name="existing_balance"]').val().replace(/,/g, ''));

      var params = {
        propertyValue: propertyValue,
        borrowAmount: borrowAmount,
        loanTerm: loanTerm,
        existingBalance: isNaN(existingBalance)?0:existingBalance,
      };

      return params;
    },

    updateState: function(newState) {

      this.currentState = newState;

      switch(this.currentState) {

        case 'questions':

          // Switch the wizard
          $('.webform-submission-mortgage-comparison-additional-add-form .progress-tracker .step-1').removeClass('is-complete is-upcoming').addClass('is-active');
          $('.webform-submission-mortgage-comparison-additional-add-form .progress-tracker .step-2').removeClass('is-active is-complete').addClass('is-upcoming');

          $('#edit-questions').show();
          $('#edit-results').hide();

          // Only allow 'show products' if inputs completed
          if (this.propertyValue && this.borrowAmount) {
            $('#btn-show-products').prop('disabled', false);
          } else {
            $('#btn-show-products').prop('disabled', true);
          }

        break;

        case 'results':

          // Switch the wizard
          $('.webform-submission-mortgage-comparison-additional-add-form .progress-tracker .step-1').removeClass('is-active is-upcoming').addClass('is-complete');
          $('.webform-submission-mortgage-comparison-additional-add-form .progress-tracker .step-2').removeClass('is-upcoming is-complete').addClass('is-active');

          this.resultsIntro(this.propertyValue, this.borrowAmount, this.loanTerm, this.existingBalance);
          this.filterResultsTable(this.propertyValue, this.borrowAmount, this.loanTerm, this.existingBalance, this.showAll);

          $('#edit-questions').hide();
          $('#edit-results').show();
          $('#edit-mortgages-available').show();
          $('#edit-mortgages-comparison').hide();

          // Only allow compare products when some selected
          if ($('.webform-submission-mortgage-comparison-additional-add-form .view-mortgage-products-additional #product-comparison-table div[data-field-name=select] input:checkbox:checked').length >= 2) {
            $('#btn-compare-products').prop('disabled', false);
          } else {
            $('#btn-compare-products').prop('disabled', true);
          }

          // Update the show all button text to show the total number of products or 'Show less'
          if (this.showAll) {
            $('#btn-show-all-products').prop('disabled', false);
            $("#btn-show-all-products").html('Show less');
          } else {
            if (this.productCount > this.productsShown) {
              $('#btn-show-all-products').prop('disabled', false);
              $("#btn-show-all-products").html('Show all ' + this.productCount + ' available loans');
            } else {
              $('#btn-show-all-products').prop('disabled', true);
              $("#btn-show-all-products").html('No more available loans');
            }
          }

        break;

        case 'comparison':
          $('#edit-results').show();
          $('#edit-mortgages-available').hide();
          $('#edit-mortgages-comparison').show();
        break;
      }

      return this.currentState;
    },

    resultsIntro: function(propertyValue, borrowAmount, loanTerm, existingBalance) {

      if (propertyValue && borrowAmount && loanTerm) {

        var formatter = new Intl.NumberFormat('en-GB', {
          style: 'currency',
          currency: 'GBP',
          minimumFractionDigits: 0
        });

        if (existingBalance > 0) {
          var existingBalanceText = "an existing mortgage balance of <strong>{existingBalance}</strong>,";
          var existingBalanceOutput = formatUnicorn(existingBalanceText, [{existingBalance: formatter.format(existingBalance)}]);
        } else {
          var existingBalanceOutput = '';
        }

        var text  = "You have {existingBalance} an estimated property value of <strong>{propertyValue}</strong> and want to borrow an additional <strong>{borrowAmount}</strong> over a period of <strong>{loanTerm}</strong> years.";
        var output = formatUnicorn(text,[{propertyValue: formatter.format(propertyValue), borrowAmount: formatter.format(borrowAmount), loanTerm: loanTerm, existingBalance: existingBalanceOutput}]);

        $('#your-results').html(output);

        var loanPercentage = borrowAmount / propertyValue * 100;
        var depositPercentage = 100 - loanPercentage;

        $('#house-indicator .house-fill').css('height', depositPercentage + '%');
        $('#house-indicator .loan .percentage').text(loanPercentage.toFixed());
        $('#house-indicator .deposit .percentage').text(depositPercentage.toFixed());

      }
    },

    filterResultsTable: function(propertyValue, borrowAmount, loanTerm, existingBalance, showAll) {

      if (propertyValue && borrowAmount) {

        var that = this;

        var $table = $(".webform-submission-mortgage-comparison-additional-add-form .view-mortgage-products-additional #product-comparison-table");

        // Work out the loan to value (This includes any exiting borrowing)
        var ltv = ((borrowAmount + existingBalance) / propertyValue) * 100;
        if (isNaN(ltv)) {
          ltv = 0;
        }

        this.filterOnUserInput(ltv, borrowAmount);

        // Now we have applied the basic user inputs filter we can get the number of products that match.
        this.productCount = $table.find('.row-data').not(".filtered").length;

        // If only showing the reduced list then we need to show only unique product types and periods (sorted by monthly amount)
        if (!showAll) {
          this.reduceProductList($table);
        } else {
          $table.find('.row-data').removeClass('hidden');
        }

        // Update the mortgage cost field
        this.updateMortgageCost($table, loanTerm, borrowAmount);

        this.productsShown = $table.find('.row-data').not(".filtered, .hidden").length;

        // ltv > 95?
        $('div.disclaimer', $table).remove();

        if ((ltv > 95 && !$("div.disclaimer", $table).length) || this.productsShown == 0) {

          var disclaimer = "<div class= \"disclaimer\" style=\"text-align: center; margin: 0 20px;\">We don't currently have any loans available based on your estimated property value and how much you want to borrow. Please change what you're looking for if you wish to compare our loan rates.</div>";

          $table.append(disclaimer);
        }
      }

      return '';
    },

    filterOnUserInput: function(ltv, borrowAmount) {

      $('#product-comparison-table .row-data').removeClass('filtered');

      var $rows = $('#product-comparison-table .row-data').filter(function() {

        var bMaxLTV = $(this).data('maxltv') >= ltv;
        var bAmount = ($(this).data('min') <= borrowAmount) && ($(this).data('max') >= borrowAmount);

        // if all conditions are true then we dont want to include this row in the filter
        if (bMaxLTV && bAmount) {
          return false;
        } else {
          return true;
        }
      });

      $rows.addClass('filtered');
    },

    reduceProductList: function($table) {

      var that = this;
      var indexes = {};
      var count = 0;

      // Initial state is that all no filtered rows will be hidden
      $table.find('.row-data').not(".filtered").addClass('hidden');

      // Look at all rows that where not filtered out by the users input
      $table.find(".row-data").not(".filtered").each(function() {

        var period = $(this).data("period");
        var ptype = $(this).data("ptype");
        var mNid = $(this).data("mnid");

        if(!indexes[ptype]) {
          indexes[ptype] = {};
        }

        if(!indexes[ptype][period]) {
          indexes[ptype][period] = {count: 1, id: ''};
        }

        if(indexes[ptype][period]['count'] <= 1) {

          count += 1;

          indexes[ptype][period]['count'] += 1;
          indexes[ptype][period]['id'] = $(this).attr("id");

        } else {
          return;
        }
      });

      // Set visible the rows that are left
      $.each(indexes, function(i, val) {
        $.each(val, function(ii, ival) {
          $table.find('#' + ival['id']).removeClass('hidden');
        });
      });

    },

    updateMortgageCost: function($table, loanTerm, borrowAmount) {

      if ((!isNaN(loanTerm)) && (!isNaN(borrowAmount))) {

        if (this.costPeriod === 'month') {
          $table.find(".row-data .monthly-cost .field-title").text('Monthly cost');
        } else {
          $table.find(".row-data .monthly-cost .field-title").text('Yearly cost');
        }

        var formatter = new Intl.NumberFormat('en-GB', {
          style: 'currency',
          currency: 'GBP',
          minimumFractionDigits: 0
        });

        var that = this;

        $table.find('.row-data').each( function() {

          var n = $(this).data('initialrate') / 100;

          if (!isNaN(n)) {

            var interest = (n / 12) / (1 - (Math.pow(1 + (n / 12), (-1 * (loanTerm * 12)))));

            if (that.costPeriod === 'month') {
              var monthlyCost = Math.round(borrowAmount * interest);
            } else {
              var monthlyCost = Math.round(borrowAmount * interest) * 12;
            }

            $(this).attr('data-monthly-cost', monthlyCost);

            $(this).find(".monthly-cost .field-value").html(formatter.format(monthlyCost));
          }
        });
      }
    },

    findEmptyComparisonCol: function() {

      var $cols = $('#mortgage-comparison-table th.col[data-current-product=none');

      // If we found one then get the column number
      if ($cols.length) {
        var $targetcol = $cols.first();
        var col = $targetcol.data('column-number').toString();
      } else {
        return false;
      }

      return col;
    },

    updateComparisonCol: function(sourcecheckbox, targetcol) {

      var sourcerow = $(sourcecheckbox).closest('.row-data');

      // Get the cells from the selected mortgage product row
      var $sourcecells = $(sourcerow).find('.field');

      var $productname = $sourcecells.filter('*[data-field-name=name]').find('.field-value').text();

      // Get all the table cells for the column we found
      var $targetcells = $('#mortgage-comparison-table td.col-product-' + targetcol + ',' + '#mortgage-comparison-table th.col-product-' + targetcol);

      // For each table cell in the target column get the data for it from the appropriate table cell of the selected row in the product table
      $targetcells.each(function(index) {

        // Get the field name of the field we are dealing with
        var fieldname = $(this).data('field-name');

        // See if there is a matching field in source cells
        var $sourcecell = $sourcecells.filter('*[data-field-name=' + fieldname + ']').find('.field-value');

        // If there is then update our value to that from the source and set the hidden title div to thr value of the product title
        if ($sourcecell.length) {
          $(this).children('.field-title').text($productname);
          $(this).children('.field-value').html($sourcecell.html());
          $(this).show();
        } else {
          $(this).children('.field-value').text('-');
          $(this).show();
        };

      });

      // Now update the column header so we know its in use.
      var productcode = $(sourcerow).data('prodcode');

      $('#mortgage-comparison-table th.col-product-' + targetcol).attr('data-current-product', productcode);
    },

    updateComparisonTitle: function() {

      var ccount = $('#mortgage-comparison-table th.col:not([data-current-product=none]').length;

      $('#mortgage-comparison-title').text("Comparing " + ccount + " loans");
    },

    resetComparisionCol: function(productid) {

      // Find the column with the product id
      var col = $('#mortgage-comparison-table th[data-current-product=' + productid + ']').first().data('column-number');

      // Reset the product id in the header
      $('#mortgage-comparison-table th.col-product-' + col).attr('data-current-product', 'none');

      // Reset all the fields in the column
      $('#mortgage-comparison-table td.col-product-' + col + ',' + '#mortgage-comparison-table th.col-product-' + col).html('<div class="field-title">-field-title-</div><div class="field-value"></div>').hide();

    },

    resetAllComparisionCols: function() {

      $('.webform-submission-mortgage-comparison-additional-add-form .view-mortgage-products-additional #product-comparison-table div[data-field-name=select] input:checkbox:checked').prop("checked", false).trigger("change");

    }
  };

  Drupal.behaviors.mortgageComparisonAdditional = {

    attach: function (context, settings) {

      $('.btn-show-products').once('santander_mortgage_comparison_show_products').each(function( index ) {

        $(this).on('click', function () {

          // Make sure any comparisons cleared
          if (Drupal.mortgageComparisonAdditional.currentState === 'questions') {
            Drupal.mortgageComparisonAdditional.resetAllComparisionCols();
            Drupal.mortgageComparisonAdditional.showAll = false;
            Drupal.mortgageComparisonAdditional.costPeriod = 'month';
          }

          Drupal.mortgageComparisonAdditional.updateState('results');

          // Move to top of product table
          $("#edit-your-results")[0].scrollIntoView({
            behavior: "smooth"
          });

        });
      });

      $('.btn-show-all-products').once('santander_mortgage_comparison_show_all_products').each(function( index ) {

        $(this).on('click', function () {
          if ($(this).text().substr(0,8) === 'Show all') {
            Drupal.mortgageComparisonAdditional.showAll = true;
          } else {
            Drupal.mortgageComparisonAdditional.resetAllComparisionCols();
            Drupal.mortgageComparisonAdditional.showAll = false;
          }
          Drupal.mortgageComparisonAdditional.updateState('results');

          // Stop sticky hover on touch devices
          $(this).clone(true).insertAfter($(this));
          $(this).remove();

          // Move to top of comparison table
          $("#product-comparison-table")[0].scrollIntoView({
            behavior: "smooth"
          });
        });

      });

      $('#edit-cost-period input[name=cost_period]').once('santander_mortgage_comparison_costs_monthly').each(function( index ) {
        $(this).on('change', function () {
          if ($(this).is(":checked")) {
            if ($(this).val() === '0') {
              Drupal.mortgageComparisonAdditional.costPeriod = 'month';
            } else {
              Drupal.mortgageComparisonAdditional.costPeriod = 'year';
            }
            Drupal.mortgageComparisonAdditional.updateState('results');
          }
        });
      });

      $('.btn-show-questions').once('santander_mortgage_comparison_show_questions').each(function( index ) {
        $(this).on('click', function () {
          Drupal.mortgageComparisonAdditional.updateState('questions');
        });
      });

      $('.btn-compare-products').once('santander_mortgage_comparison_compare_products').each(function( index ) {
        $(this).on('click', function () {
          Drupal.mortgageComparisonAdditional.updateState('comparison');
          // Move to top of detailed comparison table
          $("#mortgage-comparison-table")[0].scrollIntoView({
            behavior: "smooth"
          });
        });
      });

      $('#product-comparison-table .trigger-secondary-fields').once('santander_mortgage_comparison_secondary_trigger').each(function(index) {

        $(this).on('click', function (e) {

          var $el = $(this).closest('.fields-panel').find('.secondary-fields-wrapper');

          if ($el.is(":visible")) {

            $(this).text('Show details');

            $el.slideUp({
              duration: 500
            });

          } else {

            $el.slideDown({
              duration: 500
            });

            $(this).text('Hide details');

          }

          return false;
        });
      });

      $('.product-select').once('santander_mortgage_product_select').each(function( index ) {

        $(this).on('change', function (e) {

          if ($(this).is(':checked')) {

            // Find an empty column in the comparison table to fill
            var col = Drupal.mortgageComparisonAdditional.findEmptyComparisonCol();

            // Got an empty col so copy the source row data to the column
            if (col) {
              Drupal.mortgageComparisonAdditional.updateComparisonCol(this, col);
              Drupal.mortgageComparisonAdditional.updateComparisonTitle();
            } else {
              this.checked = false;
            }

          } else {

            // Get the product ID from the row
            var productid = $(this).closest('.row-data').data('prodcode');

            Drupal.mortgageComparisonAdditional.resetComparisionCol(productid);
            Drupal.mortgageComparisonAdditional.updateComparisonTitle();
          }

          Drupal.mortgageComparisonAdditional.updateState('results');
        });

      });

    }
  };

})(jQuery, Drupal);
